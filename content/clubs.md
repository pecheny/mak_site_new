+++
title = "Клубы МАК"
date = 2006-12-16T00:05:23
author = "Константин Кноп"
guid = "http://mak-chgk.ru/spisok-klubov-vhodyaschih-v-mak/"
slug = "clubs"
aliases = [ "/post_27", "/spisok-klubov-vhodyaschih-v-mak",]
type = "post"
categories = [ "Клубы МАК",]
tags = []
+++

В Международную ассоциацию клубов ЧГК по состоянию на 22.12.2017 входят следующие клубы и организации



**Клубы — члены МАК ЧГК** 



</p> 



  * Бакинский клуб интеллектуальных игр &#8220;Атешгях&#8221;, г. Баку, Азербайджан

  * Ереванский клуб &#8220;Что? Где? Когда?&#8221;, г. Ереван, Армения

  * Армянская ассоциация знатоков, г. Ереван, Армения

  * Гомельский клуб интеллектуальных игр &#8220;Белая рысь&#8221;, г. Гомель, Беларусь

  * Ассоциация клубов знающих, г. София, Болгария - 

_членство приостановлено с 2009 г._ 



  * Федерация интеллектуальных игр Германии, г. Кёльн, Германия

  * Клуб &#8220;Что? Где? Когда?&#8221; Грузии, г. Тбилиси, Грузия

  * Молодёжный интеллектуальный клуб МИЛИ, г. Иерусалим и др., Израиль

  * Израильский центр интеллектуальных клубов, гг. Иерусалим, Хайфа, Тель-Авив и др., Израиль

  * Канадская ассоциация клубов &#8220;Что? Где? Когда?&#8221; (КанАК)

  * Клуб интеллектуальных игр &#8220;Эрудит&#8221;, г. Даугавпилс, Латвия

  * Клуб &#8220;УМНИКИ&#8221;, г. Висагинас, Литва

  * Клуб интеллектуальных игр &#8220;ИНТИГРА&#8221;, г. Кишинев, Молдова

  * Клуб интеллектуальных игр &#8220;ВО!&#8221; (КИИ &#8220;ВО!&#8221;), г. Великие Луки, Россия

  * Владимирский клуб интеллектуальных игр, г. Владимир, Россия

  * Центр интеллектуальных технологий &#8220;ЦИТ.RUS.&#8221;, г. Воронеж, Россия

  * Детский интеллектуальный клуб &#8220;Квинт&#8221;, г. Выборг, Россия

  * Гусевский клуб интеллектуальных игр, г. Гусь-Хрустальный, Россия

  * Долгопрудненское городское отделение МОО ИНТИ, г. Долгопрудный, Россия

  * Екатеринбургский клуб интеллектуальных игр &#8220;Веда&#8221;, г. Екатеринбург, Россия

  * Клуб интеллектуальных игр &#8220;Зеленый Шум&#8221;, г. Зеленоград, Россия

  * Брэйн-клуб Республики Татарстан, г. Казань, Россия

  * Калининградская РМОО &#8220;Янтарный Брэйн&#8221;, г. Калининград, Россия

  * КИТИК &#8220;Клуб интеллектуальных творческих игр Калуги&#8221;, г. Калуга, Россия

  * Ассоциация московских клубов &#8220;Что? Где? Когда?&#8221;, г. Москва, Россия

  * Бауманский клуб знатоков, г. Москва, Россия

  * Детско-юношеское объединение интеллектуально-творческого развития (ДЮОИТР) &#8220;МАГИ&#8221;, г. Москва, Россия - 

_членство приостановлено с 20.06.2013_ 



  * Межрегиональная общественная организация интеллектуально-творческих игр (МОО &#8220;ИНТИ&#8221;), г. Москва, Россия

  * Московский университетский брэйн-клуб, г. Москва, Россия

  * Новгородский городской клуб &#8220;У лукоморья&#8221;, г. Новгород, Россия

  * Клуб любителей интеллектуальных игр, г. Новосибирск, Россия - 

_членство приостановлено с 2009 г._ 



  * Клуб любителей интеллектуальных игр НГУ &#8220;Мозговорот&#8221;, г. Новосибирск, Россия

  * Норильский клуб интеллектуальных игр, г. Норильск, Россия- 

_членство приостановлено с 20.06.2013_ 



  * Переславский клуб интеллектуальных игр, г. Переславль-Залесский, Россия

  * Клуб интеллектуальных игр &#8220;Самсон&#8221;, г. Петродворец, Россия

  * Клуб интеллектуальных развлечений &#8220;Эврика&#8221;, с. Приволжье Самарской обл., Россия

  * Клуб интеллектуальных игр &#8220;Имя розы&#8221;, г. Ростов-на-Дону, Россия

  * Самарская городская молодежная общественная организация &#8220;Самарская лига знатоков&#8221;, г. Самара, Россия

  * Самарский городской молодежный клуб интеллектуальных игр &#8220;Логос&#8221;, г. Самара, Россия

  * МГОО &#8220;Клуб интеллектуального творчества Саранска&#8221;, г. Саранск, Россия

  * Клуб интеллектуального творчества &#8220;Знак Ответа&#8221;, г. Саратов, Россия

  * Клуб интеллектуального творчества &#8220;Чёрный Квадрат&#8221;, г. Северодвинск, Россия

  * Клуб интеллектуальных игр &#8220;Дети Коломны&#8221;, г. Санкт-Петербург, Россия

  * Клуб интеллектуальных игр &#8220;Коломна&#8221;, г. Санкт-Петербург, Россия

  * Клуб ЧГК &#8220;ДОМ&#8221;, г. Санкт-Петербург, Россия

  * Тульский клуб &#8220;Что? Где? Когда?&#8221;, г. Тула, Россия

  * Ульяновская местная общественная организация &#8220;Клуб интеллектуальных игр &#8220;Ворон&#8221;&#8220;, г. Ульяновск, Россия

  * Клуб интеллектуальных игр ОАО &#8220;Северсталь&#8221;, г. Череповец, Россия

  * Якутский республиканский клуб интеллектуальных игр, г. Якутск, Россия- 

_членство приостановлено с 20.06.2013_ 



  * Нью-Йоркский интеллектуальный клуб (НИК), г. Нью-Йорк, США

  * Клуб интеллектуального творчества &#8220;Уникум&#8221;, г. Ашхабад, Туркменистан - 

_членство приостановлено с 2009 г._ 



  * Премьер-лига (Ташкентская ассоциация клубов ЧГК), г. Ташкент, Узбекистан

  * Литературно-игровой клуб ОМК &#8220;Эрудит&#8221; им. В. Я. Мороховского, г. Одесса, Украина

  * Клуб знатоков при &#8220;Центре развития интеллекта&#8221;, г. Горловка, Украина - 

_членство приостановлено с 2009 г._ 



  * Городской клуб любителей игры &#8220;Что? Где? Когда?&#8221;, г. Симферополь

  * Харьковский городской культурно-просветительский Центр интеллектуального творчества &#8220;От Винта&#8221;, г. Харьков, Украина

  * Клуб интеллектуального творчества им. А. Козубова, г. Днепропетровск, Украина

  * Клуб &#8220;Золотой шпаги&#8221;, г. Хельсинки и др., Финляндия

  * Клуб ЧГК г. Таллина (Tallinna Malumangukubi), г. Таллин, Эстония

  * Интернет-клуб &#8220;Что? Где? Когда?&#8221;, международный

  * Ярославский клуб интеллектуальных игр (ЯрКИИ), г. Ярославль, Россия

  * Тюменский областной студенческий интеллектуальный клуб (ТОСИК), г. Тюмень, Россия

  * Иркутский клуб интеллектуальных игр, г. Иркутск, Россия

  * Рижский клуб по игре &#8220;Что? Где? Когда?&#8221; (общественная организация &#8220;Intellect&#8221;), г. Рига, Латвия

  * Интеллектуальный клуб &#8220;Альбус&#8221;, г. Владикавказ, Россия

  * Клуб волгоградских интеллектуалов &#8220;КВИнтел&#8221;, г. Волгоград, Россия

  * Республиканское общественное объединение &#8220;Белорусская лига интеллектуальных команд&#8221; (РОО &#8220;БЛИК&#8221;), Беларусь

  * Клуб интеллектуальных игр &#8220;Заковат&#8221;, г. Ташкент, Узбекистан

  * Клуб интеллектуального развития &#8220;Игромир&#8221;, г. Ашхабад, Туркменистан

  * Интеллектуальный центр &#8220;Хоран Ард&#8221;, г. Гюмри, Армения

  * Клуб &#8220;Туран&#8221;, г. Баку, Азербайджан

  * Клуб &#8220;Асири&#8221;, г. Красноярск, Россия

  * Лига украинских клубов (ЛУК), Украина - 

_членство приостановлено с 14.10.2016_ 



  * КОМГО &#8220;Пiвденна Лiга&#8221;, г. Кировоград, Украина- 

_членство приостановлено с 20.06.2013_ 



  * Тольяттинская лига знатоков, г. Тольятти, Россия

  * Молодежный интеллектуальный клуб &#8220;МИК&#8221;, г. Набережные Челны, Россия

  * Воронежский областной клуб интеллектуальных игр &#8220;Афина&#8221;, г. Воронеж, Россия

  * Крымский республиканский центр интеллектуального развития, г. Симферополь

  * Клуб интеллектуальных игр &#8220;Калининградские знатоки&#8221;, г. Калининград, Россия

  * Тартуский клуб интеллектуальных игр, г. Тарту, Эстония

  * Лондонский клуб ЧГК, г. Лондон, Англия

  * Пражский клуб знатоков, г. Прага, Чехия

  * Московская областная общественная организация &#8220;Интеллектуальное кольцо&#8221;, Россия

  * Клуб &#8220;Точка вопроса&#8221;, г. Коломна, Россия

  * Интеллектуальный клуб государственного учреждения &#8220;Центр культуры &#8220;Витебск&#8221;, г. Витебск, Беларусь

  * Кировская РОО &#8220;Лига интеллектуальных игр Вятки&#8221;, г. Киров, Россия

  * Курская региональная молодёжная общественная организация &#8220;Клуб интеллектуальных игр&#8221; (КРМОО &#8220;Клуб интеллектуальных игр&#8221;), г. Курск, Россия

  * Астраханская РОО &#8220;Интеллектуальная гильдия развития Астрахани&#8221; (АРОО &#8220;ИГРА&#8221;), г. Астрахань, Россия

  * Нижегородский клуб интеллектуальных игр &#8220;А&#8221; (НИКА), г. Нижний Новгород, Россия

  * Актауский клуб знатоков &#8220;Что? Где? Когда?&#8221;, г. Актау, Казахстан

  * Карагандинский клуб &#8220;Что? Где? Когда?&#8221; КГКП &#8220;Шахтёр&#8221;, г. Караганда, Казахстан

  * Узбекская лига знатоков &#8220;Уз-лига&#8221;, г. Ташкент, Узбекистан

  * Лига интеллектуальных игр Республики Коми, г. Сыктывкар, Россия- 

_членство приостановлено с 20.06.2013_ 



  * Нижнетагильский клуб интеллектуальных игр &#8220;Герб совы&#8221;, г. Нижний Тагил, Россия

  * Союз &#8220;Интеллект&#8221;, г.Москва, Россия- 

_членство приостановлено с 20.06.2013_ 



  * Лига интеллектуального спорта, г. Алма-Ата, Казахстан

  * Интеллектуальный молодёжный клуб &#8220;Хазар&#8221;, г. Баку, Азербайджан

  * Ново-Уренгойский городской клуб интеллектуальных игр &#8220;Полярный круг&#8221;, г. Новый Уренгой, Россия

  * Клуб интеллектуальных игр города Сургута, г. Сургут, Россия

  * Общественное объединение &#8220;Интеллектуальный клуб &#8220;Байтерек&#8221;&#8220;, г. Астана, Казахстан

  * Республиканская ассоциация игровых технологий &#8220;Интеллум&#8221;, г. Уфа, Россия

  * Чикагский клуб интеллектуальных игр &#8220;ЛАЭРТ&#8221;, г. Чикаго, США

  * Клуб интеллектуального творчества САФУ имени М. В. Ломоносова, г. Архангельск, Россия

  * Орловский городской клуб интеллектуальных игр, г. Орёл, Россия

  * Клуб интеллектуальных игр &#8220;Ичеришехер&#8221;, г. Баку, Азербайджан

  * Мурманский областной клуб интеллектуальных игр &#8220;Полярная сова&#8221;, г. Мурманск, Россия

  * Ассоциация интеллектуальных клубов Азербайджана, г. Баку, Азербайджан

  * Клуб &#8220;Что Где Когда&#8221;, г. Душанбе, Таджикистан

  * Клуб интеллектуальных игр &#8220;КЛИО&#8221;, г. Магас, Россия

  * Клуб интеллектуальных игр Ноябрьска, г. Ноябрьск, Россия

  * Магнитогорский клуб интеллектуальных игр, г. Магнитогорск, Россия

  * Клуб интеллектуальных развлечений, г. Вильнюс, Литва

  * ДОО &#8220;Игра&#8221;, г. Краснодар, Россия

  * Клуб &#8220;Что? Где? Когда?&#8221; Эдинбургского университета, г. Эдинбург, Шотландия

  * Владикавказский интеллектуальный клуб &#8220;Посторонним V&#8221;, г. Владикавказ, Россия

  * Клуб &#8220;Интеллектуальные игры в Саратове&#8221;, г. Саратов, Россия

  * Нидерландский клуб &#8220;Что? Где? Когда?&#8221;, Нидерланды

  * Варшавский клуб интеллектуальных игр, г. Варшава, Польша

  * Краковский клуб интеллектуальных игр, г. Краков, Польша

  * &#8220;Clever Afrodita intellectual competitions&#8221;, г. Лимассол, Кипр

  * Женевский клуб &#8220;Что? Где? Когда?&#8221;, г. Женева, Швейцария

  * Бернский клуб &#8220;Intellectus Gelvetiki&#8221;, г. Берн, Швейцария

  * Эйлатский Клуб &#8220;Что? Где? Когда?&#8221;, г. Эйлат, Израиль

  * Берлинский клуб знатоков, г. Берлин, Германия

  * Интеллектуальный клуб &#8220;Осьминог&#8221;, г. Владивосток, Россия

  * Мюнхенский клуб &#8220;Что? Где? Когда?&#8221;, г. Мюнхен, Германия



</ol>