+++
title = "Заседания Правления"
date = 2007-07-21T22:04:09
author = "Константин Кноп"
guid = "http://mak-chgk.ru/pravlenie/"
slug = "pravlenie"
aliases = [ "/post_808", "/pravlenie",]
type = "post"
categories = [ "Правление и комиссии",]
tags = []
+++

Заседания Правления МАК обычно проходят в Москве по адресу ул. Б.Дорогомиловская, 5. Если заседание проводилось в другом месте, оно указывается в скобках после даты заседания.



После конгресса 2014 г. состоялись следующие заседания: 



  * [15 марта 2014](http://mak-chgk.ru/pravlenie/2014-03-15/)

  * [8 июня 2014](http://mak-chgk.ru/pravlenie/2014-06-08/)

  * [21 декабря 2014](http://mak-chgk.ru/pravlenie/2014-12-21/)

  * [31 мая 2015](http://mak-chgk.ru/pravlenie/2015-05-31/)

  * [20 декабря 2015](http://mak-chgk.ru/pravlenie/2015-12-20/)

  * [23 октября 2016](http://mak-chgk.ru/pravlenie/2016-10-23/)

  * [18 декабря 2016](http://mak-chgk.ru/pravlenie/2016-12-18/)

  * [21 мая 2017](http://mak-chgk.ru/pravlenie/2017-05-21/)

  * [21 октября 2017](http://mak-chgk.ru/pravlenie/2017-10-21/)

  * [24 марта 2018](http://mak-chgk.ru/pravlenie/2018-03-24/)</p> 



</ul> После конгресса 2009 г. состоялись следующие заседания: 



  * [15 ноября 2009](http://mak-chgk.ru/pravlenie/2009-11-15/) (Москва, ул. Мосфильмовская, 40)

  * [20 декабря 2009](http://mak-chgk.ru/pravlenie/2009-12-20/)

  * [3 апреля 2010](http://mak-chgk.ru/pravlenie/2010-04-03/)

  * [6 июня 2010](http://mak-chgk.ru/pravlenie/2010-06-06/)

  * [31 октября 2010](http://mak-chgk.ru/pravlenie/2010-10-31/)

  * [27 февраля 2011](http://mak-chgk.ru/pravlenie/2011-02-27/)

  * [5 июня 2011](http://mak-chgk.ru/pravlenie/2011-06-05/)

  * [27 ноября 2011](http://mak-chgk.ru/pravlenie/2011-11-27/)

  * [8 апреля 2012](http://mak-chgk.ru/pravlenie/2012-04-08/)

  * [4 июня 2012](http://mak-chgk.ru/pravlenie/2012-06-04/) Информация о предыдущих заседаниях: 



  * [17 ноября 2001](http://mak-chgk.ru/pravlenie/2001-11-17/)

  * [4 февраля 2002](http://mak-chgk.ru/pravlenie/2002-02-04/)

  * [6 апреля 2002](http://mak-chgk.ru/pravlenie/2002-04-06/)

  * [15 июня 2002](http://mak-chgk.ru/pravlenie/2002-06-15/) (Баку)

  * [7 сентября 2002](http://mak-chgk.ru/pravlenie/2002-09-07/)

  * [26 марта 2005](http://mak-chgk.ru/pravlenie/2005-03-26/)

  * [12 июня 2005](http://mak-chgk.ru/pravlenie/2005-06-12/)

  * 11 сентября 2005 (Ярославль)

  * [22 октября 2005](http://mak-chgk.ru/pravlenie/2005-10-22/)

  * [11 декабря 2005](http://mak-chgk.ru/pravlenie/2005-12-11/)

  * [18 февраля 2006](http://mak-chgk.ru/pravlenie/2006-02-18/)

  * [14 мая 2006](http://mak-chgk.ru/pravlenie/2006-05-14/)

  * [25 августа 2006](http://mak-chgk.ru/pravlenie/2006-08-25/) (Калининград)

  * [29 октября 2006](http://mak-chgk.ru/pravlenie/2006-10-29/)

  * [18 февраля 2007](http://mak-chgk.ru/pravlenie/2007-02-18/)

  * [2 июня 2007](http://mak-chgk.ru/pravlenie/2007-06-02/)

  * [24 августа 2007](http://mak-chgk.ru/pravlenie/2007-08-24/) (-)

  * [18 мая 2008](http://mak-chgk.ru/pravlenie/2008-05-18/)

  * [15 июня 2008](http://mak-chgk.ru/pravlenie/2008-06-15/)

  * [11 июля 2009](http://mak-chgk.ru/pravlenie/2009-07-11/) (-)

  * [18 октября 2009](http://mak-chgk.ru/pravlenie/2009-10-18/) (-)