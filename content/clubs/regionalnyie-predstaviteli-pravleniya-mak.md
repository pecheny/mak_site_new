+++
title = "Региональные представители Правления МАК"
date = 2007-08-15T14:13:35
author = "Константин Кноп"
guid = "http://mak-chgk.ru/about/regionalnyie-predstaviteli-pravleniya-mak/"
slug = "regionalnyie-predstaviteli-pravleniya-mak"
aliases = [ "/post_877", "/about/regionalnyie-predstaviteli-pravleniya-mak",]
type = "post"
categories = [ "Клубы МАК",]
tags = []
+++

**Леонид Климович** (Белоруссия)



**Ирина Шихова** (Молдавия, Румыния, Болгария)



**Александр Барбакадзе** (Литва, Латвия, Эстония)



**Михаил Перлин** (Германия, Польша, Голландия, Люксембург)



**Павел Шевченко** (Пермский край)



**Дмитрий Кукулин** (Тюменская область)



**Дмитрий Жарков** (Юг СибФО, кроме Тюменской области - Омская, Томская, Кемеровская, Иркутская, Новосибирская, Читинская области, Алтайский край)



**Сергей Абрамов-Герт** (Южный федеральный округ)



**Кирилл Теймуразов** (Владимирская, Ивановская, Смоленская, Тульская, Тверская, Московская области)



**Владимир Ковалев** (Калужская область)



(утверждены на заседании Правления МАК от 18.02.2006)