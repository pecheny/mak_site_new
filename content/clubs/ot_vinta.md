+++
title = "ЦИТ &#171;От Винта&#187;, Харьков"
date = 2007-08-10T00:02:20
author = "Константин Кноп"
guid = "http://mak-chgk.ru/about/clubs/ot_vinta/"
slug = "ot_vinta"
aliases = [ "/post_848", "/about/clubs/ot_vinta",]
type = "post"
categories = [ "Клубы МАК",]
tags = []
+++

**1. Название организации:**



Харьковский городской культурно-просветительский Центр интеллектуального творчества (ЦИТ) &#8220;От Винта&#8221;



**2. Страна:** Украина **Населенный пункт:** г. Харьков



**3. Руководитель организации (должность, фамилия, имя), другие руководящие органы и их члены:** 



**Президент:** Башук Дмитрий Николаевич.



**Вице-президенты:** Неумывакина Ольга Евгеньевна, Лисянский Александр Сергеевич.



**Члены Правления:** Рогачев Михаил Юрьевич, Флору Игорь Валентинович.



**Ревизионная комиссия:** Данько Вадим Евгеньевич (председатель), Загребельная Марина Федоровна, Черепнев Игорь Аркадьевич.



**4. Средства связи с организацией:**



Почтовый адрес: 61002, Украина, г. Харьков, а/я 10442.



Телефоны: +38-057-7062238, +38-095-1445050



E-mail: dmnb@mail.ru, dmnbsh@gmail.com



**5. Дата создания организации:**



Как клуб - с ноября 1993 г.В нынешнем статусе (неприбыльная общественная организация с правами юридического лица) - с 28 августа 1996 г.



 ******6. Дата принятия организации в МАК:**



Декабрь 1996 г.



**7. Количество команд/человек, входящих в организацию:**



Официально - 17 команд (15 взрослых, 2 школьных), в которых играет 139 человек.



**8. Перечень мероприятий, которые проводила организация в 2006/07 гг.:**



_1. Межрегиональные:_



Самостоятельно: 



  * фестиваль &#8220;ОтВинтЕЖ-2006&#8221; (июль 2006 г.);

  * телефонный турнир по &#8220;Что? Где? Когда?&#8221; (далее - ЧГК) &#8220;Кубок &#8220;От Винта&#8221; (ноябрь 2006 г.);

  * микст-турнир по &#8220;Своей Игре&#8221; (далее - СИ) &#8220;Мартовские Коты&#8221; (март 2007 г.). В сотрудничестве с другими организациями: 



  * чемпионат Украины по СИ и &#8220;Эрудит-Квартету&#8221; (далее - ЭК) (сентябрь 2006 г.);

  * этапы чемпионата Украины по ЧГК во второй лиге (синхронный турнир ЛУК) (ноябрь 2006 г., январь, март 2007 г.).

  * Суперкубок ЛУК по ЧГК



_2. Харьковские:_



Самостоятельно: 



  * кубок открытия сезона по ЧГК &#8220;Братская Могила&#8221; (сентябрь 2006 г.);

  * студенческий Кубок ректора ХНУРЭ по ЧГК (декабрь 2006 г.). В сотрудничестве с другими организациями: 



  * чемпионат Харькова по ЧГК (первая и вторая лиги);

  * чемпионат Харькова по &#8220;Брэйн-рингу&#8221; (далее - БР) (высшая, первая и вторая лиги);

  * чемпионат Харькова по СИ;

  * чемпионат Харькова по ЭК;

  * отборочный турнир Кубка Украины по ЧГК;

  * молодежный чемпионат Харькова и чемпионаты вузов Харькова по ЧГК;

  * школьный чемпионат Харькова по ЧГК.



_3. Внутриклубные:_ 



  * чемпионат ЦИТ по ЧГК (сентябрь 2006 г. - июнь 2007 г.);

  * кубок ЦИТ по ЧГК (январь 2007 г.);

  * осенний (сентябрь - ноябрь 2006 г.) и весенний (февраль - март 2007 г.) чемпионаты ЦИТ по БР;

  * кубок ЦИТ по БР (май 2007 г.);

  * кубок ЦИТ по СИ (октябрь - декабрь 2006 г.);

  * суперкубок ЦИТ по СИ (январь 2007 г.);

  * чемпионат ЦИТ по ЭК (май 2007 г.);

  * кубок ЦИТ по ЭК (сентябрь 2006 г.);

  * кубок Дома ученых по БР, посвященный Дню Независимости Украины (август 2006 г.);

  * чемпионат ЦИТ по СИ (апрель-май 2007 г.);



**9. Перечень мероприятий, в которых участвовали команды и отдельные игроки организации в 2006/07 гг.(кроме мероприятий, указанных в п.8):** 



  * Кубок Наций по ЧГК;

  * Чемпионат и Кубок Украины по ЧГК;

  * Чемпионат Украины по БР;

  * Чемпионаты Украины по ЧГК и БР среди молодежных и школьных команд;

  * международные синхронные турниры по ЧГК: Кубок Провинций, Открытый Кубок России, Кубок Городов, Кубок Голливуда и т.д.