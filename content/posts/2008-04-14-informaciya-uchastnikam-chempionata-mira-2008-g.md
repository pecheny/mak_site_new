+++
title = "Информация участникам Чемпионата мира 2008"
date = 2008-04-14T20:01:40
author = "Константин Кноп"
guid = "http://mak-chgk.ru/informaciya-uchastnikam-chempionata-mira-2008-g/"
permalink = "/post_3513/"
categories = [ "Турниры",]
slug = "informatsiia-uchastnikam-chempionata-mira-2008"
aliases = [ "/post_3513", "/informaciya-uchastnikam-chempionata-mira-2008-g",]
type = "post"
tags = []
+++

Информируем Вас, что 7-й Чемпионат Мира по игре &#8220;Что? Где? Когда?&#8221; состоится предположительно 10-14 сентября 2008 года в Калининградской области (Россия). Более точные даты и примерное расписание будет опубликовано не позднее чем за 60 дней до даты заезда.



К участию в ЧМ-08 с оплатой проезда и проживания за счет Оргкомитета допускаются следующие команды:



  1. Чемпион мира.

  2. Чемпионы следующих стран: Россия, Израиль, Украина, Германия, Азербайджан, США, Латвия, Армения, Финляндия, Белоруссия

  3. Победители этапов КМ сезона 2007-2008.

  4. Не более 3-х команд, из занимающих наиболее высокие места в ТОП10 рейтинга МАК (на 1.06.08), не прошедшие по пунктам 1-3.

  5. 2 команды, получившие вайлд-кард Правления МАК.



С оплатой проезда и проживания за свой счет допускаются следующие команды:



<ol start="6">

  <li>

    Чемпионы следующих стран: Эстония, Англия, Грузия, Канада, Молдавия, Туркмения, Узбекистан, Литва

  </li>

  <li>

    Экс-чемпионы мира, не прошедшие по пунктам 1-5.

  </li>

  <li>

    Команды, входящие в ТОП10 рейтинга МАК (на 1.06.08), не прошедшие по пунктам 1-5.

  </li>

</ol>



Для команд, перечисленных в пп. 6-8, возможна частичная компенсация расходов на проезд и проживание за счет Оргкомитета. Для каждой команды вопрос будет решаться индивидуально, по согласованию с Оргкомитетом.



С дополнительным оргвзносом допускаются следующие команды:



<ol start="9">

  <li>

    9. Команды, входящие в ТОП20 рейтинга МАК (на 1.06.08), имеют право участвовать за свой счет + оргвзнос в размере 37500 руб.

  </li>

  <li>

    10. Команды, входящие в ТОП50 рейтинга МАК (на 1.06.08), имеют право участвовать за свой счет + оргвзнос в размере 75000 руб.

  </li>

  <li>

    11. Команды, не входящие в ТОП50 рейтинга МАК (на 1.06.08), имеют право участвовать за свой счет + оргвзнос в размере 125000 руб.

  </li>

</ol>



Чемпионаты стран должны быть проведены до 01.06.08, а информация о них, включающая регламент, турнирную таблицу и составы всех игравших команд, должна быть прислана на адрес mak-vip@yahoogroups.com с копией на адрес cla@pivdenny.odessa.ua до 10.06.08. Только в этом случае победитель чемпионата страны может быть допущен к участию в ЧМ.



В составе команды-участницы ЧМ должно быть не менее 4 игроков того состава, в котором она играла турнир, выведший ее на ЧМ, или базового состава этой команды в рейтинге МАК, если она проходит по пунктам 4, 8, 9, 10 . В ее составе должно быть не более 1 игрока, участвовавшего в составе других команд в турнирах, выводящих на ЧМ (к таким турнирам относятся ЧМ-2007, чемпионаты стран и этапы Кубка Мира сезона 2007-2008).



Обращаем внимание капитанов ВСЕХ команд, которые планируют принять участие в ЧИ -2008! Если чемпионат Вашей страны уже прошел, Вам необходимо не позднее 01.05.08 отправить свои контакты на rubin@amsgroup.ru или angor85@sport.com.ua для связи с Вами.



Заявки (с паспортными данными всех игроков) всех команд, прошедших по пунктам 1, 2, 3, 5, 6, 7 должны быть присланы на адреса rubin@amsgroup.ru и angor85@sport.com.ua до 24.00 МСК 15.06.2008. Для команд, прошедших по пунктам 4, 8, 9, 10, 11, крайняя дата и время приема приема заявки - до 24.00 МСК 10.07.08. В этом году никакие заявки, пришедшие после указанного срока, рассматриваться не будут.



Представителей команд, указанных в пунктах 6-8, просим связаться с Оргкомитетом по вышеуказанным электронным адресам не позднее 16.05 08 для урегулирования финансовых вопросов. В противном случае эти все команды едут СТРОГО за свой счет.



Запасные игроки указываются в общей заявке и едут СТРОГО за свой счет. После истечения срока приема заявок замены в них возможны только по согласованию с Оргкомитетом.



Вопрос оплаты виз будет решен на ближайшем заседании Правления МАК, о чем командам будет сообщено дополнительно.



С уважением, председатель Оргкомитета Рубин Александр