+++
title = "Сайт Чемпионата Мира"
date = 2012-08-31T13:55:31
author = "Константин Кноп"
guid = "http://mak-chgk.ru/?p=21297"
permalink = "/post_21297/"
categories = [ "Прочее",]
slug = "sait-chempionata-mira"
aliases = [ "/post_21297",]
type = "post"
tags = []
+++

Открылся сайт Чемпионата мира по ЧГК 2012 года. Его адрес - <a title="http://worldchamp.chgk.su/" href="http://worldchamp.chgk.su/" target="_blank">http://worldchamp.chgk.su/</a>