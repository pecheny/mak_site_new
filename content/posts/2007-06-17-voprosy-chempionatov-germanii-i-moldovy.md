+++
title = "Вопросы Чемпионатов Германии и Молдовы"
date = 2007-06-17T05:12:31
author = "roma7"
guid = "http://mak-chgk.ru/voprosy-chempionatov-germanii-i-moldovy/"
permalink = "/post_906/"
Source = [ "http://lists.chgk.info/z-info/200706/msg00013.html",]
categories = [ "Турниры",]
slug = "voprosy-chempionatov-germanii-i-moldovy"
aliases = [ "/post_906", "/voprosy-chempionatov-germanii-i-moldovy",]
type = "post"
tags = []
+++

Вопросы Чемпионатов Германии и Молдовы выставлены на [сайте немецкого ЧГК](http://www.znatoki.de/turniry/Vesna/ChG-Voprosy.html)



Вопросы обсуждались (обсуждаются) в ЖЖ немецкого ЧГК: [1 тур](http://community.livejournal.com/chgk_de/70455.html), [2 тур](http://community.livejournal.com/chgk_de/70677.html), [3 тур](http://community.livejournal.com/chgk_de/71043.html), [4 тур](http://community.livejournal.com/chgk_de/71246.html).



Если эти вопросы обсуждаются еще где-нибудь - сообщите мне, буду признателен.



_Паша Худяков, модератор сайта немецких знатоков_