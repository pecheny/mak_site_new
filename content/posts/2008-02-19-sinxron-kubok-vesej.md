+++
title = "Синхрон &#171;Кубок весей&#187;"
date = 2008-02-19T01:37:20
author = "DimoN"
guid = "http://mak-chgk.ru/sinxron-kubok-vesej/"
permalink = "/post_3038/"
categories = [ "Прочее",]
slug = "sinkhron-kubok-vesei"
aliases = [ "/post_3038", "/sinxron-kubok-vesej",]
type = "post"
tags = []
+++

**_<font size="4" face="Arial">Уважаемые знатоки!</font>_**



****



Вышеназванный чемпионат будет предложен общественности Бауманским клубом знатоков при участии Клуба знатоков МИФИ. Чемпионат лицензирован МАК. В Наблюдательный совет по синхронам (НаСоС) направлена заявка о курировании данного синхрона. В этой связи предполагается, что итоги чемпионата будут учтены в рейтинге МАК.



Основная дата проведения синхрона &#8212; 5-го апреля 2008 года (суббота). По согласованию с Оргкомитетом допустимо проведение отыгрыша в том или ином городе 6-го апреля. Все подробности читайте на странице чемпионата по адресу [<font color="#3f5f9e">http://hoster.bmstu.ru/~brain/BMSTU-Kub<wbr></wbr>Ves.htm<img src="http://i.ixnp.com/images/v3.16.1/t.gif" style="padding-right: 0px; background-position: -944px 0px; display: inline; padding-left: 0px; font-weight: normal; left: auto; float: none; background-image: url('http://i.ixnp.com/images/v3.16.1/theme/silver/palette.gif'); visibility: visible; padding-bottom: 0px; margin: 0px; vertical-align: top; width: 14px; line-height: normal; padding-top: 1px; background-repeat: no-repeat; font-style: normal; font-family: 'trebuchet ms', arial, helvetica, sans-serif; position: static; top: auto; height: 12px; background-color: transparent; text-decoration: none; cssfloat: none; border-width: 0px" id="snap_com_shot_link_icon" class="snap_preview_icon" /></font>](http://hoster.bmstu.ru/~brain/BMSTU-KubVes.htm){.snap_shots} .



ВОПРОСНИКАМ:



Прием вопросов для чемпионата осуществляется до конца февраля. Ваши вопросы (не более 10 от одного автора) шлите на <font size="1"><a href="mailto:bmstu-redactura@diar.ru">bmstu-redactura@diar.ru</a>  .</font>



<font size="1">Оплата вопросов, отыгранных в чемпионате, составит 100 рублей ($4) за вопрос.</font>