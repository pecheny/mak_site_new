+++
title = "Кубок вМоГоТУ-2010: объявление для вопросников и редакторов"
date = 2010-09-02T15:17:05
author = "DimoN"
guid = "http://mak-chgk.ru/post_13360/"
permalink = "/post_13360/"
categories = [ "Анонс мероприятий",]
slug = "kubok-vmogotu-2010-obiavlenie-dlia-voprosnikov-i-redaktorov"
aliases = [ "/post_13360", "/post_13360",]
type = "post"
tags = []
+++

Уважаемые коллеги!



Оргкомитет фестиваля, который намечено провести  30-го октября текущего года, начинает формирование пакета вопросов, а также редакторской группы, которая с ним плотно и продуктивно поработает. Приглашаем к сотрудничеству как старых и проверенных партнеров БКЗ, так и молодых и талантливых авторов и редакторов.



Актуальны вопросы для ЧГК и БР, а также разнообразные зрелищные и содержательные конкурсы аля &#8220;Даугавпилс&#8221;. Приветствуются свежий взгляд, богатая фактура и нетривиальные идеи.



Фестиваль в этом году пройдет под знаком 15-летия БКЗ, в связи с чем мы отдаем предпочтение &#8220;дареным коням&#8221;. С удовольствием посотрудничаем на бартерной основе и с оргкомитетами любых турниров, намеченных к проведению в ноябре-декабре: вы нам вопросы - мы вам полный пакет для отыгрыша.



Ваши вопросы, конкурсы и предложения по сотрудничеству присылайте до конца сентября на адрес dbs AT zmail tochka ru .



От имени Оргкомитета фестиваля Дмитрий Смирнов.