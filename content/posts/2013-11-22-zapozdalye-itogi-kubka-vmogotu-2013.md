+++
title = "Запоздалые итоги &#171;Кубка вМоГоТУ-2013&#187;"
date = 2013-11-22T20:25:29
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21711"
permalink = "/post_21711/"
categories = [ "Турниры",]
slug = "zapozdalye-itogi-kubka-vmogotu-2013"
aliases = [ "/post_21711",]
type = "post"
tags = []
+++

Очередной бауманский фестиваль, в котором приняли участие 53 команды из 13-ти городов России, стал достоянием истории!            



Главный приз фестиваля - Кубок вМоГоТУ - увезла с собой московская команда &#8220;Gambler&#8221;.



<img src="http://ic.pics.livejournal.com/dimonn/8105409/4475/4475_600.jpg" alt="Chempiones-2013" width="600" height="348" />



Серебряным призером фестиваля стала сборная команда &#8220;All-in&#8221;.



Третье место завоевала московская команда &#8220;Саморазгружающийся полувагон&#8221;.



Призёрская тройка в общем зачёте чемпионата по игре &#8220;Что? Где? Когда?&#8221; в этом году полностью совпала с призёрской тройкой зачёта &#8221;Этап Кубка России сезона 2013/2014 г.&#8221;:



  1. &#8220;Gambler&#8221; (Москва)



  2. &#8220;Саморазгружающийся полувагон&#8221; (Москва)



  3. &#8220;Уездный доктор&#8221; (Тверь)



Призёрская тройка зачета &#8220;Межрегиональный турнир&#8221;: 1. &#8221;Сорок пяток&#8221; (Москва)



  1. &#8220;Кровлю Кромвеля снесло&#8221; (Москва)



  2. &#8220;Раскольник OFF&#8221; (Коломна)



Призёрская тройка зачета &#8220;Межвузовский чемпионат Москвы&#8221;:



  1. &#8220;Дно&#8221; (НИУ ВШЭ)



  2. &#8220;МГТУ - Понятые&#8221; (МГТУ им.Н.Э.Баумана)



  3. &#8220;Доведённые до сарказма&#8221; (МГИМО)



Призёрская тройка по итогам интеллектуального многоборья:



  1. &#8220;All-in&#8221; (сборная)



  2. &#8220;Дважды на одни грабли&#8221; (Москва)



  3. &#8220;Уездный доктор&#8221; (Тверь)



Полные итоги фестиваля доступны  по адресу <a href="http://bmstu.ru/~brain/BMSTU-vMoGoTU-results-12.htm" target="_blank">http://bmstu.ru/~brain/BMSTU-vMoGoTU-results-13.htm</a>.



Обращаю внимание, что <a href="http://ratingnew.chgk.info/tournaments.php?displaytournament=2548" target="_blank">опубликованная на сайте Рейтинга МАК итоговая таблица</a> отличается от реальной в связи с тем, что рейтинговая комиссия МАК не принимает во внимание доппоказатель в виде суммарного рейтинга, тогда как мы этот доппоказатель учитывали согласно Регламенту фестиваля.



В то же время, начисление рейтинговых баллов, полученных командами на нашем чемпионате, и распределение команд по местам имеют расхождение с <a href="http://ratingnew.chgk.info/tournaments.php?displaytournament=2548&show_tours&page=1" target="_blank">таблицей плюсиков на сайте Рейтинга МАК</a>, т.к. при распределении во внимание принимались итоги &#8220;перестрелки&#8221; на &#8220;нулевых&#8221; вопросах, отобразить которые на сайте Рейтинга МАК технически не удалось. В общем, все сложно&#8230;



Благодарим всех наших гостей за участие, а призеров - за красивую, сильную и честную игру.



Ждем вас на наших следующих играх!



P.S. На обсуждение вопросов нашего фестиваля в открытых и публичных источниках установлен мораторий до 1 января 2014 г.



А эмоции, отзывы, критику и благодарности уже можно присылать либо в личку, либо выкладывать здесь или в своих персональных страничках, группах и сообществах.