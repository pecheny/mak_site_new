+++
title = "Мемориал Дмитрия Коноваленко"
date = 2009-01-21T22:22:16
author = "Константин Кноп"
guid = "http://mak-chgk.ru/post_6132/"
permalink = "/post_6132/"
categories = [ "Календарь",]
slug = "memorial-dmitriia-konovalenko"
aliases = [ "/post_6132", "/post_6132",]
type = "post"
tags = []
+++

<font size="+1" face="Arial,Helvetica"><em><strong>Мемориал Дмитрия Коноваленко</strong></em></font> Турнир, посвященный памяти Дмитрия Коноваленко, состоится 1.02.09 в БГЗ Дворца пионеров на Воробьевых горах.



Начало регистрации - 12.30. Начало игры - 13.00.



Регламент турнира: [<font color="#3f5f9e">http://www.potashev.ru/Reglament-2009.ht<wbr></wbr>ml<img src="http://i.ixnp.com/images/v3.64/t.gif" style="padding-right: 0px; background-position: -1128px 0px; min-width: 0px; display: inline; padding-left: 0px; font-weight: normal; min-height: 0px; left: auto; float: none; background-image: url('http://i.ixnp.com/images/v3.64/theme/silver/palette.gif'); visibility: visible; max-width: 2000px; padding-bottom: 0px; margin: 0px; vertical-align: top; width: 14px; max-height: 2000px; line-height: normal; padding-top: 1px; background-repeat: no-repeat; font-style: normal; font-family: 'trebuchet ms', arial, helvetica, sans-serif; position: static; top: auto; height: 12px; background-color: transparent; text-decoration: none; cssfloat: none; border-width: 0px" id="snap_com_shot_link_icon" class="snap_preview_icon" /></font>](http://www.potashev.ru/Reglament-2009.html){.snap_shots}.



Заявки можно оставлять в комментах к этому посту или слать на адрес max_po собака rambler.ru до 28.01.09. По тому же адресу до 25.01.09 продолжается прием вопросов.  Заявки от клубов на участие в неофициальном асинхронном турнире принимаются по тому же адресу до 31.01.09.



P.S. Участие в турнире платное - 600 руб. с команды. Все собранные деньги будут переданы Елене Коноваленко.