+++
title = "Молодежный кубок «Дружба народов» пройдет в Дубне"
date = 2017-09-29T12:22:10
author = "nail"
guid = "http://mak-chgk.ru/?p=29718"
permalink = "/post_29718/"
categories = [ "Анонс мероприятий", "Календарь",]
slug = "molodezhnyi-kubok-druzhba-narodov-proidet-v-dubne"
aliases = [ "/post_29718",]
type = "post"
tags = []
+++

Рады сообщить, что 9-10 декабря в Дубне (Московская область, Россия) состоится <a href="http://youthcupofnations.tilda.ws/" target="_blank">Молодежный кубок по интеллектуальным играм «Дружба народов»</a>. В турнире примут участие молодежные сборные стран СНГ и Восточной Европы, состав которых определится по итогам отборочного синхронного турнира <a href="http://youthcupofnations.tilda.ws/synchron" target="_blank">«Синхрон молодежного кубка &#8220;Дружба народов&#8221;»</a>.



В отборе смогут участвовать не только молодежные команды, но и все желающие. Взноса на синхроне не будет. Молодежными командами будут считаться те, всем игрокам которых на момент 10 декабря 2017 года не исполнилось 30 лет.



Сам синхрон пройдет 27-31 октября, редакторами трех туров по 15 вопросов стали <a title="Вопросы Михаила Иванова в Базе вопросов" href="https://db.chgk.info/person/mivanov" target="_blank">Михаил Иванов</a> — победитель конкурса Вопрос года по версии МАК, неоднократный редактор чемпионатов мира, России и других престижных турниров по Что? Где? Когда?, <a title="Вопросы Михаила Савченкова в Базе вопросов" href="https://db.chgk.info/person/msavchenkov" target="_blank">Михаил Савченков</a> — финалист конкурса Вопрос года по версии МАК, чемпион мира и России; <a title="Вопросы Станислава Мереминского в Базе вопросов" href="https://db.chgk.info/person/smereminsky" target="_blank">Станислав Мереминский</a> — чемпион мира и России. Взноса, как мы уже сказали, не будет. Зарегистрироваться можно <a href="http://rating.chgk.info/tournament/4563" target="_blank">на сайте рейтинга</a>. 







Регламент турнира можно прочитать <a href="http://youthcupofnations.tilda.ws/reglament" target="_blank">на нашем сайте</a>. Кроме того, на сайте и в социальных сетях турнира будут публиковаться интервью участников, редакторов, представления команд, разбор вопросов от опытных игроков и редакторов и много чего еще. Если вы следили за публикациями в пабликах Вконтакте <a href="https://vk.com/studchr2017" target="_blank">Студенческого чемпионата России-2017</a>, <a href="https://vk.com/kest2017" target="_blank">Студенческого кубка Европы-2017</a>, <a href="https://vk.com/chgkvk" target="_blank">ЧГК в ВК</a> и сайта <a href="https://www.chgk.gg" target="_blank">chgk.gg</a> вам понравится: мы собираемся взять с них пример и воплотить в жизнь лучшие идеи, которые позволят наслаждаться интеллектуальными играми не только участникам, но и сторонним наблюдателям.



Подписывайтесь на наши социальные сети: <a href="https://vk.com/youthcupofnations" target="_blank">Вконтакте</a>, <a href="https://www.facebook.com/youthnationscup/" target="_blank">Facebook</a>, <a href="https://twitter.com/youthnationscup" target="_blank">Twitter</a>, <a href="https://www.instagram.com/youthnationscup/" target="_blank">Instagram</a>.



Партнеры проекта: Ассоциация интеллектуальных клубов Азербайджана; телекомпания &#8220;Игра ТВ-Бел&#8221; (Республика Беларусь); Молдавский клуб интеллектуальных игр (г. Кишинев); Эстонский телеканал русскоязычного вещания ETV+; Государственный комитет Республики Мордовия по делам молодежи; Национальный исследовательский Мордовский государственный университет им. Н.П. Огарёва; Представительство Воронежской области в федеральных органах государственной власти Российской Федерации; ФГБОУ ВО «Воронежский государственный университет»; Лаборатория интеллектуальных технологий (г. Воронеж); Интеллектуальный клуб «Без дураков» (г. Москва)