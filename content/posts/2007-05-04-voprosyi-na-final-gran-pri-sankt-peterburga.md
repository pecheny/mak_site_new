+++
title = "Вопросы на финал Гран При Санкт-Петербурга"
date = 2007-05-04T09:10:51
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/04/voprosyi-na-final-gran-pri-sankt-peterburga/"
permalink = "/post_613/"
Source = [ "http://lists.chgk.info/z-info/200705/msg00008.html",]
categories = [ "Прочее",]
slug = "voprosy-na-final-gran-pri-sankt-peterburga"
aliases = [ "/post_613", "/2007/05/04/voprosyi-na-final-gran-pri-sankt-peterburga",]
type = "post"
tags = []
+++

Для финала молодежного чемпионата &#8220;Гран При СПб&#8221; по ЧГК и БР срочно необходимы вопросы. <!--more--> Вопросы должны быть средней степени сложности, тематика не ограничена, наличие источника информации обязательно. Вопросы должны быть &#8220;несвеченными&#8221;/свеченными в регионе, но отсутствующими в базе данных.



Оплата - 100 руб. за отобранный вопрос ЧГК, 60 руб. за отобранный вопрос БР.



Желающих прошу откликнуться по адресу: adrouz@mail.ru