+++
title = "Долгожданные результаты асинхронной вышки"
date = 2007-04-24T02:20:36
author = "roma7"
guid = "http://mak-chgk.ru/2007/04/24/dolgozhdannyie-rezultatyi-asinhronnoy-vyishki/"
permalink = "/post_594/"
Source = [ "http://lists.chgk.info/z-info/200704/msg00033.html",]
categories = [ "Прочее",]
slug = "dolgozhdannye-rezultaty-asinkhronnoi-vyshki"
aliases = [ "/post_594", "/2007/04/24/dolgozhdannyie-rezultatyi-asinhronnoy-vyishki",]
type = "post"
tags = []
+++

В начале апреля прошёл III асинхронный чемпионат по игре &#8220;Что? Где? Когда?&#8221; среди вузов &#8220;Интеллектуальная Вышка&#8221;. В турнире приняли участие 118 команды из 15 городов и 3 стран (Россия, Беларусь, Украина и Азербайджан). Чемпионат состоял из 36 вопросов (редактор Алексей Богословский).



<!--more-->



Топ 10 чемпионата:



  1. А-Элита (С.-Петербург) 30  

    

  2. Прочерк (Москва) 25  

    

  3. УГАТУ (Уфа) 23 (1573)  

    

  4. Руевит (Витебск) 23 (1493)  

    

  5. Квант (С.-Петербург) 23 (1476)  

    

  6. Ума Палата (Москва) 23 (1449)  

    

  7. НВ (СПб) 22 (1437)  

    

  8. 30тиногая сороконожка (Иркутск) 22 (1412)  

    

  9. Юпитер (С.-Петербург) 22 (1384)  

    

 10. МГТУ-Поехали! (Москва) 21 (1460)



Полную таблицу можно скачать [здесь](http://mak-chgk.ru/wp-content/uploads/2007/05/res_asinhr07.xls)



Оргкомитет поздравляет команду Санкт-Петербургского государственного университета Экономики и Финансов &#8220;А-Элита&#8221; (капитан Анастасия Степанова) с абсолютной победой как в очном, так и асинхронном чемпионате с очень большим отрывом от 2 места.



Все желающие могут получить пакет вопросов пока он не отправлен в базу.



Спасибо за приятные отзывы, ждём ваши города и команды на IV асинхронном чемпионате осенью 2007 года!



_Председатель Оргкомитета

  

Илья Трофимов._