+++
title = "Вопросы XII Чемпионата США  и 1-го Международного Кубка Америки"
date = 2007-05-01T19:31:28
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/01/voprosyi-xii-chempionata-ssha-i-1-go-mezhdunarodnogo-kubka-ameriki/"
permalink = "/post_580/"
Source = [ "http://lists.chgk.info/z-info/200704/msg00048.html",]
categories = [ "Прочее",]
slug = "voprosy-xii-chempionata-ssha-i-1-go-mezhdunarodnogo-kubka-ameriki"
aliases = [ "/post_580", "/2007/05/01/voprosyi-xii-chempionata-ssha-i-1-go-mezhdunarodnogo-kubka-ameriki",]
type = "post"
tags = []
+++

Опубликованы [вопросы XII Чемпионата США](http://us12.chgk.info/chgk_q/index.html) и [1-го Международного Кубка Америки (МКА)](http://mka.chgk.info/quest.htm):



Обратите внимание, что туры 1, 2 и 3 пакета МКА соответствуют турам 1, 3 и 4 (за исключением нулевого вопроса) пакета XII Чемпионата США.



_Константин Бриф_