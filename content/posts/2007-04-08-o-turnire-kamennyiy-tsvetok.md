+++
title = "О турнире Каменный Цветок"
date = 2007-04-08T10:10:08
author = "roma7"
guid = "http://mak-chgk.ru/2007/04/08/o-turnire-kamennyiy-tsvetok/"
permalink = "/post_604/"
Source = [ "http://lists.chgk.info/z-info/200704/msg00013.html",]
categories = [ "Турниры",]
slug = "o-turnire-kamennyi-tsvetok"
aliases = [ "/post_604", "/2007/04/08/o-turnire-kamennyiy-tsvetok",]
type = "post"
tags = []
+++

Турниру &#8220;Каменный цветок-2007&#8221; (Екатеринбург, 14-15 апреля) присвоен статус этапа Кубка России. <!--more--> Таким образом, лучшая команда этого турнира из числа играющих в зачёт этапа КР получит путёвку на Чемпионат России 2008 года.



_Председатель Оргкомитета чемпионата России

  

Константин Кноп_