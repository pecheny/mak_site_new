+++
title = "Решения по спорным ответам KG-2007"
date = 2007-04-06T16:07:09
author = "roma7"
guid = "http://mak-chgk.ru/2007/04/06/resheniya-po-spornyim-otvetam-kg-2007/"
permalink = "/post_605/"
Source = [ "http://lists.chgk.info/z-info/200704/msg00010.html",]
categories = [ "Турниры",]
slug = "resheniia-po-spornym-otvetam-kg-2007"
aliases = [ "/post_605", "/2007/04/06/resheniya-po-spornyim-otvetam-kg-2007",]
type = "post"
tags = []
+++

Решения по спорным ответам IX синхронного турнира &#8220;Кубок городов&#8221; [опубликованы на сайте](http://kubgor.chgk.info/2007/disputed.htm)



<!--more-->



Апелляции на зачет ответов принимаются от представителей в городах до 23:59 местного времени в понедельник, 9 апреля.



Апелляционное Жюри Кубка Городов:

  

Олег Леденев,

  

Евгений Поникаров,

  

Сергей Шихов.



_Оргкомитет Кубка Городов_