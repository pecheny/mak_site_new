+++
title = "Перерегистрация клубов 2018"
date = 2018-02-27T23:50:35
author = "Константин Кноп"
guid = "http://mak-chgk.ru/?p=29827"
permalink = "/post_29827/"
categories = [ "Правление и комиссии",]
slug = "pereregistratsiia-klubov-2018"
aliases = [ "/post_29827",]
type = "post"
tags = []
+++

Уважаемые коллеги!



Комиссия по работе в регионах и приему новых членов в рамках подготовки к Конгрессу МАК 2018 года просит клубы, входящие в МАК, до 30.04.2018 заполнить анкетную  <a title="Форма перерегистрации клубов МАК" href=" https://docs.google.com/forms/d/e/1FAIpQLScrPF7xFGOin5xBwcOl9pNuYfxE6S64WPkcx8pw-VCSrNJ1Kg/viewform" target="_blank">форму для прохождения перерегистрации</a>.



По всем возникающим вопросам можно писать председателю Комиссии Сергею Абрамову abragert@mail.ru



С уважением, Сергей Абрамов.



