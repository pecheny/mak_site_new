+++
title = "Заседание Правления МАК ЧГК 25.08.2006"
date = 2006-08-25T09:09:03
author = "admin"
guid = "http://mak-chgk.ru/?p=7"
permalink = "/post_7/"
categories = [ "Правление и комиссии",]
slug = "zasedanie-pravleniia-mak-chgk-25-08-2006"
aliases = [ "/post_7",]
type = "post"
tags = []
+++

<!--more--> 25 августа 2006 в Калининграде состоялось очередное заседание Правления МАК ЧГК.



На заседании присутствовали избранные Конгрессом члены Правления МАК: К.Алдохин Е.Алексеев В.Белкин А.Друзь Я.Зайдельман Е.Кисленкова А.Козлов Б.Крюк М.Левандовский М.Поташев А.Рубин Д.Соловьев Н.Стеценко Л.Черненко



Отсутствовал М.Мун. Голос М.Муна в открытых голосованиях был передан Е.Кисленковой.



Председателем заседания избран А.Козлов.



Правление рассмотрело следующие вопросы.



  1. О присуждении премий МАК за 2006 год.



Подробное изложение принятых по этому вопросу решений опубликовано в отдельном информационном сообщении.



  1. О проведении Кубка Наций 2006 года.



Правление утвердило следующий принцип формирования сборных команд стран-участниц Кубка Наций:



если в стране имеется единственный клуб-член МАК или же существует организованное объединение всех таких клубов, состав сборной определяет руководитель данного клуба или объединения;



если в стране действует более одного клуба-члена МАК и нет объединяющей их организации, состав сборной определяет капитан команды, выигравшей последний по времени чемпионат данной страны.



Данный порядок был принят поименным голосованием.



За: К.Алдохин, Е.Алексеев, В.Белкин, Я.Зайдельман, Е.Кисленкова, А.Козлов, М.Левандовский, М.Мун, М.Поташев, А.Рубин, Н.Стеценко, Д.Соловьев, Л.Черненко (13) Против: А.Друзь (1) Воздержались: Б.Крюк (1)



Правление рассмотрело вопрос о допуске к участию к Кубке Наций сборной Чехии. Было решено допустить сборную Чехии к участию, несмотря на отсутствие в Чехии клубов-членов МАК. Было рассмотрено два варианта допуска сборной Чехии:



(а) участие в Кубке вне зачета; (б) участие на общих основаниях.



По данному вопросу проводилось поименное голосование.



За вариант (а): Е.Алексеев, Е.Кисленкова, А.Козлов, М.Левандовский, М.Поташев, Н.Стеценко, Д.Соловьев, Л.Черненко (8) За вариант (б): К.Алдохин, В.Белкин, А.Друзь, Я.Зайдельман, Б.Крюк, А.Рубин (6) Воздержались: М.Мун



Сборная Чехии допущена к участию в Кубке Наций вне основного зачета.



Правление рассмотрело предложение о присвоении Кубку Наций наименования &#8220;Кубок Наций - чемпионат мира среди сборных стран&#8221;. По данному вопросу проводилось поименное голосование.



За: А.Друзь, А.Козлов, Б.Крюк, А.Рубин, Н.Стеценко, Л.Черненко (6) Против: К.Алдохин, Е.Алексеев, Е.Кисленкова, М.Левандовский, М.Мун, М.Поташев, Д.Соловьев (7) Воздержались: В.Белкин, Я.Зайдельман (2)



Решение не принято.



  1. О приеме новых членов МАК.



По представлению председателя комиссии по работе с регионами и приему В.Белкина Правление приняло в члены МАК Волгоградский клуб интеллектуальных игр.



  1. Об участниках молодежных турниров



По представлению председателя комиссии по работе с детьми и молодежью К.Алдохина Правление утвердило предложенные комиссией положения о молодежных турнирах сезона 2006/2007:



1) До утверждения постоянно действующего положения о молодежных турнирах считать данный сезон переходным. 2) Разбиение участников молодежных турниров на категории осуществляется только по возрасту. 3) Для сезона 2006/2007 г.г. установить следующие единые возрастные границы: - группа &#8220;А&#8221; - родившиеся 01.09.1991 и позже; - группа &#8220;Б&#8221; - родившиеся 01.09.1989 и позже; - группа &#8220;В&#8221; - родившиеся 01.09.1986 и позже; - группа &#8220;Г&#8221; - родившиеся 01.09.1983 и позже.