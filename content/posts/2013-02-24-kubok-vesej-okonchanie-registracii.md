+++
title = "&#171;Кубок весей&#187;: окончание регистрации"
date = 2013-02-24T21:33:13
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21504"
permalink = "/post_21504/"
categories = [ "Анонс мероприятий",]
slug = "kubok-vesei-okonchanie-registratsii"
aliases = [ "/post_21504",]
type = "post"
tags = []
+++

<div>

</div>



<div>

  </p> 

  

  <p>

    Уважаемые представители городов и особенно весей! 

    

    <div>

    </div>

    

    <div>

    </div> Напоминаю о том, что с 1-го по 3-е марта 2013 г. Бауманским клубом знатоков (БКЗ)

    

    <div>

    </div> будет проведен традиционный синхронный чемпионат по игре &#8220;Что? Где? Когда?&#8221; &#8220;Кубок весей&#8221;.

    

    <div>

    </div>

    

    <div>

    </div>

    

    <a href="http://dimonn.livejournal.com/pics/catalog/320/362" target="_blank"><img src="http://ic.pics.livejournal.com/dimonn/8105409/362/362_original.jpg" alt="CupVes2013" width="465" height="283" /></a>

  </p>

  

  <p>

    </div> 

    

    <div>

      Пакет из 36-40 вопросов для вас подготовит редакторская группа в составе: Евгений Куфтинов, Галина Наумова, Дмитрий Свинтицкий, Дмитрий Смирнов. 

      

      <div>

      </div>

      

      <div>

      </div> Чемпионат лицензирован МАК и проводится через сайт рейтинга МАК, поэтому его результаты будут рейтинговаться. 

      

      <div>

      </div> Взнос за участие в игре составит 210 рублей с команды.</p> 

      

      <p>

        С нами уже 37 площадок в 12 странах мира. Присоединяйтесь! 

        

        <div>

        </div> Регистрация на сайте рейтинга 

        

        <a href="http://ratingnew.chgk.info/synch.php?requests&idtournament=2261" target="_blank">закроется завтра</a> 

        

        <div>

        </div> (для подачи заявки необходимо авторизоваться).

      </p>

      

      <p>

        Председатель Оргкомитета &#8220;Кубка весей-2013&#8221; 

        

        <div>

        </div> Дмитрий Смирнов.

      </p>

      

      <p>

        </div>

      </p>