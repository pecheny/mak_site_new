+++
title = "&#171;Мемориал Диара Туганбаева&#187;: заканчивается регистрация площадок"
date = 2013-05-24T11:51:57
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21594"
permalink = "/post_21594/"
categories = [ "Анонс мероприятий",]
slug = "memorial-diara-tuganbaeva-zakanchivaetsia-registratsiia-ploshchadok"
aliases = [ "/post_21594",]
type = "post"
tags = []
+++

<span>Уважаемые знатоки!</span>



<span>Завершается регистрация площадок для отыгрыша рейтингового синхронного чемпионата по игре "Что? Где? Когда?" - <a href="http://chgk.livejournal.com/2432804.html" target="_blank">"Мемориал Диара Туганбаева"</a>, проводимого уже во второй раз Бауманским клубом знатоков в знак светлой памяти безвременно ушедшего нашего <a href="http://diar.ru/diar/chgk/" rel="nofollow" target="_blank">одноклубника и товарища</a>.</span>



<img alt="" border="0" src="http://img1.liveinternet.ru/images/attach/c/2//69/809/69809302_romka.jpg" width="300" />



<span>Редакторская группа: Дмитрий Смирнов (Москва), Дмитрий Свинтицкий (Могилёв).</span> <span>Вас ждут 3 тура по 12 вопросов.</span> <span><font color="#000000" face="Times New Roman" size="3"><span style="line-height: 20px">Сроки проведения: 1-3 июня 2013 года.</span></font></p> 



<p>

  <font color="#000000" face="Times New Roman" size="3"><span style="line-height: 20px">Игровой взнос составит 300 рублей с команды в пересчете на основные валюты.</span></font> <font color="#000000" face="Times New Roman" size="3"><span style="line-height: 20px">Часть собранных средств будет передана вдове Диара.</span></font></span>

</p>



<p>

  <span>Чемпионат лицензирован МАК, подана заявка на получение эгиды НаСоСа</span><span><font color="#000000" face="Times New Roman" size="3"><span style="line-height: 20px">.</span></font></span>

</p>



<p>

  <span>Регистрация площадок и прием отчетов об игре осуществляется через <a href="http://ratingnew.chgk.info/synch.php?add_request&idtournament=2326" rel="nofollow" target="_blank">специальную страничку</a> на сайте Рейтинга МАК.</span>

</p>