+++
title = "Расписание VI Открытого Чемпионата Твери и Тверской области по интеллектуальным играм."
date = 2007-05-10T16:27:10
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/10/raspisanie-vi-otkryitogo-chempionata-tveri-i-tverskoy-oblasti-po-intellektualnyim-igram/"
permalink = "/post_627/"
Source = [ "http://community.livejournal.com/chgk/579157.html",]
categories = [ "Прочее",]
slug = "raspisanie-vi-otkrytogo-chempionata-tveri-i-tverskoi-oblasti-po-intellektualnym-igram"
aliases = [ "/post_627", "/2007/05/10/raspisanie-vi-otkryitogo-chempionata-tveri-i-tverskoy-oblasti-po-intellektualnyim-igram",]
type = "post"
tags = []
+++

<!--more-->



11.00 - 12.00 Регистрация команд.



12.00 - 12.20 Торжественное открытие турнира. Выступление:



Губернатор Тверской области Д.В.Зеленин 



Глава города Твери О.С. Лебедев



Ректор Тверского Госуниверситета 



12.20 - 13.00 1 тур ЧГК.



13.00 - 13.20 Перерыв. Эрудит лотто.



13.20 - 14.00 2 тур ЧГК.



14.00 - 14.15 Перерыв.



14.15 - 15.00 3 тур ЧГК.



15.00 - 15.10 Подведение предварительных итогов.



15.10 - 16.10 Брейн-ринг для лучших 8 команд



16.10 - 16.40 Награждение участников.