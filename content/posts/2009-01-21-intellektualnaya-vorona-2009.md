+++
title = "Интеллектуальная Ворона &#8212; 2009"
date = 2009-01-21T22:56:54
author = "Константин Кноп"
guid = "http://mak-chgk.ru/post_6139/"
permalink = "/post_6139/"
categories = [ "Календарь",]
slug = "intellektualnaia-vorona-2009"
aliases = [ "/post_6139", "/post_6139",]
type = "post"
tags = []
+++

Пятый ежегодный турнир «Интеллектуальная ворона» состоится 8 февраля 2009 года.



В программе турнира 60 вопросов «Что? Где? Когда?». Редакторы пакета – Константин Науменко (Киев) и Антон Губанов (Санкт-Петербург).



Заявки принимаются до 31 января включительно по адресу int.vorona@gmail.com



Турнир пройдет в здании ГУ-ВШЭ на Кирпичной улице (близ м.Семеновская, там же, где ИВ-2008).