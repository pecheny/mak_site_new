+++
title = "&#171;Ветер удачи&#187; сопутствовал &#171;Ноеву ковчегу&#187;"
date = 2013-05-24T11:50:04
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21592"
permalink = "/post_21592/"
categories = [ "Прочее",]
slug = "veter-udachi-soputstvoval-noevu-kovchegu"
aliases = [ "/post_21592",]
type = "post"
tags = []
+++

<p style="color: #000000;font-family: Verdana;font-size: 13px;line-height: normal;background-color: #ffffff">

  <span style="font-family: georgia, serif"><span style="font-size: 10pt">Уважаемые знатоки!</span></span>

</p>



<p style="color: #000000;font-family: Verdana;font-size: 13px;line-height: normal;background-color: #ffffff">

  <span style="font-family: georgia, serif"><span style="font-size: 10pt">Подведены <a href="http://ratingnew.chgk.info/tournaments.php?displaytournament=2325" rel="nofollow" target="_blank">итоги</a></span></span> приуроченного к недавно прошедшему 18-летию Бауманского клуба знатоков (БКЗ) <span style="font-family: georgia, serif"><span style="font-size: 10pt">международного рейтингового синхронного чемпионата по игре “Что? Где? Когда?” под символическим названием “18+”, в котором приняли участие 82 команды из 20-ти городов мира.</span></span>

</p>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">В этот раз на пьедестале почёта сразу 2 команды: </span></span> <span style="font-family: georgia, serif"><span style="font-size: 10pt">московская - <strong>&#8220;Ветер удачи&#8221;</strong></span></span>_<span style="font-family: georgia, serif"><span style="font-size: 10pt"><strong> </strong>(Иван Митрофанов, Мария Наркевич, Юрий Наркевич, Алексей Сироткин, Андрей Севастьянов, Александра Семёнова)</span></span>_ <span style="font-family: georgia, serif"><span style="font-size: 10pt">и питерская - <strong>&#8220;Ноев ковчег&#8221;</strong></span></span>_<span style="font-family: georgia, serif"><span style="font-size: 10pt"><strong> </strong>(Андрей Кузьма, Ольга Кузьма, Дмитрий Петров, Лев Орлов, Светлана Орлова)</span></span>_



<img src="http://ic.pics.livejournal.com/dimonn/8105409/3374/3374_600.jpg" alt="Ковчег2" width="600" height="453" />



<span style="font-family: georgia, serif"><span style="font-size: 10pt">Благодарю:</span></span> <span style="font-size: 10pt;font-family: georgia, serif">&#8212;  всех участников синхрона за сильную игру и всего 4 апелляции;</span> <span style="font-family: georgia, serif"><span style="font-size: 10pt">&#8212; Максима Евланова (Харьков), Галину Наумову (Москва), Ирину Юдакову (Москва) за работу в АЖ;</span></span> <span style="font-family: georgia, serif"><span style="font-size: 10pt">&#8212; хранителей рейтинга МАК за оперативное техническое сопровождение турнира.</span></span>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">До встречи на июньском синхроне <a href="http://ratingnew.chgk.info/synch.php?add_request&idtournament=2326" target="_blank">&#8220;Мемориал Диара Туганбаева&#8221;</a>.</span></span>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">От имени Оргкомитета “18+” Дмитрий Смирнов.</span></span>



_<span style="font-family: georgia, serif"><span style="font-size: 10pt">P.S. Обращаюсь к гениям интернет-поиска: пожалуйста, найдите указ президента РФ, которым утверждена дата празднования с 1997 г. Дня служителя маяка (маячника). А то лично я для себя пока еще не поставил точку в вопросе, когда же стоит отмечать сие знаменательное событие.</span></span>_