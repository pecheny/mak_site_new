+++
title = "Кубок ЛКИ по &#171;Что? Где? Когда?&#187;: подробности"
date = 2007-05-30T02:19:35
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/30/kubok-lki-po-chto-gde-kogda-podrobnosti/"
permalink = "/post_776/"
Source = [ "http://community.livejournal.com/chgk/595196.html",]
categories = [ "Турниры",]
slug = "kubok-lki-po-chto-gde-kogda-podrobnosti"
aliases = [ "/post_776", "/2007/05/30/kubok-lki-po-chto-gde-kogda-podrobnosti",]
type = "post"
tags = []
+++

Регламент кубка журнала &#8220;Лучшие компьютерные игры&#8221; по ЧГК, список заявившихся (на сегодняшний день) участников, подробности о месте и времени проведения можно найти на [странице турнира](http://erudit.lki.ru/text.php?id=11). По завершении турнира там же будут опубликованы результаты.