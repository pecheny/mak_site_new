+++
title = "ОК ЛУК 2006/2007"
date = 2007-05-09T00:00:19
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/09/anons-ok-luk-20062007/"
permalink = "/post_625/"
Source = [ "http://luk.chgk.info/index.php?option=com_content&task=view&id=160&Itemid=28",]
url = [ "95",]
categories = [ "Календарь", "Турниры",]
slug = "ok-luk-2006-2007"
aliases = [ "/post_625", "/2007/05/09/anons-ok-luk-20062007",]
type = "post"
tags = []
+++

Правление ЛУК приняло решение о проведении Олимпийского Кубка ЛУК сезона 2006/07 гг. в г. Кривой Рог 9-10 июня



Там же будет проведен новый турнир - Суперигра для лучших украинских команд. Формат этого соревнования еще определяется. Более детальная информация о квотах, приеме отчетов об отборах и пр. будет выставлена на этот лист позже.



_Владислав Карнацевич_