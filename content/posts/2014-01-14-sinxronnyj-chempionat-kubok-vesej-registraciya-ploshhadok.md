+++
title = "Синхронный чемпионат &#171;Кубок весей&#187;: регистрация площадок"
date = 2014-01-14T10:37:06
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21738"
permalink = "/post_21738/"
categories = [ "Анонс мероприятий",]
slug = "sinkhronnyi-chempionat-kubok-vesei-registratsiia-ploshchadok"
aliases = [ "/post_21738",]
type = "post"
tags = []
+++

Уважаемые знатоки!



Напоминаю вам о том, что с 1-го по 3-е февраля 2014 г. Бауманским клубом знатоков (БКЗ) будет проведен <a href="http://chgk.wikia.com/wiki/%D0%9A%D1%83%D0%B1%D0%BE%D0%BA_%D0%B2%D0%B5%D1%81%D0%B5%D0%B9" target="_blank">традиционный</a> синхронный чемпионат по игре &#8220;Что? Где? Когда?&#8221; <a href="http://vk.com/event64485313" target="_blank">&#8220;Кубок весей&#8221;</a>.



<a href="http://dimonn.livejournal.com/pics/catalog/320/362" target="_blank"><img src="http://ic.pics.livejournal.com/dimonn/8105409/362/362_original.jpg" alt="CupVes2013" width="465" height="283" /></a>



Пакет из 36-ти несложных, но интересных вопросов для вас подготовит редакторская группа в составе: Виктор Мялов, Дмитрий Свинтицкий, Дмитрий Смирнов, Юрий Чулинин.



Чемпионат будет проводиться через сайт рейтинга МАК, поэтому его результаты будут рейтинговаться.



<span style="line-height: 1.4;">Взнос за участие в игре составит 240 рублей с команды. </span>



Регистрация на сайте рейтинга уже <a href="http://ratingnew.chgk.info/synch.php?requests&idtournament=2724" target="_blank">открыта</a> (для подачи заявки необходимо авторизоваться).



Председатель Оргкомитета &#8220;Кубка весей - 2014&#8221; Дмитрий Смирнов.