+++
title = "Кубок закрытия сезона &#8212; 16-й Открытый чемпионат Москвы среди школьников по игре &#171;Что? Где? Когда?&#187;"
date = 2007-05-07T19:39:51
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/21/kubok-zakryitiya-sezona-16-y-otkryityiy-chempionat-moskvyi-sredi-shkolnikov-po-igre-chto-gde-kogda/"
permalink = "/post_699/"
url = [ "http://mak-chgk.ru/category/competitions/youth/shchm/",]
categories = [ "Календарь", "Прочее", "Турниры",]
slug = "kubok-zakrytiia-sezona-16-i-otkrytyi-chempionat-moskvy-sredi-shkolnikov-po-igre-chto-gde-kogda"
aliases = [ "/post_699", "/2007/05/21/kubok-zakryitiya-sezona-16-y-otkryityiy-chempionat-moskvyi-sredi-shkolnikov-po-igre-chto-gde-kogda",]
type = "post"
tags = []
+++

Кубок закрытия сезона. 16-й Открытый чемпионат Москвы среди школьников по игре &#8220;Что? Где? Когда?&#8221; Состоится 27-го мая в 12 часов в Большом гостином зале МГДД(Ю)Т - м. Воробьевы Горы.



<!--more-->



Планируется - ЧГК - 3 тура по 12 вопросов, Брэйн - для 6 лучших команд ЧГК.



Заявки только от ШКОЛЬНЫХ КОМАНД принимаются на mosoml2005@yandex.ru



Чемпионат не является закрытым - потому есть возможность подать заявки иногородним командам.



В заявке указать - название, город, состав участников(дата рождения, место учебы).