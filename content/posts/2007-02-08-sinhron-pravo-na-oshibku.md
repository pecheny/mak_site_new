+++
title = "Синхрон &#171;Право на ошибку&#187;"
date = 2007-02-08T14:10:38
author = "roma7"
guid = "http://mak-chgk.ru/2007/02/08/sinhron-pravo-na-oshibku/"
permalink = "/post_565/"
Source = [ "http://lists.chgk.info/z-info/200702/msg00008.html",]
categories = [ "Турниры",]
slug = "sinkhron-pravo-na-oshibku"
aliases = [ "/post_565", "/2007/02/08/sinhron-pravo-na-oshibku",]
type = "post"
tags = []
+++

Уважаемые игроки, авторы вопросов и организаторы турниров!



Предлагаю ознакомиться с проектом нового синхрона &#8220;Право на ошибку&#8221;. <!--more--> Основная страничка - 



<http://www.chgk.info/~knop/write_2b_rong/>



Ориентировочные даты проведения - 7-8 апреля 2007.



Сейчас на сайте выложены:



Черновик регламента &#8212; прочтите обязательно, в нем много существенных отличий от привычных синхронных турниров



<http://www.chgk.info/~knop/write_2b_rong/rules.html>



Приглашение для авторов &#8212; прочтите обязательно, есть важные отличия от стандартных авторских анонсов



<http://www.chgk.info/~knop/write_2b_rong/anons.html>



&#8220;Как подать заявку&#8221; &#8212; информация для местных представителей



<http://www.chgk.info/~knop/write_2b_rong/how_to.html>



Здесь нет никаких особых новаций.



Присылать вопросы и заявляться на участие можно уже сейчас, хотя сам турнир, возможно, переместится на запасную дату &#8212; 9-10 июня 2007. Первые списки заявившихся я выложу не ранее 1 марта.



_Константин Кноп_