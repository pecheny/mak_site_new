+++
title = "XIV съезд ЛУК"
date = 2016-10-26T14:09:50
author = "Константин Кноп"
guid = "http://mak-chgk.ru/?p=29610"
permalink = "/post_29610/"
categories = [ "Прочее",]
slug = "xiv-sezd-luk"
aliases = [ "/post_29610",]
type = "post"
tags = []
+++

14 октября в Запорожье состоялся XIV съезд ЛУК.



По его итогам, в частности, изменился состав органов управления ЛУК: из состава Правления ЛУК были выведены Елена Кочемировская, подавшая в отставку 29.07.2015 г., и Андрей Дихтяренко, подавший в отставку 01.01.2016 г. В Правление избран Андрей Задворнов. Из состава ревизионной комиссии ЛУК вышел Евгений Духопельников, в РК избрана Марина Рехтман.



Съезд принял изменения к Уставу ЛУК, приводящие его в соответствие с Налоговым кодексом.



Съезд рассмотрел вопрос взаимоотношений ЛУК и МАК и принял следующее заявление: «В августе 2016 года в СМИ (источник: <http://ria.ru/syria/20160815/1474342509.html>) появилась информация о проведении Президентом МАК А.Козловым турнира по интеллектуальным играм для военнослужащих Российской Федерации в Сирии. Также вице-президент МАК Б.Крюк в интервью изданию „Московский комсомолец“ (источник: <http://www.mk.ru/culture/2016/08/17/boris-kryuk-kak-odet-znatokov-v-smokingi.html>) увязал исключение Ильи Новикова из телеклуба знатоков с его публичной профессиональной деятельностью, связанной с защитой Надежды Савченко. Мы считаем, что господа А.Козлов и Б.Крюк, являясь официальными лицами МАК, не имеют права проявлять политическую ангажированность. ЛУК, как коллективный член МАК, заявляет, что подобные действия руководства МАК нарушают подтверждённый на Конгрессе МАК в 2014 году принцип „МАК ЧГК – вне политики“, дискредитируют саму идею Международной ассоциации клубов. На основании этого ЛУК требует отставки президента МАК А.Козлова и вице-президента МАК Б.Крюка с занимаемых ими в МАК должностей».



Съезд также принял решение приостановить членство ЛУК в МАК до проведения ближайшего Конгресса МАК.



Из ЛУК исключены А.Вассерман и А.Скрипцов. Почётными членами ЛУК избраны Борис Бурда, Игорь Флору и Александр Дануца. 







(Источник: <http://luk.org.ua/announcements/485/xiv-sezd-luk>)