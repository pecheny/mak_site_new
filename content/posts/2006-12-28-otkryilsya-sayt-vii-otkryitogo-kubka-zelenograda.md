+++
title = "Открылся сайт VII Открытого кубка Зеленограда"
date = 2006-12-28T12:00:10
author = "roma7"
guid = "http://mak-chgk.ru/2006/12/28/otkryilsya-sayt-vii-otkryitogo-kubka-zelenograda/"
permalink = "/post_298/"
categories = [ "Турниры",]
slug = "otkrylsia-sait-vii-otkrytogo-kubka-zelenograda"
aliases = [ "/post_298", "/2006/12/28/otkryilsya-sayt-vii-otkryitogo-kubka-zelenograda",]
type = "post"
tags = []
+++

По адресу http://okz2007.narod.ru открылся сайт VII Открытого кубка Зеленограда.



На сайте можно ознакомиться с Регламентом турнира, его расписанием, поступившими заявками и другой информацией.



Кубок состоится 13 января 2007 года. Приём заявок пока продолжается.