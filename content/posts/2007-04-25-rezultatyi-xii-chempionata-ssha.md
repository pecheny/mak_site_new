+++
title = "Результаты XII чемпионата США"
date = 2007-04-25T04:28:44
author = "roma7"
guid = "http://mak-chgk.ru/2007/04/25/rezultatyi-xii-chempionata-ssha/"
permalink = "/post_587/"
Source = [ "http://lists.chgk.info/z-info/200704/msg00036.html",]
categories = [ "Турниры",]
slug = "rezultaty-xii-chempionata-ssha"
aliases = [ "/post_587", "/2007/04/25/rezultatyi-xii-chempionata-ssha",]
type = "post"
tags = []
+++

На веб-сайте XII чемпионата США опубликованы основные результаты [&#8220;Что? Где? Когда?&#8221;](http://us12.chgk.info/chgk_res/index.html), [отборочного зачёта ЧМ-2007](http://us12.chgk.info/chgk_res/chm_otbor.html), [&#8220;Брейн-ринга&#8221;](http://us12.chgk.info/br_res/index.html), [&#8220;Своей Игры&#8221;](http://us12.chgk.info/si_res/index.html), [&#8220;Эрудит-квартета&#8221;](http://us12.chgk.info/ek_res/index.html), и [&#8220;Кубка Зодиака&#8221;](http://us12.chgk.info/kz_res/index.html). 



Более подробная информация о результатах будет опубликована в ближайшее время.



_Константин Бриф_