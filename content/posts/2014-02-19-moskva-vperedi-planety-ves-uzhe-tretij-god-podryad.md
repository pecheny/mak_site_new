+++
title = "Москва &#8212; впереди планеты весь! Уже третий год подряд&#8230;"
date = 2014-02-19T13:01:18
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21766"
permalink = "/post_21766/"
categories = [ "Прочее",]
slug = "moskva-vperedi-planety-ves-uzhe-tretii-god-podriad"
aliases = [ "/post_21766",]
type = "post"
tags = []
+++

<span style="color: black">Уважаемые знатоки!</span>



<span style="color: black">Подведены <a href="http://ratingnew.chgk.info/tournaments.php?displaytournament=2724" target="_blank">итоги</a> международного рейтингового синхронного чемпионата по игре “Что? Где? Когда?”</span> <span style="color: black">под символическим названием “Кубок весей”, в котором приняли участие 22 “веси” пяти государств.</span>



<span style="color: black">На этот раз чемпионов у нас два – команда «Азык Тулек» из Рязани и питерская команда «Ноев ковчег». Поздравляем сильнейших, которым поддались по 26 вопросов из 36-ти.</span>



<img src="http://ic.pics.livejournal.com/dimonn/8105409/7391/7391_600.jpg" alt="6t354w0reqd6" width="576" height="457" />



<span style="color: black">Отдельно был подведен “весевой зачет”*, в котором третий год подряд уверенно победила Москва.</span> <span style="color: black">Второе место у Тюмени. Бронзовым призером в этот раз стал казахстанский город Актау.</span>



<span style="color: black">====================================</span> 



<table style="border-collapse: collapse;width: 192pt" width="256" border="0" cellspacing="0" cellpadding="0">

  <tr style="height: 15.0pt">

    <td style="height: 15.0pt;width: 48pt" width="64" height="20">

      Место

    </td>

    

    <td class="xl65" style="width: 48pt" width="64">

      &#8220;Весь&#8221;

    </td>

    

    <td class="xl65" style="width: 48pt;padding-bottom: 0cm;padding-top: 0cm" width="64">

      Сумма

    </td>

    

    <td class="xl65" style="width: 48pt;padding-bottom: 0cm;padding-top: 0cm" width="64">

      Команд

    </td>

  </tr></p> 

  

  <h2>

    </table>

  </h2>

  

  <p>

    <table style="border-collapse: collapse;width: 192pt" width="256" border="0" cellspacing="0" cellpadding="0">

      <col style="width: 48pt" span="4" width="64" /> <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt;width: 48pt" width="64" height="20">

          1

        </td>

        

        <td class="xl65" style="width: 48pt" width="64">

          Москва

        </td>

        

        <td class="xl65" style="width: 48pt;padding-bottom: 0cm;padding-top: 0cm" width="64">

          87

        </td>

        

        <td class="xl65" style="width: 48pt;padding-bottom: 0cm;padding-top: 0cm" width="64">

          28

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          2

        </td>

        

        <td class="xl65">

          Тюмень

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          65

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          4

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          3

        </td>

        

        <td class="xl65">

          Актау

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          53

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          5

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          4

        </td>

        

        <td class="xl65">

          Астана

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          51

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          7

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          5

        </td>

        

        <td class="xl65">

          Омск

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          44

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          4

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          6

        </td>

        

        <td class="xl65">

          Хабаровск

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          44

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          5

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          7

        </td>

        

        <td class="xl65">

          Калининград

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          36

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          3

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          8-9

        </td>

        

        <td class="xl65">

          Рязань

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          26

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          8-9

        </td>

        

        <td class="xl65">

          Санкт-Петербург

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          26

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          10

        </td>

        

        <td class="xl65">

          Волгоград

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          22

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          11-12

        </td>

        

        <td class="xl65">

          Горки

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          21

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          11-12

        </td>

        

        <td class="xl65">

          Тула

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          21

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          13

        </td>

        

        <td class="xl65">

          Одесса

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          20

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          14

        </td>

        

        <td class="xl65">

          Могилёв

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          19

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          15

        </td>

        

        <td class="xl65">

          Уфа

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          18

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          16

        </td>

        

        <td class="xl65">

          Егорьевск

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          17

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          17

        </td>

        

        <td class="xl65">

          Денвер

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          16

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          18

        </td>

        

        <td class="xl65">

          Усть-Каменогорск

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          15

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          19

        </td>

        

        <td class="xl65">

          Жуковский

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          14

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          20

        </td>

        

        <td class="xl65">

          Ростов-на-Дону

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          13

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          21

        </td>

        

        <td class="xl65">

          Липецк

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          10

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr>

      

      <tr style="height: 15.0pt">

        <td class="xl66" style="height: 15.0pt" height="20">

          22

        </td>

        

        <td class="xl65">

          Владивосток

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          7

        </td>

        

        <td class="xl65" style="padding-bottom: 0cm;padding-top: 0cm">

          1

        </td>

      </tr></p> 

      

      <h2>

        </table>

      </h2>

      

      <p>

        <span style="color: black">*в зачет “весевого зачета” бралась сумма правильных ответов до 4-х лучших команд каждого населенного пункта</span>

      </p>

      

      <p>

        <span style="color: black">Благодарю:</span> <span style="color: black">&#8212; всех авторов, редакторов и тестеров вопросов за качественный контент;</span> <span style="color: black">&#8212;  всех участников синхрона за сильную игру и всего 2 апелляции;</span> <span style="color: black">&#8212; Максима Евланова (Харьков), Михаила Локшина (Санкт-Петербург), Владимира Грамагина (Нью-Йорк) и Леонида Климовича (Гомель) за работу в АЖ;</span> <span style="color: black">&#8212; хранителей рейтинга МАК за оперативное техническое сопровождение турнира.</span>

      </p>

      

      <p>

        <span style="color: black">Напоминаю, что вдогонку к прошедшему рейтинговому синхрону мы предлагаем до конца месяца отыграть в знак солидарности с олимпийцами тематический нерейтинговый асинхронный чемпионат <a href="http://chgk.livejournal.com/2598725.html" target="_blank">&#8220;Блиц-марафон, или Олимпийский перебор&#8221;</a> по необычным правилам. И пусть победит сильнейший!</span>

      </p>

      

      <p>

        <span style="color: black">От имени Оргкомитета “Кубка весей”</span> <span style="color: black">Дмитрий Смирнов.</span>

      </p>