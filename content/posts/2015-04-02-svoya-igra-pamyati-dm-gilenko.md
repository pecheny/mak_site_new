+++
title = "&#171;Своя игра&#187; памяти Дм. Гиленко"
date = 2015-04-02T21:13:39
author = "Константин Кноп"
guid = "http://mak-chgk.ru/?p=22130"
permalink = "/post_22130/"
categories = [ "Прочее",]
slug = "svoia-igra-pamiati-dm-gilenko"
aliases = [ "/post_22130",]
type = "post"
tags = []
+++

Группа редакторов (Вахрив, Дружинин, Кудрявцев, Солахян, Фарукшин, Шляхов, Шойхет) под началом Бориса Юхвеца формируют пакет-мемориал памяти Дмитрия Гиленко. Пакет будет состоять примерно из 100 тем и распространяться на бесплатной основе. Если вы хотите внести свою лепту в этот проект, отправьте 1-2 темы на адрес Бориса Юхвеца: ioukhvets@freenet.de Гонорары авторам тем не предусмотрены.



