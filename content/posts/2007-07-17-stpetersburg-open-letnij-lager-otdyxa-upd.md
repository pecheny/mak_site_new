+++
title = "St.Petersburg Open: Летний лагерь отдыха. upd."
date = 2007-07-17T05:22:43
author = "roma7"
guid = "http://mak-chgk.ru/stpetersburg-open-letnij-lager-otdyxa-upd/"
permalink = "/post_907/"
categories = [ "Прочее",]
slug = "st-petersburg-open-letnii-lager-otdykha-upd"
aliases = [ "/post_907", "/stpetersburg-open-letnij-lager-otdyxa-upd",]
type = "post"
tags = []
+++

На \[http://www.aerobrain.info/\](сайте клуба &#8220;Аэробрэйн&#8221;) опубликован список поступивших на 16:00 17 июня заявок на участие в лагере интеллектуальных игр &#8220;Белая дача - 2007&#8221;: http://www.aerobrain.info/lager/people.html



Лагерь пройдет с 9 по 19 июля. Место проведения - лагерь &#8220;Сокол&#8221; (Карельский перешеек, 100 км. от СПб, 20 км. от Лейпясуо, на берегу оз. Глубокое).



Стоимость участия для игроков команд-участниц St.Petersburg Open - 2006/07 составляет 1000 руб. Для всех остальных - 1250 руб.



Всего в лагере 120 мест.



Полный текст объявления: http://www.aerobrain.info/lager/info.html



Нереализованными остались чуть более 30 путевок. Напоминаю, что заявки на участие в лагере принимаются до 25 июня. Просьба поторопиться.



Если до 25 июня список из 120 человек не будет заполнен, начнем принимать заявки только на выходные 14-15 июля.