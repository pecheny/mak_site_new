+++
title = "Синхронный рейтинговый чемпионат &#171;18+&#187;: завершение регистрации"
date = 2013-04-26T11:19:14
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21589"
permalink = "/post_21589/"
categories = [ "Анонс мероприятий",]
slug = "sinkhronnyi-reitingovyi-chempionat-18-zavershenie-registratsii"
aliases = [ "/post_21589",]
type = "post"
tags = []
+++

Уважаемые представители площадок!



У вас осталось времени до полуночи, чтобы <a href="http://ratingnew.chgk.info/synch.php?requests&idtournament=2325" target="_blank">зарегистрировать</a> свой город для отыгрыша <a href="http://chgk.livejournal.com/2417007.html" target="_blank">рейтингового синхрона &#8220;18+&#8221;</a>.



<img src="http://static.ngs.ru/news/preview/a81786c57efc902b4067b6d97ea17206081562fe_395.jpg" alt="" border="0" />



Обращаю внимание на то, что в связи с турнирной перегруженностью уик-энда, в зачет пойдут и результаты площадок, на которых чемпионат будет проведен в понедельник.



* * *



Проведем синхрон везде - 18 нам уже!