+++
title = "&#171;Шулер&#187; вне конкуренции"
date = 2013-06-19T09:48:36
author = "DimoN"
guid = "http://mak-chgk.ru/?p=21610"
permalink = "/post_21610/"
categories = [ "Прочее",]
slug = "shuler-vne-konkurentsii"
aliases = [ "/post_21610",]
type = "post"
tags = []
+++

<p style="color: #000000;font-family: Verdana;font-size: 13px;line-height: normal;background-color: #ffffff">

  <span style="font-family: georgia, serif"><span style="font-size: 10pt">Уважаемые знатоки!</span></span>

</p>



<p style="color: #000000;font-family: Verdana;font-size: 13px;line-height: normal;background-color: #ffffff">

  <span style="font-family: georgia, serif"><span style="font-size: 10pt">Подведены <a href="http://ratingnew.chgk.info/tournaments.php?displaytournament=2326" rel="nofollow" target="_blank">итоги</a></span></span> <span style="font-family: georgia, serif"><span style="font-size: 10pt">международного рейтингового синхронного чемпионата по игре “Что? Где? Когда?” “Мемориал Диара Туганбаева”, в котором приняли участие 73 команды из 14-ти городов мира.</span></span>

</p>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">В этот раз на верхней ступени пьедестале почёта оказался могилевский квартет &#8220;Шулер&#8221; в составе:<span></span> </span></span><span><span style="font-size: 10pt">Д<span style="font-family: georgia, serif"><em>митрий Корнеев, Максим Бучнев, Андрей Клекель, Борис Мишкин. </em></span></span></span>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">&#8220;Полный боекомплект&#8221; команды выглядит так:</span></span>



<img src="http://ic.pics.livejournal.com/dimonn/8105409/3732/3732_600.jpg" alt="042" width="600" height="338" />



<span style="font-family: georgia, serif"><span style="font-size: 10pt">Серебряным и бронзовым призерами чемпионата стали москвичи - команды &#8220;Гросс-бух&#8221; и &#8220;Gambler&#8221;, соответственно.</span></span>



Благодарю: <span style="font-size: 10pt;font-family: georgia, serif">&#8212;  всех участников синхрона за честную игру и всего 3 апелляции; </span><span style="font-family: georgia, serif"><span style="font-size: 10pt">&#8212; Максима Евланова (Харьков), Леонида Климовича (Гомель), Александра Шапиро (Тель-Авив), Михаила Локшина (Санкт-Петербург), Ивана Топчия (Минск) </span></span><span style="font-family: georgia, serif"><span style="font-size: 10pt"> за работу в АЖ; </span></span><span style="font-family: georgia, serif"><span style="font-size: 10pt">&#8212; хранителей рейтинга МАК за оперативное техническое сопровождение турнира.</span></span>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">До встречи в октябре на &#8220;VI Кубке невМоГоТУ &#8220;.</span></span>



<span style="font-family: georgia, serif"><span style="font-size: 10pt">От имени Оргкомитета “Мемориала Диара Туганбаева” Дмитрий Смирнов.</span></span>