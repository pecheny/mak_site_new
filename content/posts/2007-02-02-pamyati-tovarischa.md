+++
title = "Памяти товарища"
date = 2007-02-02T14:19:59
author = "Константин Кноп"
guid = "http://mak-chgk.ru/2007/02/02/pamyati-tovarischa/"
permalink = "/post_550/"
categories = [ "Прочее",]
slug = "pamiati-tovarishcha"
aliases = [ "/post_550", "/2007/02/02/pamyati-tovarischa",]
type = "post"
tags = []
+++

День сегодня недобрый, ох, недобрый.



Вчера вечером умер Дима Коноваленко. Сердце.



Невероятно тяжело поверить в случившееся. Всего 36 лет. Дима очень много успел как игрок ЧГК - выиграл чемпионат России, завоевал &#8220;Хрустальную сову&#8221;, стал чемпионом мира. Сколького он не успел, мы уже никогда не узнаем.



Искренние соболезнования семье и близким (ох&#8230; рука дрожит, не могу печатать. Четверо детей, старший еще школьник, младшая дочка вообще кроха.). Мы тебя помним.