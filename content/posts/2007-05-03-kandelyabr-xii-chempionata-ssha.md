+++
title = "Канделябр XII чемпионата США"
date = 2007-05-03T03:02:27
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/03/kandelyabr-xii-chempionata-ssha/"
permalink = "/post_590/"
Source = [ "http://lists.chgk.info/z-info/200705/msg00004.html",]
categories = [ "Турниры",]
slug = "kandeliabr-xii-chempionata-ssha"
aliases = [ "/post_590", "/2007/05/03/kandelyabr-xii-chempionata-ssha",]
type = "post"
tags = []
+++

Вопросы XII Чемпионата США можно оценить в Канделябре: <!--more-->



  * [Тур 1](http://internet.chgk.info/cgi-bin/kand.cgi?comp=us12&vote=yes&tur=1)</p> 



    <li><a href="http://internet.chgk.info/cgi-bin/kand.cgi?comp=us12&vote=yes&tur=2">Тур 2</a></li>

    

    <li><a href="http://internet.chgk.info/cgi-bin/kand.cgi?comp=us12&vote=yes&tur=3">Тур 3</a></li>

    

    <li><a href="http://internet.chgk.info/cgi-bin/kand.cgi?comp=us12&vote=yes&tur=4">Тур 4</a></li>

    <li><a href="http://internet.chgk.info/cgi-bin/kand.cgi?comp=us12&vote=yes&tur=5">Перестрелка</a></li>

    



</ul>



Обратите внимание, что три тура из четырех (туры 1, 3 и 4) пакета ЧСША составляли также пакет 1-го Международного Кубка Америки.



_Оргкомитет XII Чемпионата США_