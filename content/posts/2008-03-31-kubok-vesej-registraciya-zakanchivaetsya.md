+++
title = "&#171;Кубок весей&#187; &#8212; регистрация заканчивается"
date = 2008-03-31T19:54:19
author = "DimoN"
guid = "http://mak-chgk.ru/kubok-vesej-registraciya-zakanchivaetsya/"
permalink = "/post_3381/"
categories = [ "Турниры",]
slug = "kubok-vesei-registratsiia-zakanchivaetsia"
aliases = [ "/post_3381", "/kubok-vesej-registraciya-zakanchivaetsya",]
type = "post"
tags = []
+++

Уважаемые знатоки!



Спешу напомнить, что регистрация на синхронный чемпионат &#8220;Кубок весей&#8221; близится к завершению. На данный момент заявки на участие подали представители 25 городов Армении, Азербайджана, Латвии, России, США, Узбекистана и Украины (подробнее см. [<font color="#3f5f9e">http://hoster.bmstu.ru/~brain/BMSTU-Kub<wbr></wbr>Ves-registr.htm<img src="http://i.ixnp.com/images/v3.23/t.gif" style="padding-right: 0px; background-position: -944px 0px; display: inline; padding-left: 0px; font-weight: normal; min-height: 0px; left: auto; float: none; background-image: url('http://i.ixnp.com/images/v3.23/theme/silver/palette.gif'); visibility: visible; padding-bottom: 0px; margin: 0px; vertical-align: top; width: 14px; line-height: normal; padding-top: 1px; background-repeat: no-repeat; font-style: normal; font-family: 'trebuchet ms', arial, helvetica, sans-serif; position: static; top: auto; height: 12px; background-color: transparent; text-decoration: none; cssfloat: none; maxheight: 2000px; maxwidth: 2000px; minwidth: 0px; border-width: 0px" id="snap_com_shot_link_icon" class="snap_preview_icon" /></font>](http://hoster.bmstu.ru/~brain/BMSTU-KubVes-registr.htm){.snap_shots}). Все подробности о чемпионате можно прочитать здесь [<font color="#3f5f9e">http://hoster.bmstu.ru/~brain/BMSTU-Kub<wbr></wbr>Ves.htm<img src="http://i.ixnp.com/images/v3.23/t.gif" style="padding-right: 0px; background-position: -944px 0px; display: inline; padding-left: 0px; font-weight: normal; min-height: 0px; left: auto; float: none; background-image: url('http://i.ixnp.com/images/v3.23/theme/silver/palette.gif'); visibility: visible; padding-bottom: 0px; margin: 0px; vertical-align: top; width: 14px; line-height: normal; padding-top: 1px; background-repeat: no-repeat; font-style: normal; font-family: 'trebuchet ms', arial, helvetica, sans-serif; position: static; top: auto; height: 12px; background-color: transparent; text-decoration: none; cssfloat: none; maxheight: 2000px; maxwidth: 2000px; minwidth: 0px; border-width: 0px" id="snap_com_shot_link_icon" class="snap_preview_icon" /></font>](http://hoster.bmstu.ru/~brain/BMSTU-KubVes.htm){.snap_shots}



К сожалению, НаСоС отказался брать проведение данного чемпионата под свою эгиду, поэтому мы вынуждены немного изменить порядок его проведения. В частности, для ряда российских и украинских площадок мы беремся организовать &#8220;эмиссарский синхрон&#8221;, итоги которого будут направлены для учета в рейтинге МАК (ранее чемпионат был лицензирован МАК именно как синхрон). Заявки о готовности принять эмиссара направляйте в адрес Оргкомитета до среды включительно! Остальным городам мы предлагаем отыграть наш чемпионат в режиме асинхрона с 6-го по 13-е апреля включительно. Итоги отыгрыша всех площадок будут сведены в неофициальный &#8220;асинхронный зачет&#8221;, который, правда, в рейтинге МАК не будет учтен.