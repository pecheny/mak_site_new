+++
title = "Отзывы о ЧР &#8212; 2009"
date = 2009-03-24T22:31:32
author = "Константин Кноп"
guid = "http://mak-chgk.ru/post_7236/"
permalink = "/post_7236/"
categories = [ "Прочее",]
slug = "otzyvy-o-chr-2009"
aliases = [ "/post_7236", "/post_7236",]
type = "post"
tags = []
+++

Вот список частных отзывов о ЧР-2009. Сообщения типа &#8220;был, видел, устал&#8221; не учитывались. Если кто не нашел себя в списке и хочет поделиться своим мнением с массами, пишите на 4lia.mak на gmail.



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://andrey-kouzma.livejournal.com/116183.html"><font face="Calibri">http://andrey-kouzma.livejournal.com/116183.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://andrey-kouzma.livejournal.com/116541.html"><font face="Calibri">http://andrey-kouzma.livejournal.com/116541.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://andrey-kouzma.livejournal.com/116915.html"><font face="Calibri">http://andrey-kouzma.livejournal.com/116915.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://andrey-kouzma.livejournal.com/117118.html"><font face="Calibri">http<span lang="RU">://</span>andrey<span lang="RU">-</span>kouzma<span lang="RU">.</span>livejournal<span lang="RU">.</span>com<span lang="RU">/117118.</span>html</font></a></span>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"></span>

</p>





  





<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://e-rubik.livejournal.com/734971.html"><font face="Calibri"><span lang="EN-US">http</span>://<span lang="EN-US">e</span>-<span lang="EN-US">rubik</span>.<span lang="EN-US">livejournal</span>.<span lang="EN-US">com</span>/734971.<span lang="EN-US">html</span></font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://e-rubik.livejournal.com/735415.html"><font face="Calibri">http://e-rubik.livejournal.com/735415.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://e-rubik.livejournal.com/736278.html"><font face="Calibri">http<span lang="RU">://</span>e<span lang="RU">-</span>rubik<span lang="RU">.</span>livejournal<span lang="RU">.</span>com<span lang="RU">/736278.</span>html</font></a></span><font face="Calibri"><span lang="EN-US"> </span>видео</font>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  &nbsp;

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://knop.livejournal.com/114208.html"><font face="Calibri">http://knop.livejournal.com/114208.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://e-ponikarov.livejournal.com/119469.html"><font face="Calibri"><span lang="EN-US">http</span>://<span lang="EN-US">e</span>-<span lang="EN-US">ponikarov</span>.<span lang="EN-US">livejournal</span>.<span lang="EN-US">com</span>/119469.<span lang="EN-US">html</span></font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://ze-oboltus.livejournal.com/250561.html"><font face="Calibri">http://ze-oboltus.livejournal.com/250561.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://andrey-lensky.livejournal.com/158840.html"><font face="Calibri">http://andrey-lensky.livejournal.com/158840.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://blackbak.livejournal.com/43036.html"><font face="Calibri">http://blackbak.livejournal.com/43036.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://tea-cutter.livejournal.com/177880.html"><font face="Calibri">http://tea-cutter.livejournal.com/177880.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://septembreange.livejournal.com/175535.html"><font face="Calibri">http://septembreange.livejournal.com/175535.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://smartnik.livejournal.com/190791.html"><font face="Calibri">http://smartnik.livejournal.com/190791.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://mitya-kapl.livejournal.com/77725.html"><font face="Calibri">http://mitya-kapl.livejournal.com/77725.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://sllonja.livejournal.com/98627.html"><font face="Calibri">http://sllonja.livejournal.com/98627.html</font></a>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://torrio.livejournal.com/283225.html"><font face="Calibri">http<span lang="RU">://</span>torrio<span lang="RU">.</span>livejournal<span lang="RU">.</span>com<span lang="RU">/283225.</span>html</font></a></span><span lang="EN-US"> </span>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://meihoa.livejournal.com/137243.html"><font face="Calibri">http://meihoa.livejournal.com/137243.html</font></a></span>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://re-gata.livejournal.com/415195.html"><font face="Calibri">http<span lang="RU">://</span>re<span lang="RU">-</span>gata<span lang="RU">.</span>livejournal<span lang="RU">.</span>com<span lang="RU">/415195.</span>html</font></a></span><font face="Calibri"><span lang="EN-US"> </span>фото</font>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <span lang="EN-US"><a href="http://urival.livejournal.com/69814.html"><font face="Calibri">http://urival.livejournal.com/69814.html</font></a></span><br /> <span lang="EN-US"><a href="http://interpretator.livejournal.com/412094.html"><font face="Calibri">http://interpretator.livejournal.com/412094.html</font></a></span><br /> <span lang="EN-US"><a href="http://axinija.livejournal.com/234417.html"><font face="Calibri">http://axinija.livejournal.com/234417.html</font></a></span>

</p>



<p style="margin: 0cm 5.65pt 0pt 0cm" class="MsoNormal">

  <a href="http://kotomkina.livejournal.com/4485.html"><font face="Calibri">http://kotomkina.livejournal.com/4485.html</font></a><br /> <span style="color: black"><a href="http://andy-racing.livejournal.com/61845.html"><font face="Calibri">http://andy-racing.livejournal.com/61845.html</font></a></span><br /> <span style="color: black"><a href="http://solmyr.livejournal.com/159952.html"><font face="Calibri">http://solmyr.livejournal.com/159952.html</font></a></span><br /> <a href="http://pohanius.livejournal.com/18805.html"><font face="Calibri">http://pohanius.livejournal.com/18805.html</font></a><font face="Calibri"> фотографии: личные наблюдения при прогулке по городу, несколько фотографий команды «Мираж», всего одна фотография, где видно в зале команд 10-15</font>

</p>