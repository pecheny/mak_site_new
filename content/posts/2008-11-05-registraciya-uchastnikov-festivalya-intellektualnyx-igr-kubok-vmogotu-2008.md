+++
title = "Регистрация участников фестиваля интеллектуальных игр &#171;Кубок вМоГоТУ-2008&#187;"
date = 2008-11-05T12:41:26
author = "DimoN"
guid = "http://mak-chgk.ru/registraciya-uchastnikov-festivalya-intellektualnyx-igr-kubok-vmogotu-2008/"
permalink = "/post_5086/"
categories = [ "Прочее",]
slug = "registratsiia-uchastnikov-festivalia-intellektualnykh-igr-kubok-vmogotu-2008"
aliases = [ "/post_5086", "/registraciya-uchastnikov-festivalya-intellektualnyx-igr-kubok-vmogotu-2008",]
type = "post"
tags = []
+++

Уважаемые знатоки!    



    



Началась регистрация участников &#8220;Кубка вМоГоТУ-2008&#8221;, который пройдет в Москве 22-го ноября 2008 года.  



Предварительная информация здесь 



<http://www.bmstu.ru/~brain/BMSTU-vMoGoTU-08.htm> 



<span>  </span>Чуть позже там будут заполнены все необходимые разделы.   



Заявки на участие, оформленные строго по форме



<http://www.bmstu.ru/~brain/BMSTU-vMoGoTU-zayavka-08.htm>  ,



следует присылать уже сейчас в формате Word на специальный адрес Оргкомитета фестиваля



<span lang="EN-US">orgchamp_2008 AT diar TOCHKA ru </span>



<span lang="EN-US"> </span>до 16-го ноября включительно. 



   



<p style="margin: 0cm 0cm 0pt" class="MsoNormal">

  Будем рады старым и новым участникам!

</p>