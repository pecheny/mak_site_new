+++
title = "Срочно нужны вопросы для телеигры &#171;Ум за разум&#187;"
date = 2007-05-04T09:10:56
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/04/srochno-nuzhnyi-voprosyi-dlya-teleigryi-um-za-razum/"
permalink = "/post_612/"
Source = [ "http://lists.chgk.info/z-info/200705/msg00007.html",]
categories = [ "Прочее",]
slug = "srochno-nuzhny-voprosy-dlia-teleigry-um-za-razum"
aliases = [ "/post_612", "/2007/05/04/srochno-nuzhnyi-voprosyi-dlya-teleigryi-um-za-razum",]
type = "post"
tags = []
+++

Для возобновляющейся телеигры &#8220;Ум за разум&#8221; СРОЧНО необходимы вопросы. Поскольку программа цикловая вопросов требуется много.



<!--more-->



Вопросы должны быть средней степени сложности, тематика не ограничена, наличие источника информации обязательно.



Вопросы должны быть &#8220;несвеченными&#8221; /свеченными в регионе, но отсутствующими в базе данных.



Оплата - 100 руб. за отобранный вопрос.



Желающих прошу откликнуться по адресу: adrouz@mail.ru



_Александр Друзь_