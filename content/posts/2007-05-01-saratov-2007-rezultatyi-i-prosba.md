+++
title = "&#171;Саратов-2007&#187; &#8212; результаты и просьба"
date = 2007-05-01T22:40:04
author = "roma7"
guid = "http://mak-chgk.ru/2007/05/01/saratov-2007-rezultatyi-i-prosba/"
permalink = "/post_578/"
Source = [ "http://lists.chgk.info/z-info/200705/msg00001.html",]
categories = [ "Турниры",]
slug = "saratov-2007-rezultaty-i-prosba"
aliases = [ "/post_578", "/2007/05/01/saratov-2007-rezultatyi-i-prosba",]
type = "post"
tags = []
+++

Результаты фестиваля &#8220;Саратов-2007&#8221; можно увидеть на сайте [клуба &#8220;Знак ответа&#8221;](http://saratov.chgk.info).



Просьба к игравшим на нашем фестивале командам - не обсуждать и не светить вопросы ЧГК и брейн-пакетов до 13 мая. Спасибо.



_Борис Гуревич._