+++
title = "ВДИ &#171;Весна-2007&#187; &#8212; приём вопросов"
date = 2007-02-21T22:37:52
author = "roma7"
guid = "http://mak-chgk.ru/2007/02/21/vdi-vesna-2007-priyom-voprosov/"
permalink = "/post_577/"
Source = [ "http://lists.chgk.info/z-info/200702/msg00027.html",]
categories = [ "Турниры",]
slug = "vdi-vesna-2007-priiom-voprosov"
aliases = [ "/post_577", "/2007/02/21/vdi-vesna-2007-priyom-voprosov",]
type = "post"
tags = []
+++

Открыт прием вопросов на VIII турнир ВДИ сезона 2006/07 гг. - &#8220;Весна-2007&#8221;. Турнир состоится 25 марта 2007 г.



Редакторы - Денис Гончар (Одесса), Дмитрий Борок (Самара) - будут рады увидеть ваши вопросы.



<!--more-->



Вопросы принимаются по адресам: dg@list.ru, dborok@mail.ru (пожалуйста, шлите вопросы на оба адреса и включайте в тему письма строчку VDI) до 12 марта 2007 г.



Принятый в пакет вопрос оплачивается из расчета $2.



Поскольку турниры ВДИ ориентированы не только на регулярно играющих знатоков, но и на новичков-любителей, старайтесь, пожалуйста, избегать внутризнатоковских &#8220;цеховых&#8221; реалий и подходов, а также &#8220;скользких&#8221; тем. Нежелательны очень сложные вопросы, длинноты, опора на глубокую эрудицию.



Если вы высылаете вопросы, которые уже предлагали редакторам какого-либо турнира, ОБЯЗАТЕЛЬНО указывайте, кому именно вы уже предлагали данный вопрос.



Просим переслать это сообщение на региональные и клубные листы.



_С уважением,

  

Денис Гончар,

  

Дмитрий Борок._