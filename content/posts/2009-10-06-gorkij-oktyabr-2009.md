+++
title = "Горький Октябрь &#8212; 2009"
date = 2009-10-06T11:23:36
author = "Константин Кноп"
guid = "http://mak-chgk.ru/post_9491/"
permalink = "/post_9491/"
categories = [ "Календарь",]
slug = "gorkii-oktiabr-2009"
aliases = [ "/post_9491", "/post_9491",]
type = "post"
tags = []
+++

**24-25 октября 2009 года** в Нижнем Новогороде пройдет фестиваль “Горький Октябрь - 2009”. Расписание и вся информация о фестивале находится на [<font color="#34425b">сайте турнира</font>](http://nnov.chgk.info/).