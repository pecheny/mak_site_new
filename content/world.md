+++
title = "Чемпионат мира"
date = 2012-02-09T03:23:04
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/"
slug = "world"
aliases = [ "/post_20375", "/world",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

Проводится с 2002 года.



**Действующие правила**



[Положение (Постоянная часть)](http://mak-chgk.ru/world/2013/regulations13)

  

[Регламент (Постоянная часть)](http://mak-chgk.ru/world/2013/reglament13)

  

[Шаблон для переменной части регламента](http://mak-chgk.ru/world/2013/templates) 



&nbsp;



**История**



[Победители и призёры](http://mak-chgk.ru/world/winners)

  

[Статистика, команды](http://mak-chgk.ru/world/teams)

  

[Статистика, игроки](http://mak-chgk.ru/world/players)

  

[Чемпионат мира 2002 года](http://mak-chgk.ru/world/2002)

  

[Чемпионат мира 2003 года](http://mak-chgk.ru/world/2003)

  

[Чемпионат мира 2004 года](http://mak-chgk.ru/world/2004)

  

[Чемпионат мира 2005 года](http://mak-chgk.ru/world/2005)

  

[Чемпионат мира 2006 года](http://mak-chgk.ru/world/2006)

  

[Чемпионат мира 2007 года](http://mak-chgk.ru/world/2007)

  

[Чемпионат мира 2008 года](http://mak-chgk.ru/world/2008)

  

[Чемпионат мира 2010 года](http://mak-chgk.ru/world/2010)

  

[Чемпионат мира 2011 года](http://mak-chgk.ru/world/2011)

  

[Чемпионат мира 2012 года](http://mak-chgk.ru/world/2012)

  

[Чемпионат мира 2013 года](http://mak-chgk.ru/world/2013)