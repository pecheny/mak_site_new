+++
title = "Победители"
date = 2010-10-13T01:26:46
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2010/"
slug = "winners"
aliases = [ "/post_13940", "/mak-cup/2010",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Команды**



2002/03 - Genius (Москва, Россия)



2003/04 - Троярд (Санкт-Петербург, Россия)



2004/05 - Ксеп (Москва, Россия)



2005/06 - Социал-демократы (Москва, Россия)



2006/07 - Команда Губанова (Петродворец, Россия)



2007/08 - Катус (Санкт-Петербург, Россия)



2008/09 - победитель не был определён



2009/10 - ЛКИ (Москва, Россия)



2010/11 - Команда Губанова (Петродворец, Россия), ЛКИ (Москва, Россия)



&nbsp;



**Игроки**



2002/03



Genius (Москва, Россия)



  * Елена Александрова

  * Вадим Карлинский

  * Дмитрий Коноваленко

  * Максим Поташев (к)

  * Владимир Степанов

  * Антон Чернин

  * Юрий Черушев



&nbsp;



2003/04



Троярд (Санкт-Петербург, Россия)



  * Михаил Басс

  * Владимир Белкин

  * Алексей Вавилов

  * Фёдор Двинятин

  * Александр Друзь (к)

  * Инна Друзь

  * Марина Друзь

  * Михаил Дюба

  * Александра Киланова



&nbsp;



2004/05



Ксеп (Москва, Россия)



  * Игорь Бахарев

  * Сергей Вакуленко

  * Станислав Мереминский

  * Константин Мильчин

  * Роман Немучинский

  * Илья Новиков

  * Владимир Севриновский (к)

  * Антон Снятковский



&nbsp;



2005/06



Социал-демократы (Москва, Россия)



  * Виктор Аролович

  * Мария Баранчикова

  * Дмитрий Иванов

  * Илья Крамник

  * Павел Малышев (к)

  * Антон Попов

  * Юрий Попов

  * Денис Сомов

  * Олег Христенко



&nbsp;



2006/07



Команда Губанова (Петродворец, Россия)



  * Ольга Берёзкина

  * Юрий Выменец

  * Антон Губанов (к)

  * Михаил Матвеев

  * Борис Моносов

  * Александр Скородумов



&nbsp;



2007/08



Катус (Санкт-Петербург, Россия)



  * Юрий Выменец

  * Дмитрий Герасимов

  * Константин Кноп

  * Мария Наумова

  * Александр Огнев

  * Борис Паленовский

  * Ирина Прокофьева

  * Александр Рыжанов

  * Роман Семизаров (к)

  * Василий Хомутов



&nbsp;



2008/09



Победитель не был определён



&nbsp;



2009/10



ЛКИ (Москва, Россия)



  * Дмитрий Белявский

  * Александра Брутер

  * Андрей Ленский (к)

  * Роман Немучинский

  * Вероника Ромашова

  * Максим Руссо

  * Иван Семушин (к)

  * Ольга Хворова



&nbsp;



2010/11



Команда Губанова (Петродворец, Россия)



  * Ольга Берёзкина

  * Юрий Выменец

  * Антон Губанов (к)

  * Алексей Гилёв

  * Александра Киланова

  * Михаил Матвеев

  * Евгений Миротин

  * Александр Скородумов



ЛКИ (Москва, Россия)



  * Дмитрий Белявский

  * Александра Брутер

  * Илья Иткин

  * Вероника Ромашова

  * Максим Руссо

  * Иван Семушин (к)