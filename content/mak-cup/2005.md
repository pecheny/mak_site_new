+++
title = "Кубок мира сезона 2005/06"
date = 2010-11-05T22:47:18
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2005/"
slug = "kubok-mira-sezona-2005-06"
aliases = [ "/post_14331", "/mak-cup/2005",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Правила**



[Регламент](http://mak-chgk.ru/mak-cup/2005/reglament)



[Условия предоставления статуса ЭКМ](http://mak-chgk.ru/mak-cup/2005/conditions)



&nbsp;



**Результаты**



[МГТУ, 29-30.10.2005](http://mak-chgk.ru/mak-cup/2005/stage1/)



[“СамариУМ”, 04-06.11.2005](http://mak-chgk.ru/mak-cup/2005/stage2/)



[“Белые ночи”, 01-02.07.2006](http://mak-chgk.ru/mak-cup/2005/stage3)



[Общий зачёт](http://mak-chgk.ru/mak-cup/2005/overall)