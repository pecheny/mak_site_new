+++
title = "Этап КМ, “Белые ночи”"
date = 2011-10-10T00:43:17
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2006/stage4/"
slug = "stage4"
aliases = [ "/post_18814", "/mak-cup/2006/stage4",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Международный фестиваль &#8220;Белые ночи - Энергокапитал&#8221;,** **этап Кубка мира сезона 2006/07**



**Санкт-Петербург, 7-8 июля 2007 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      С

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      62

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      60

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ЮМА - Энергокапитал

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Неспроста - WestCall

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      40,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Eclipse

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      51,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Бандерлоги

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      57,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Особое мнение

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Росто-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Кулверстукас

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нижний Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      69

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Ресурс

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      69,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Крамбамбуля

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      90

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ла Скала

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      78

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Динамит

    </td>

    

    <td>

    </td>

    

    <td>

      Казань

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      86

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      86

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ноев ковчег

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      92

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      93

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      94,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td>

      Неглинка

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      103,5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Уездный доктор

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      103,5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Борьба с умом

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      108

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

  </tr>

  

  <tr>

    <td>

      Salvatore

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      110

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Опухлики

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      111

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td>

      Маргиналы

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      115

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      119

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Зубий дряполап

    </td>

    

    <td>

    </td>

    

    <td>

      Зеленоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      116,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Розовый слон

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нижний Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      117

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Приматы-Шейк

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      121,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

  </tr>

  

  <tr>

    <td>

      Архимед

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      125,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28

    </td>

  </tr>

  

  <tr>

    <td>

      МаФи

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      124

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

  </tr>

  

  <tr>

    <td>

      Катус

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      128

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td>

      Форум

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      139

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td>

      Дао

    </td>

    

    <td>

    </td>

    

    <td>

      Тула

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      144

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      145

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

  </tr>

  

  <tr>

    <td>

      Синоп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      153

    </td>

    

    <td>

    </td>

    

    <td align="center">

      34

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td>

      Эспоо

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      164,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35

    </td>

  </tr>

  

  <tr>

    <td>

      ИНЖЭКОН

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      167,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Старики-разбойники

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт=-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      164,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37

    </td>

  </tr>

  

  <tr>

    <td>

      Signum

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      172,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      38

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Коваленко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великий Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      180,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      39

    </td>

  </tr>

  

  <tr>

    <td>

      Мы-6

    </td>

    

    <td>

    </td>

    

    <td>

      Хельсинки

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      184

    </td>

    

    <td>

    </td>

    

    <td align="center">

      40

    </td>

  </tr>

  

  <tr>

    <td>

      В-Ч-Ж

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      191

    </td>

    

    <td>

    </td>

    

    <td align="center">

      41

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Чёрный квадрат

    </td>

    

    <td>

    </td>

    

    <td>

      Северодвинск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      183

    </td>

    

    <td>

    </td>

    

    <td align="center">

      42

    </td>

  </tr>

  

  <tr>

    <td>

      Гладиолус

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      199

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43

    </td>

  </tr>

  

  <tr>

    <td>

      Ч-2

    </td>

    

    <td>

    </td>

    

    <td>

      Калининград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      212,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      44

    </td>

  </tr>

  

  <tr>

    <td>

      RTFM

    </td>

    

    <td>

    </td>

    

    <td>

      Череповец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      213,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      45

    </td>

  </tr>

  

  <tr>

    <td>

      Ледокол

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великие Луки

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      218

    </td>

    

    <td>

    </td>

    

    <td align="center">

      46

    </td>

  </tr>

  

  <tr>

    <td>

      Чердак

    </td>

    

    <td>

    </td>

    

    <td>

      Ярославль

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      222

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47

    </td>

  </tr>

  

  <tr>

    <td>

      ИНЖЭКОН-2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      239,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      48

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 01.07.2007

  

**В** - количество взятых вопросов

  

**С** - сумма мест

  

**М** - место в зачёте этапа 



[Сайт турнира](http://belyenochi.chgk.info/2007/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200707SPb.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=269)