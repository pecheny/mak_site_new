+++
title = "Общий зачёт"
date = 2011-10-11T01:23:17
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2006/overall/"
slug = "overall"
aliases = [ "/post_18832", "/mak-cup/2006/overall",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Общий зачёт Кубка мира сезона 2006/07**



**Чемпионат мира, Светлогорск, 23-26 августа 2007 года. Финал**

  

(результаты команд - победительниц этапов Кубка мира)



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Ч

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td>

      Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**Ч** - место в зачёте чемпионата

  

**М** - место в общем зачёте 



[Сайт чемпионата мира](http://www.worldchamp6.chgk.info/)



&nbsp;



**Команда Губанова (Петродворец, Россия)**



  * Ольга Берёзкина

  * Юрий Выменец

  * Антон Губанов (к)

  * Михаил Матвеев

  * Борис Моносов

  * Александр Скородумов