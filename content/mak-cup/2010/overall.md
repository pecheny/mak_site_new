+++
title = "Общий зачёт"
date = 2011-08-13T01:46:57
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2010/stage2/"
slug = "overall"
aliases = [ "/post_18123", "/mak-cup/2010/stage2",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Общий зачёт Кубка МАК сезона 2010/11**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      1-2

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td>

      Семушин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      1-2

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Ла Скала

    </td>

    

    <td>

    </td>

    

    <td>

      Уточкин

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Тайга

    </td>

    

    <td>

    </td>

    

    <td>

      Игнатенков

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6.5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18.5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td>

      Лёгенький

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td>

      Райко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Integro

    </td>

    

    <td>

    </td>

    

    <td>

      Романенко

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      8-9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Одушевлённые аэросани

    </td>

    

    <td>

    </td>

    

    <td>

      Марцинкевич

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      8-9

    </td>

  </tr>

  

  <tr>

    <td>

      Gambler

    </td>

    

    <td>

    </td>

    

    <td>

      Ковальчук

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      10-11

    </td>

  </tr>

  

  <tr>

    <td>

      Шулер

    </td>

    

    <td>

    </td>

    

    <td>

      Корнеев

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td nowrap align="center">

      10-11

    </td>

  </tr>

</table>



**1** - очки на 1-м этапе

  

**2** - очки на 2-м этапе

  

**О** - очки в общем зачёте

  

**М** - место в общем зачёте 



&nbsp;



**Команда Губанова (Петродворец, Россия)**



  * Ольга Берёзкина

  * Юрий Выменец

  * Антон Губанов (к)

  * Алексей Гилёв

  * Александра Киланова

  * Михаил Матвеев

  * Евгений Миротин

  * Александр Скородумов



**ЛКИ (Москва, Россия)**



  * Дмитрий Белявский

  * Александра Брутер

  * Илья Иткин

  * Вероника Ромашова

  * Максим Руссо

  * Иван Семушин (к)