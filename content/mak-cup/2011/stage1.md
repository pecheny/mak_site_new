+++
title = "Этап КМ, &#171;Славянка&#187;"
date = 2012-08-09T13:18:58
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=21289"
slug = "stage1"
aliases = [ "/post_21289",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**IV международный турнир по интеллектуальным играм &#8220;Славянка-2012&#8221;**



**Витебск, 4-5 августа 2012 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      51

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Тайга

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Минус один

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      47

    </td>

    

    <td align="center">

      +3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ла Скала

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      47

    </td>

    

    <td align="center">

      +2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Дилемма United

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нижний Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Бандерлоги

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      45

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мания Величия

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      44

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      43

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      South Butovo Park

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      42

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Гросс-бух

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5,5

    </td>

  </tr>

  

  <tr>

    <td>

      Integro

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3,5

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Одушевлённые аэросани

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      36

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Пожарные машинки

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      36

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      X-promt

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Легион

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ультиматум

    </td>

    

    <td>

    </td>

    

    <td>

      Гомель

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      32

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Хронически разумные United

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      32

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ЗАО Дно

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Смерть верхом на лошади

    </td>

    

    <td>

    </td>

    

    <td>

      Волгоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Medium

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Памяти Пауля

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дежавю

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      28

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Non-Sense

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ТПРУНЯ

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Interzanoza

    </td>

    

    <td>

    </td>

    

    <td>

      Даугавпилс

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Команда

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Рабкор

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Белгазпромбанка

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Чудо в перьях

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Зеленоград-43

    </td>

    

    <td>

    </td>

    

    <td>

      Зеленоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      32-33

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Шулер

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      32-33

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Electronic wife

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      34

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ход конём

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 01.06.2012

  

**В** - количество взятых вопросов

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ 



[Сайт турнира](http://slavyanka.us/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/201108Vitebsk.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=1863)