+++
title = "Регламент Кубка МАК (2011)"
date = 2011-11-05T02:46:31
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2011/reglament/"
slug = "reglament"
aliases = [ "/post_19212", "/mak-cup/2011/reglament",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

(_действует с сезона 2011/12_)



**1. ОБЩИЕ ПОЛОЖЕНИЯ**



1.1. Кубок МАК - серия турниров (далее - “этапов”) одного игрового сезона, объединённых общим зачётом.



1.2. Для присвоения соревнованию статуса этапа Кубка МАК необходимо выполнение условий, сформулированных в отдельном документе.



1.3. Регламент каждого этапа разрабатывается Оргкомитетом этого этапа. Регламент этапа не должен противоречить данному документу и Кодексу спортивного “Что? Где? Когда?”.



**2. УЧАСТНИКИ**



2.1. Команды, входящие в число 15 лучших команд рейтинга МАК (на момент присвоения соревнованию статуса этапа Кубка МАК), имеют безусловное право выступить на данном этапе. Организаторы этапа могут пригласить также любые другие команды.



2.2. Перед началом участия в Кубке МАК команда подаёт базовый заявочный список, содержащий от 4 до 8 игроков. Заявочные списки команд принимаются лицом, уполномоченным турнирной комиссией МАК. Если команда не подала базовый заявочный список, то базовым заявочным списком команды считается её состав на первом этапе, в котором она приняла участие.



2.3. На отдельном этапе Кубка МАК в состав участвующей в Кубке МАК команды должно входить не менее 4 игроков из базового заявочного списка.



2.4. На отдельном этапе Кубка МАК в составе участвующей в Кубке МАК команды может находиться не более одного игрока, уже выступавшего в текущем Кубке МАК за любую другую команду.



**3. ПОДВЕДЕНИЕ ИТОГОВ**



3.1. По итогам каждого этапа командам присуждаются призовые очки в общий зачёт Кубка МАК по следующей схеме: 



  * 15 очков - за 1-е место

  * 14 очков - за 2-е место

  * 13 очков - за 3-е место

  * 12 очков - за 4-е место

  * 11 очков - за 5-е место

  * 10 очков - за 6-е место

  * 9 очков - за 7-е место

  * 8 очков - за 8-е место

  * 7 очков - за 9-е место

  * 6 очков - за 10-е место

  * 5 очков - за 11-е место

  * 4 очка - за 12-е место

  * 3 очка - за 13-е место

  * 2 очка - за 14-е место

  * 1 очко - за 15-е место В случае дележа какого-либо из первых 15 мест сумма призовых очков, полагающихся за каждое из поделённых мест, делится поровну между командами, принимающими участие в дележе.



3.2. Команда включается в общий зачёт Кубка МАК, если она принимает участие не менее чем в двух этапах.



3.3. Результатом команды в общем зачёте Кубка МАК является сумма набранных ею призовых очков. В случае если команда набирала очки в трех или более этапах, то в зачёт идут лучшие результаты по следующей схеме: 



  * 2 лучших результата, если Кубок МАК состоит из 3 этапов;

  * 3 лучших результата, если Кубок МАК состоит из 4 или 5 этапов;

  * 4 лучших результата, если Кубок МАК состоит из 6 и более этапов. 3.4. При равной сумме призовых очков более высокое место в общем зачёте занимает команда, принявшая участие в меньшем числе этапов Кубка МАК. При равенстве этого показателя места считаются разделёнными на равных.