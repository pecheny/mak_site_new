+++
title = "Этап КМ, “Весна в ЛЭТИ”"
date = 2011-10-13T01:35:52
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2007/stage1/"
slug = "stage1"
aliases = [ "/post_18865", "/mak-cup/2007/stage1",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**VI международный фестиваль &#8220;Весна в ЛЭТИ&#8221;, этап Кубка мира сезона 2007/08**



**Санкт-Петербург, 15-16 марта 2008 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Р

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Катус

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      60

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1083

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      58

    </td>

    

    <td>

    </td>

    

    <td align="right">

      883

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Ресурс

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="right">

      930

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="right">

      777

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Крамбамбуля

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="right">

      765

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="right">

      854

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Бандерлоги

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="right">

      703

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Янович

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="right">

      673

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="right">

      686

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Припев 2 раза

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="right">

      677

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Зубий дряполап

    </td>

    

    <td>

    </td>

    

    <td>

      Зеленоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="right">

      622

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Бавина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="right">

      550

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Опухлики

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="right">

      527

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Неглинка BP

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="right">

      603

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Зарница

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="right">

      505

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ла Скала

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="right">

      500

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Коваленко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великий Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      432

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ноев ковчег

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      396

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Спонсора.net

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="right">

      506

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Чудо в перьях

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="right">

      491

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

  </tr>

  

  <tr>

    <td>

      Синоп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="right">

      484

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Дилетанты

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="right">

      463

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

  </tr>

  

  <tr>

    <td>

      БН

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      471

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

  </tr>

  

  <tr>

    <td>

      Тайга

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      427

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

  </tr>

  

  <tr>

    <td>

      Signum

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      448

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Борьба с умом

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      464

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

  </tr>

  

  <tr>

    <td>

      ЗППП

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      492

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td>

      Эспоо

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      411

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

  </tr>

  

  <tr>

    <td>

      ГРМ-2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      503

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

  </tr>

  

  <tr>

    <td>

      Архимед

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      460

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      16 тонн

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      380

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

  </tr>

  

  <tr>

    <td>

      Маргиналы

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      355

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

  </tr>

  

  <tr>

    <td>

      ГРМ

    </td>

    

    <td>

    </td>

    

    <td>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      412

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Назарова

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      312

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бензопила &#8220;Дружба&#8221;

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      347

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

  </tr>

  

  <tr>

    <td>

      Химера

    </td>

    

    <td>

    </td>

    

    <td>

      Зеленоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      275

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

  </tr>

  

  <tr>

    <td>

      Буравчик

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="right">

      374

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

  </tr>

  

  <tr>

    <td>

      НВ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      369

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Геном Ньютона

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      282

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мы-6

    </td>

    

    <td>

    </td>

    

    <td>

      Хельсинки

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      284

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Друзья К.Р.

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      176

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Vizit к доктору

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      278

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

  </tr>

  

  <tr>

    <td>

      RTFM

    </td>

    

    <td>

    </td>

    

    <td>

      Череповец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      145

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

  </tr>

  

  <tr>

    <td>

      КИО

    </td>

    

    <td>

    </td>

    

    <td>

      Тольятти

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      99

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Третий Рим

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      102

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Толстый и тонкий

    </td>

    

    <td>

    </td>

    

    <td>

      Тольятти

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

    </td>

    

    <td>

    </td>

    

    <td align="right">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 01.03.2008

  

**В** - количество взятых вопросов

  

**Р** - рейтинг

  

**М** - место в зачёте этапа 



[Сайт турнира](http://vvl.chgk.info/?mdlname=text-page&pageNamePart=y2008)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200803SPb.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=318)