+++
title = "Этап КМ, “Одессея”"
date = 2010-11-07T15:38:13
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2002/stage3/"
slug = "stage3"
aliases = [ "/post_14359", "/mak-cup/2002/stage3",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**&#8220;Одессея-2003&#8221;, этап Кубка мира сезона 2002/03**



**Одесса, 10-11 мая 2003 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td>

      Genius

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      63

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Мороховского

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      61

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      61

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6,5

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      61

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6,5

    </td>

  </tr>

  

  <tr>

    <td>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      60

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      59

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2,5

    </td>

  </tr>

  

  <tr>

    <td>

      Шалтай-Болтай

    </td>

    

    <td>

    </td>

    

    <td>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2,5

    </td>

  </tr>

  

  <tr>

    <td>

      СПС

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      0,33

    </td>

  </tr>

  

  <tr>

    <td>

      Тачанка

    </td>

    

    <td>

    </td>

    

    <td>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      0,33

    </td>

  </tr>

  

  <tr>

    <td>

      Фарлеп-Гамбринус

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      0,33

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13-14

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Минус один

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13-14

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Москито

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16-17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Приматы

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16-17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Стирол

    </td>

    

    <td>

    </td>

    

    <td>

      Горловка

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18-19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Элефант

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18-19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дуплет

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20-21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Золотая амфора

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20-21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МПС

    </td>

    

    <td>

    </td>

    

    <td>

      Бердянск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22-25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Новая реальность

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22-25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22-25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Триол-ХИИТ-2002

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22-25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Кредо

    </td>

    

    <td>

    </td>

    

    <td>

      Симферополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26-27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Торпедный отсек

    </td>

    

    <td>

    </td>

    

    <td>

      Донецкая область

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26-27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Rara Avis

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Автоленд

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ЗНТУ

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Форс-мажор

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Зловещие сухари

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33-35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Донбасс

    </td>

    

    <td>

    </td>

    

    <td>

      Донецкая область

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33-35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Зелёный трамвай

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33-35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Мания величия

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Катти-Сарк - УМП

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37-40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Королевское мороженое

    </td>

    

    <td>

    </td>

    

    <td>

      Луганск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37-40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Сателiт

    </td>

    

    <td>

    </td>

    

    <td>

      Тернополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37-40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Чёрная кошка

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37-40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Обоз

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      41

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ЮСМ

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      42

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Аборигены

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Вырезано цензурой

    </td>

    

    <td>

    </td>

    

    <td>

      Херсон

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      От винта - ЭПТ

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Потанинцы - ЦИТ Rus

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Асгард - ЮСИ

    </td>

    

    <td>

    </td>

    

    <td>

      Николаев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Пилигрим

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Проект-Геркулес

    </td>

    

    <td>

    </td>

    

    <td>

      Новоукраинка

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Славяне

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      SoftServe-Pride

    </td>

    

    <td>

    </td>

    

    <td>

      Львов

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      51

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Английский клуб

    </td>

    

    <td>

    </td>

    

    <td>

      Мариуполь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      52-53

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Арго

    </td>

    

    <td>

    </td>

    

    <td>

      Николаев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      52-53

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Эврика - СумГУ

    </td>

    

    <td>

    </td>

    

    <td>

      Сумы

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      54

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ШДК им. Р. Тагора

    </td>

    

    <td>

    </td>

    

    <td>

      Херсон

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      55

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Малыши

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      56

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ликбез

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      57-58

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ноль

    </td>

    

    <td>

    </td>

    

    <td>

      Винница

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      57-58

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ближний Восток

    </td>

    

    <td>

    </td>

    

    <td>

      Луганск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      59

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МСУ

    </td>

    

    <td>

    </td>

    

    <td>

      Симферополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      60

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ-2

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      61

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Альфа-Бета

    </td>

    

    <td>

    </td>

    

    <td>

      Николаев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      62

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ахтиар

    </td>

    

    <td>

    </td>

    

    <td>

      Севастополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      63

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ 



[Сайт турнира](http://intellect-club.narod.ru/wcup.html)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200305Odessa.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=1361)