+++
title = "Общий зачёт"
date = 2010-11-07T16:05:13
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2002/overall/"
slug = "overall"
aliases = [ "/post_14360", "/mak-cup/2002/overall",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Общий зачёт Кубка мира сезона 2002/03**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      Э

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr></p> 

  

  <p>

    <tr>

      <td>

        Genius

      </td>

      

      <td>

      </td>

      

      <td>

        Поташев

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        10

      </td>

      

      <td>

      </td>

      

      <td align="center">

        10

      </td>

      

      <td>

      </td>

      

      <td align="center">

        20

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        1

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Ксеп

      </td>

      

      <td>

      </td>

      

      <td>

        Севриновский

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9

      </td>

      

      <td>

      </td>

      

      <td align="center">

        18

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Ра

      </td>

      

      <td>

      </td>

      

      <td>

        Руссо

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9

      </td>

      

      <td>

      </td>

      

      <td align="center">

        4,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        4

      </td>

      

      <td>

      </td>

      

      <td align="center">

        13,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        Команда Кузьмина

      </td>

      

      <td>

      </td>

      

      <td>

        Кузьмин

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        13

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        4

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        Команда Губанова

      </td>

      

      <td>

      </td>

      

      <td>

        Губанов

      </td>

      

      <td>

      </td>

      

      <td>

        Петродворец

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        4

      </td>

      

      <td>

      </td>

      

      <td align="center">

        8

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        12

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        5

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        КП-Неспроста

      </td>

      

      <td>

      </td>

      

      <td>

        Белкин

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        7

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        ЮМА

      </td>

      

      <td>

      </td>

      

      <td>

        Виватенко

      </td>

      

      <td>

      </td>

      

      <td nowrap>

        Санкт-Петербург

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        7

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        Команда Мороховского

      </td>

      

      <td>

      </td>

      

      <td>

        Морозовский

      </td>

      

      <td>

      </td>

      

      <td>

        Одесса

      </td>

      

      <td>

      </td>

      

      <td>

        Украина

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        8

      </td>

      

      <td>

      </td>

      

      <td align="center">

        8

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        8

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        ОНУ им. Мечникова

      </td>

      

      <td>

      </td>

      

      <td>

        Санников

      </td>

      

      <td>

      </td>

      

      <td>

        Одесса

      </td>

      

      <td>

      </td>

      

      <td>

        Украина

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        7

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        9

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Стирол

      </td>

      

      <td>

      </td>

      

      <td>

        Левин

      </td>

      

      <td>

      </td>

      

      <td>

        Горловка

      </td>

      

      <td>

      </td>

      

      <td>

        Украина

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        10

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        СПС

      </td>

      

      <td>

      </td>

      

      <td>

        Бер

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        0,33

      </td>

      

      <td>

      </td>

      

      <td align="center">

        6,33

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        11

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Хонка

      </td>

      

      <td>

      </td>

      

      <td>

        Богословский

      </td>

      

      <td>

      </td>

      

      <td nowrap>

        Санкт-Петербург

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        5

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        12

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Тачанка

      </td>

      

      <td>

      </td>

      

      <td nowrap>

        Абрамов-Герт

      </td>

      

      <td>

      </td>

      

      <td nowrap>

        Ростов-на-Дону

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        0,33

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2,83

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        13

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        Социал-демократы

      </td>

      

      <td>

      </td>

      

      <td>

        Малышев

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2,5

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        14

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        От Винта - Братья по фазе

      </td>

      

      <td>

      </td>

      

      <td>

        Данько

      </td>

      

      <td>

      </td>

      

      <td>

        Харьков

      </td>

      

      <td>

      </td>

      

      <td>

        Украина

      </td>

      

      <td>

      </td>

      

      <td align="center">

        1

      </td>

      

      <td>

      </td>

      

      <td align="center">

        1

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        15

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        NB

      </td>

      

      <td>

      </td>

      

      <td>

        Шихов

      </td>

      

      <td>

      </td>

      

      <td>

        Кишинёв

      </td>

      

      <td>

      </td>

      

      <td>

        Молдова

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        АМО

      </td>

      

      <td>

      </td>

      

      <td>

        Мухин

      </td>

      

      <td>

      </td>

      

      <td>

        Минск

      </td>

      

      <td>

      </td>

      

      <td>

        Беларусь

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Импульс

      </td>

      

      <td>

      </td>

      

      <td>

        Беспалова

      </td>

      

      <td>

      </td>

      

      <td>

        Саранск

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Исида

      </td>

      

      <td>

      </td>

      

      <td>

        Толстобров

      </td>

      

      <td>

      </td>

      

      <td>

        Саранск

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Литрус

      </td>

      

      <td>

      </td>

      

      <td>

        Прохоров

      </td>

      

      <td>

      </td>

      

      <td>

        Висагинас

      </td>

      

      <td>

      </td>

      

      <td>

        Литва

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Немчиновка

      </td>

      

      <td>

      </td>

      

      <td>

        Майсюк

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Пента

      </td>

      

      <td>

      </td>

      

      <td>

        Славин

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td nowrap>

        Реал-Мордовия

      </td>

      

      <td>

      </td>

      

      <td>

        Беспалов

      </td>

      

      <td>

      </td>

      

      <td>

        Саранск

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Фиеста

      </td>

      

      <td>

      </td>

      

      <td>

        Агеев

      </td>

      

      <td>

      </td>

      

      <td>

        Саранск

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        Химэкс

      </td>

      

      <td>

      </td>

      

      <td>

        Денисов

      </td>

      

      <td>

      </td>

      

      <td>

        Саранск

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        -

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        2

      </td>

      

      <td>

      </td>

      

      <td align="center" nowrap>

        16-25

      </td>

    </tr>

  </p>

  

  <p>

    <tr>

      <td>

        МГТУ

      </td>

      

      <td>

      </td>

      

      <td>

        Смирнов

      </td>

      

      <td>

      </td>

      

      <td>

        Москва

      </td>

      

      <td>

      </td>

      

      <td>

        Россия

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

      </td>

      

      <td>

      </td>

      

      <td align="center">

        3

      </td>

      

      <td>

      </td>

      

      <td align="center">

        26

      </td>

    </tr>

  </p>

  

  <p>

    </table> 

    

    <p>

      <strong>1</strong> - очки на 1-м этапе<br /> <strong>2</strong> - очки на 2-м этапе<br /> <strong>3</strong> - очки на 3-м этапе<br /> <strong>О</strong> - очки в общем зачёте<br /> <strong>Э</strong> - сыгранные этапы<br /> <strong>М</strong> - место в общем зачёте

    </p>

    

    <p>

      &nbsp;

    </p>

    

    <p>

      <strong>Genius (Москва, Россия)</strong>

    </p>

    

    <ul>

      <li>

        Елена Александрова

      </li>

      <li>

        Вадим Карлинский

      </li>

      <li>

        Дмитрий Коноваленко

      </li>

      <li>

        Максим Поташев (к)

      </li>

      <li>

        Владимир Степанов

      </li>

      <li>

        Антон Чернин

      </li>

      <li>

        Юрий Черушев

      </li>

    </ul>

  </p>