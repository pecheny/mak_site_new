+++
title = "Этап КМ, “Белые ночи”"
date = 2011-10-15T16:48:51
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2009/stage1/"
slug = "stage1"
aliases = [ "/post_18906", "/mak-cup/2009/stage1",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**XIX международный фестиваль &#8220;Белые ночи&#8221;, этап Кубка мира сезона 2009/10**



**Санкт-Петербург, 4-5 июля 2009 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      С

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Salvatore

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Фикус

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ла Скала

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Немчиновка

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      58,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Припев 2 раза

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      66,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      69

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Ершова

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      70,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      АСБ-2

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      73,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Eclipse Mix

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      80,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Gambler

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      81

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Зарница

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      90,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Чудо в перьях

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      85

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td>

      ГРМ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      96,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      No name

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      92

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      НВ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      94,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

  </tr>

  

  <tr>

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      97

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

  </tr>

  

  <tr>

    <td>

      Нимлот

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      103,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ч-2

    </td>

    

    <td>

    </td>

    

    <td>

      Калининград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      109

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Коваленко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великий Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      112

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ГРМ-2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      114

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td>

      Синоп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      115,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Уездный доктор

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      122

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td>

      Архимед

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      122,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      6 из 45

    </td>

    

    <td>

    </td>

    

    <td>

      Красноярск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      127

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td>

      Integro

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      127

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td>

      Опухлики

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      142,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда №2

    </td>

    

    <td>

    </td>

    

    <td>

      Тула

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      143

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Чёрный квадрат

    </td>

    

    <td>

    </td>

    

    <td>

      Северодвинск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      143,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      В-Ч-Ж

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      152,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Янович

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      158

    </td>

    

    <td>

    </td>

    

    <td align="center">

      34

    </td>

  </tr>

  

  <tr>

    <td>

      Лимпопо

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      159

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      X-Ray

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      164

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td>

      Эспоо

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      163

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      По фигу

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      172,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      38

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Палата №N

    </td>

    

    <td>

    </td>

    

    <td>

      Тула

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      191,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      39

    </td>

  </tr>

  

  <tr>

    <td>

      ИНЖЭКОН

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      193

    </td>

    

    <td>

    </td>

    

    <td align="center">

      40

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Григоряна

    </td>

    

    <td>

    </td>

    

    <td>

      Гюмри

    </td>

    

    <td>

    </td>

    

    <td>

      Армения

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      195,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      41

    </td>

  </tr>

  

  <tr>

    <td>

      Альбус

    </td>

    

    <td>

    </td>

    

    <td>

      Владикавказ

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      191,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      42

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 01.06.2009

  

**В** - количество взятых вопросов

  

**С** - сумма мест

  

**М** - место в зачёте этапа 



[Сайт турнира](http://belyenochi.chgk.info/2009/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200907SPb.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=485)