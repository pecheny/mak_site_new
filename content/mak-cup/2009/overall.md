+++
title = "Общий зачёт"
date = 2011-10-18T02:58:05
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2009/overall/"
slug = "overall"
aliases = [ "/post_18941", "/mak-cup/2009/overall",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Общий зачёт Кубка мира сезона 2009/10**



**Чемпионат мира, Эйлат, 11-14 ноября 2010 года. Финал**

  

(результаты команд - победительниц этапов Кубка мира)



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Ч

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ленский, Семушин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td>

      Скородумов

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**Ч** - место в зачёте чемпионата

  

**М** - место в общем зачёте



[Сайт чемпионата мира](http://znatokiada.chgk.info/2010)



&nbsp;



**ЛКИ (Москва, Россия)**



  * Дмитрий Белявский

  * Александра Брутер

  * Андрей Ленский (к)

  * Роман Немучинский

  * Вероника Ромашова

  * Максим Руссо

  * Иван Семушин (к)

  * Ольга Хворова