+++
title = "Этап КМ, &#171;Славянка&#187;"
date = 2011-10-18T02:24:41
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2009/etap-km-slavyanka/"
slug = "stage2"
aliases = [ "/post_18940", "/mak-cup/2009/etap-km-slavyanka",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**II международный турнир &#8220;Славянка-2009&#8221;, этап Кубка мира сезона 2009/10**



**Витебск, 29-30 августа 2009 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Р

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      59

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1171

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1126

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1110

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Кирибати

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1099

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Salvatore

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1060

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="right">

      990

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="right">

      937

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Тайга

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="right">

      932

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Быдло бескультурное

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="right">

      890

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Бавина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="right">

      881

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Бандерлоги

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="right">

      875

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Аномалия

    </td>

    

    <td>

    </td>

    

    <td>

      Калуга

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="right">

      797

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Зарница

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="right">

      846

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="right">

      773

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Биркиркара

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="right">

      762

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Encounter

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="right">

      739

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      South Butovo Park

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="right">

      661

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Кефирные грибки

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="right">

      679

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Ультиматум

    </td>

    

    <td>

    </td>

    

    <td>

      Гомель

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      611

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

  </tr>

  

  <tr>

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      608

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Брутальные мужики

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      604

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Коваленко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великий Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      591

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Дикси-С

    </td>

    

    <td>

    </td>

    

    <td>

      Смоленск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      584

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Почти что гении

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      567

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

  </tr>

  

  <tr>

    <td>

      Столичная

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      560

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бензопила &#8220;Дружба&#8221;

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      552

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

  </tr>

  

  <tr>

    <td>

      Сирин

    </td>

    

    <td>

    </td>

    

    <td>

      Волгоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      566

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

  </tr>

  

  <tr>

    <td>

      Gambler

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      543

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

  </tr>

  

  <tr>

    <td>

      Форум

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      538

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

  </tr>

  

  <tr>

    <td>

      Signum

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      512

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Medium

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      475

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

  </tr>

  

  <tr>

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      471

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

  </tr>

  

  <tr>

    <td>

      Пилигрим

    </td>

    

    <td>

    </td>

    

    <td>

      Новополоцк

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      397

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Витебский трамвай

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      389

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

  </tr>

  

  <tr>

    <td>

      Felidae

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      405

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Енотики-7

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      330

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

  </tr>

  

  <tr>

    <td>

      Ледокол

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великие Луки

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      328

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Хронически разумные United

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      348

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

  </tr>

  

  <tr>

    <td>

      Шулер

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      331

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

  </tr>

  

  <tr>

    <td>

      Наши

    </td>

    

    <td>

    </td>

    

    <td>

      Гомель

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      299

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ход конём

    </td>

    

    <td>

    </td>

    

    <td>

      Витебск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      238

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ФУПМ-Party

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      159

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

  </tr>

  

  <tr>

    <td>

      Дельтаплан

    </td>

    

    <td>

    </td>

    

    <td>

      Новополоцк

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      144

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

  </tr>

  

  <tr>

    <td>

      Лыжню!

    </td>

    

    <td>

    </td>

    

    <td>

      Псков

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      121

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 01.06.2009

  

**В** - количество взятых вопросов

  

**Р** - рейтинг

  

**М** - место в зачёте этапа 



[Сайт турнира](http://sl.blik.by/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200908Vitebsk.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=506)