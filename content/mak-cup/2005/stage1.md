+++
title = "Этап КМ, МГТУ"
date = 2011-10-01T18:12:24
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2005/stage1/"
slug = "stage1"
aliases = [ "/post_18722", "/mak-cup/2005/stage1",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Фестиваль интеллектуальных игр &#8220;175 лет МГТУ!&#8221;, этап Кубка мира сезона 2005/06**



**Москва, 29-30 октября 2005 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Р

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="right">

      825

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Гросс-бух

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="right">

      821

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ЮМА - Энергокапитал

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="right">

      736

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="right">

      661

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Тайга

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      505

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Импульс - Золотое сечение

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      441

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Снарк

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      418

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Тверь-икс

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      386

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Буравчик

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      395

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      БорисКо

    </td>

    

    <td>

    </td>

    

    <td>

      Ступино

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      385

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Сборная ГУ-ВШЭ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      361

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Z

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      315

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      ТвГУ

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      259

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Ума палата

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      220

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Быдло бескультурное

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      216

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      НовоКАИн

    </td>

    

    <td>

    </td>

    

    <td>

      Казань

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      184

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Какая разница

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      154

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Полис

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      113

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дельта фи

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      120

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ - Поехали!

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      105

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Alter orbis

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      77

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Никак

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Backspace

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      66

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Вне зачёта

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ - Липа

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**Р** - рейтинг

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ



[Страница турнира в Летописи](http://letopis.chgk.info/200510MoscowBauman.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=123)