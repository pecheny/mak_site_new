+++
title = "Этап КМ, “Белые ночи”"
date = 2011-09-30T02:33:00
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2004/stage4/"
slug = "stage4"
aliases = [ "/post_18703", "/mak-cup/2004/stage4",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**ХV Международный фестиваль &#8220;Белые ночи - Энергокапитал&#8221;, этап Кубка мира сезона 2004/05**



**Санкт-Петербург, 2-3 июля 2005 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      С

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Богословского

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      60

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      59

    </td>

    

    <td>

    </td>

    

    <td align="center">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Гросс-бух

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      58

    </td>

    

    <td>

    </td>

    

    <td align="center">

      39,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      58

    </td>

    

    <td>

    </td>

    

    <td align="center">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td>

      Симферополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center">

      48,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      55,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Иткина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      71,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Приматы-Шейк

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      94,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Архимед

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      97,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center">

      98,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Ноев Ковчег

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center">

      103,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Импульс - Золотое сечение

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      105

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      107

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      108,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Вологда

    </td>

    

    <td>

    </td>

    

    <td>

      Вологда

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      113

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      112

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Palmali

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      115,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      120

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дилемма

    </td>

    

    <td>

    </td>

    

    <td>

      Саратов

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      120,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ЧП

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      121,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Розовый слон

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нижний Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      129

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Синоп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      132

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      132,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Холдинг Потребительский

    </td>

    

    <td>

    </td>

    

    <td>

      Челябинск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      148

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Форум

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      142,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ГРМ-дубль

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      149

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Старики-разбойники

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      149,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ГРМ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      155

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ВГ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      157

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МИРаж

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      162

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Коваленко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Великий Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      175

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Опухлики

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      174

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Борьба с умом

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      184

    </td>

    

    <td>

    </td>

    

    <td align="center">

      34

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      БРИФ

    </td>

    

    <td>

    </td>

    

    <td>

      Краснодар

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      190,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Гелиос

    </td>

    

    <td>

    </td>

    

    <td>

      Астрахань

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      189

    </td>

    

    <td>

    </td>

    

    <td align="center">

      36

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Буравчик

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      204,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      37

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Мурманск

    </td>

    

    <td>

    </td>

    

    <td>

      Мурманск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      226

    </td>

    

    <td>

    </td>

    

    <td align="center">

      38

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Амира

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      227

    </td>

    

    <td>

    </td>

    

    <td align="center">

      39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Чёрный квадрат

    </td>

    

    <td>

    </td>

    

    <td>

      Северодвинск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      228,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Малябра

    </td>

    

    <td>

    </td>

    

    <td>

      Пермь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      229,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      41

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ИНЖЭКОН-2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      234,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      42

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Восточный экспресс

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      240,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      43

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Тридцатиногая сороконожка

    </td>

    

    <td>

    </td>

    

    <td>

      Иркутск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      241

    </td>

    

    <td>

    </td>

    

    <td align="center">

      44

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Зона поражения

    </td>

    

    <td>

    </td>

    

    <td>

      Березники

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      242,5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      45-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ИНЖЭКОН

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      242,5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      45-46

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Оксюморон

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      250,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      47

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Северсталь

    </td>

    

    <td>

    </td>

    

    <td>

      Череповец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      247

    </td>

    

    <td>

    </td>

    

    <td align="center">

      48

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Мир

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      257,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      49

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Братья по разуму

    </td>

    

    <td>

    </td>

    

    <td>

      Смоленск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      261

    </td>

    

    <td>

    </td>

    

    <td align="center">

      50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Бронепоезд из Ромашково

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      263,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      51

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      БорисКо

    </td>

    

    <td>

    </td>

    

    <td>

      Ступино

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      272

    </td>

    

    <td>

    </td>

    

    <td align="center">

      52

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Пионеры ЭКЗ

    </td>

    

    <td>

    </td>

    

    <td>

      Кольчугино

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      271

    </td>

    

    <td>

    </td>

    

    <td align="center">

      53

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td>

      Эспоо

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      278

    </td>

    

    <td>

    </td>

    

    <td align="center">

      54

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ToorMoos

    </td>

    

    <td>

    </td>

    

    <td>

      Тарту

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      269

    </td>

    

    <td>

    </td>

    

    <td align="center">

      55

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Несчастный случай

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      270,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      56

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Ч-2

    </td>

    

    <td>

    </td>

    

    <td>

      Калининград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      296,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      57

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Yes, problems!

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      298,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      58

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Союз

    </td>

    

    <td>

    </td>

    

    <td>

      Радужный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      303,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      59

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дашковцы

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      320,5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      60

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Пеньки с глазами

    </td>

    

    <td>

    </td>

    

    <td>

      Череповец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      321

    </td>

    

    <td>

    </td>

    

    <td align="center">

      61

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**С** - сумма мест

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ



[Сайт турнира](http://belyenochi.chgk.info/2005/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/200507SPb.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=112)