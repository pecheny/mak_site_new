+++
title = "Этап КМ, “СамариУМ”"
date = 2011-09-26T02:33:55
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/2004/stage1/"
slug = "stage1"
aliases = [ "/post_18634", "/mak-cup/2004/stage1",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Фестиваль интеллектуальных игр &#8220;СамариУМ-2004&#8221;, этап Кубка мира сезона 2004/05**



**Самара, 6-8 ноября 2004 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      Р

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="right">

      494

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="right">

      509

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="right">

      492

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Иткина

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="right">

      471

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Импульс - Золотое сечение

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="right">

      409

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="right">

      427

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="right">

      391

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Тачанка

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="right">

      395

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Гросс-бух

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      352

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Разъезд Восстания

    </td>

    

    <td>

    </td>

    

    <td>

      Казань

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      351

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      МУР-ЛЭТИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      350

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Розовый слон

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нижний Новгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="right">

      348

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      344

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      339

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td>

      Саратов

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      337

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Биотех

    </td>

    

    <td>

    </td>

    

    <td>

      Саратов

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      392

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Шанс

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      320

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      319

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Дзяофани

    </td>

    

    <td>

    </td>

    

    <td>

      Ковров

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      312

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МИРаж

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      298

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      НТР

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      256

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Анталлактика

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      239

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Фиеста

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      215

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ФУПМ-Party

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      167

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Команда Евелева

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      183

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Полный Физтех

    </td>

    

    <td>

    </td>

    

    <td>

      Долгопрудный

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      183

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ОГУЗ

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Похоронная

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**В** - количество взятых вопросов

  

**Р** - рейтинг

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ 



[Страница турнира в Летописи](http://letopis.chgk.info/200411Samara.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=67)