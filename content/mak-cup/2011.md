+++
title = "Кубок МАК сезона 2011/12"
date = 2011-10-21T02:01:24
author = "Антон Губанов"
guid = "http://mak-chgk.ru/mak-cup/statistics/"
slug = "kubok-mak-sezona-2011-12"
aliases = [ "/post_18995", "/mak-cup/statistics",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**Правила**



[Регламент](http://mak-chgk.ru/mak-cup/2011/reglament)



[Условия предоставления статуса ЭКМ](http://mak-chgk.ru/mak-cup/2010/conditions)



&nbsp;



**Результаты**



[&#8220;Славянка&#8221;, 04-05.08.2012](http://mak-chgk.ru/mak-cup/2011/stage1)



[Общий зачёт](http://mak-chgk.ru/mak-cup/2011/overall)