+++
title = "Этап КМ, Открытый кубок Черноземья"
date = 2014-07-07T00:46:09
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=21952"
slug = "stage1"
aliases = [ "/post_21952",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

**II Открытый кубок Черноземья по интеллектуальным играм**



**Воронеж, 7-8 июня 2014 года**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      Т

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      75

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      74

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td>

      Мираж

    </td>

    

    <td>

    </td>

    

    <td>

      Самара

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      71

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Негодный буцентавр

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      69

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная юга

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      67

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Павла Ершова

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      64

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Keiseker

    </td>

    

    <td>

    </td>

    

    <td>

      Киев

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      62

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Перелётный кабак

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      62

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Призраки Коши

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      +

    </td>

    

    <td>

    </td>

    

    <td>

      60

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td>

      Церебрум

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      59

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Механизм реакции

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      58

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Хронически разумные United

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      58

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сокращённо 177С

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Спонсора.net

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2,5

    </td>

  </tr>

  

  <tr>

    <td>

      Водоворот

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      0,5

    </td>

  </tr>

  

  <tr>

    <td>

      Сельдь

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      0,5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Севера

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      София

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Alter orbis

    </td>

    

    <td>

    </td>

    

    <td>

      Тверь

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Памяти Пауля

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Шесть без козыря

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Шесть в большом городе

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Noname

    </td>

    

    <td>

    </td>

    

    <td>

      Волгоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      52

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Химера

    </td>

    

    <td>

    </td>

    

    <td>

      Зеленоград

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      52

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ни стыда, ни совести

    </td>

    

    <td>

    </td>

    

    <td>

      Краснодар

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ППП

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Сфинкс

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      БН

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      МГТУ

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ДНК

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      47

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Северная корона

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      47

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      За!нзибар

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      46

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Летающий цирк Инала Хаева

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Гугол скифов

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      34

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ан-Ти

    </td>

    

    <td>

    </td>

    

    <td>

      Орёл

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      35

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Британские учёные

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      36-39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Гвоздь

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      36-39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Несомненный скрамасакс

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      36-39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ОМ

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      36-39

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      БорисКо

    </td>

    

    <td>

    </td>

    

    <td>

      Ступино

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      40

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Потанинцы

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      41

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Olymp

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      42-43

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Опережая мысли

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      42-43

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Международец

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      44

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Одуванчик над коллайдером

    </td>

    

    <td>

    </td>

    

    <td>

      Тула

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      45

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Авалон

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      31

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      46-48

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Поросёнок Пётр

    </td>

    

    <td>

    </td>

    

    <td>

      Орёл

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      31

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      46-48

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Чердак

    </td>

    

    <td>

    </td>

    

    <td>

      Ярославль

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      31

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      46-48

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Два капитана

    </td>

    

    <td>

    </td>

    

    <td>

      Липецк

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      49-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Шесть шекелей

    </td>

    

    <td>

    </td>

    

    <td>

      Брянск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      49-50

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Машина времени

    </td>

    

    <td>

    </td>

    

    <td>

      Липецк

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      51-52

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Пуззли

    </td>

    

    <td>

    </td>

    

    <td>

      Ярославль

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      51-52

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Пумбы-тащумбы

    </td>

    

    <td>

    </td>

    

    <td>

      Курск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      53

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Альтаир

    </td>

    

    <td>

    </td>

    

    <td>

      Брянск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      54

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      ХАМяки

    </td>

    

    <td>

    </td>

    

    <td>

      Белгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      55

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Полный истец

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      56-57

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ура-gun

    </td>

    

    <td>

    </td>

    

    <td>

      Брянск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      56-57

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Арсенал

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      58-59

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Тюбик

    </td>

    

    <td>

    </td>

    

    <td>

      Белгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      58-59

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Джеронимо!

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ростов-на-Дону

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      60

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Легион Шрёдингера

    </td>

    

    <td>

    </td>

    

    <td>

      Белгород

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      61

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Oldschool

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      62-66

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Генераторы идей

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      62-66

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Квазар

    </td>

    

    <td>

    </td>

    

    <td>

      Нововоронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      62-66

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Плеяда

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      62-66

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Револьвер

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      62-66

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

      Фация

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      67

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Никитинская, 35

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      68

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Барсуки британских островов

    </td>

    

    <td>

    </td>

    

    <td>

      Воронеж

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td>

      -

    </td>

    

    <td>

    </td>

    

    <td>

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      69

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

  </tr>

</table>



**Т** - присутствие в топ-20 рейтинга МАК на 27.02.2014

  

**В** - количество взятых вопросов

  

**М** - место в зачёте этапа

  

**О** - количество призовых очков в общий зачёт КМ 



[Сообщество турнира в &#8220;Живом журнале&#8221;](http://chernozemfest.livejournal.com/)

  

[Страница турнира в Летописи](http://letopis.chgk.info/201405Voronezh-invit.html)

  

[Страница турнира в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=2728)