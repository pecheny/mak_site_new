+++
title = "Кубок МАК (Кубок мира)"
date = 2010-06-18T23:25:15
author = "Антон Губанов"
guid = "http://mak-chgk.ru/worldcup/"
slug = "mak-cup"
aliases = [ "/post_12742", "/worldcup",]
type = "post"
categories = [ "Кубок МАК",]
tags = []
+++

Кубок МАК - серия международных турниров одного игрового сезона, объединённых общим зачётом. Проводится с сезона 2002/03. В сезонах 2002/03 - 2009/10 соревнование называлось Кубком мира. 



[Победители](http://mak-chgk.ru/mak-cup/winners)



[Статистика](http://mak-chgk.ru/mak-cup/statistics)



[Кубок мира сезона 2002/03](http://mak-chgk.ru/mak-cup/2002)



[Кубок мира сезона 2003/04](http://mak-chgk.ru/mak-cup/2003)



[Кубок мира сезона 2004/05](http://mak-chgk.ru/mak-cup/2004)



[Кубок мира сезона 2005/06](http://mak-chgk.ru/mak-cup/2005)



[Кубок мира сезона 2006/07](http://mak-chgk.ru/mak-cup/2006)



[Кубок мира сезона 2007/08](http://mak-chgk.ru/mak-cup/2007)



[Кубок мира сезона 2008/09](http://mak-chgk.ru/mak-cup/2008)



[Кубок мира сезона 2009/10](http://mak-chgk.ru/mak-cup/2009)



[Кубок МАК сезона 2010/11](http://mak-chgk.ru/mak-cup/2010)



[Кубок МАК сезона 2011/12](http://mak-chgk.ru/mak-cup/2011)



[Кубок МАК сезона 2012/13](http://mak-chgk.ru/mak-cup/2012)



[Кубок МАК сезона 2013/14](http://mak-chgk.ru/mak-cup/2013)