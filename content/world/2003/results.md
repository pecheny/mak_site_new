+++
title = "Результаты"
date = 2012-04-22T22:11:18
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=20832"
slug = "results"
aliases = [ "/post_20832",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Итоговая классификация**



<table>

  <tr>

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

  </tr>

  

  <tr>

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td>

      Genius

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Севриновский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Белкин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td>

      Незнайка

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Колмаков

    </td>

    

    <td>

    </td>

    

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Иткина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Иткин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Санников

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Стирол

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Левин

    </td>

    

    <td>

    </td>

    

    <td>

      Горловка

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Перлин

    </td>

    

    <td>

    </td>

    

    <td>

      Кёльн

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      ЮМА-Яблоко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Виватенко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Данько

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Друзь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Руссо

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Тейтельман

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Пало-Альто

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Беспалов

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Хатни Бусел

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Л. Климович

    </td>

    

    <td>

    </td>

    

    <td>

      Гомель

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Азимова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Азимов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td>

      Aztech

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ф. Гусейнов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Алиева

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      З. Алиев

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      О. Кирничанская

    </td>

    

    <td>

    </td>

    

    <td>

      Даугавпилс

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td>

      Ликбез

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Г. Алхазов

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td>

      Котка

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Кулев

    </td>

    

    <td>

    </td>

    

    <td>

      Тбилиси

    </td>

    

    <td>

    </td>

    

    <td>

      Грузия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Третий путь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Бутуришвили

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

  </tr>

  

  <tr>

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td>

      ЛитРус

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Прохоров

    </td>

    

    <td>

    </td>

    

    <td>

      Висагинас

    </td>

    

    <td>

    </td>

    

    <td>

      Литва

    </td>

  </tr>

</table>



&nbsp;



**Первый этап, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Genius

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Незнайка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Иткина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Стирол

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ЮМА-Яблоко

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Хатни Бусел

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Азимова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Aztech

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Алиева

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Ликбез

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td>

      Котка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Третий путь

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

  </tr>

  

  <tr>

    <td>

      Литрус

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Первый этап, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Genius

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Незнайка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Иткина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Стирол

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ЮМА-Яблоко

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td>

      Ра

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Хатни Бусел

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Азимова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Aztech

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Алиева

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Ликбез

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td>

      Котка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Третий путь

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

  </tr>

  

  <tr>

    <td>

      Литрус

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Второй этап**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Genius

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Незнайка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

</table>



**1**, **2** - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



1. Genius (Москва, Россия)



  * Елена Александрова

  * Вадим Карлинский

  * Дмитрий Коноваленко

  * Максим Поташев (к)

  * Владимир Степанов

  * Антон Чернин

  * Юрий Черушев



2. Ксеп (Москва, Россия)



  * Игорь Бахарев

  * Станислав Мереминский

  * Роман Немучинский

  * Илья Новиков

  * Владимир Севриновский (к)

  * Антон Снятковский



3. Golden Telecom (Москва, Россия)



  * Лариса Архипова

  * Павел Володин

  * Андрей Кузьмин (к)

  * Михаил Левандовский

  * Александр Либер

  * Валентин Мельников

  * Пётр Сухачёв