+++
title = "Расписание"
date = 2012-02-16T02:11:25
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2002/schedule/"
slug = "schedule"
aliases = [ "/post_20489", "/world/2002/schedule",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**13 июня, четверг**

  

17:00 (_МСК_) - Вылет из Москвы (_Домодедово_)

  

21:00-22:00 (_здесь и далее - МСК+1_) - Размещение в гостинице &#8220;Апшерон&#8221;

  

22:00-23:00 - Ужин (_гостиница &#8220;Апшерон&#8221;_)



**14 июня, пятница**

  

09:45-10:30 - Собрание капитанов команд (_фойе гостиницы &#8220;Апшерон&#8221;_)

  

10:00-11:00 - Завтрак (_гостиница &#8220;Апшерон&#8221;_)

  

11:00-13:00 - Отборочные игры брейн-ринга (_гостиница &#8220;Апшерон&#8221;_)

  

13:00-14:00 - Обед (_гостиница &#8220;Апшерон&#8221;_)

  

14:00-16:30 - Отборочные игры брейн-ринга (_гостиница &#8220;Апшерон&#8221;_)

  

17:00-18:00 - Репетиция торжественного открытия (_концертный зал &#8220;Шахрияр&#8221;_)

  

18:00-20:00 - Торжественное открытие ЧМ, игры с залом, полуфиналы и финал &#8220;Брейн-ринга (_концертный зал &#8220;Шахрияр&#8221;_)

  

21:00-22:00 - Ужин (_гостиница &#8220;Апшерон&#8221;_)



**15 июня, суббота**

  

10:00-11:00 - Завтрак (_гостиница &#8220;Апшерон&#8221;_)

  

11:00-14:00 - Культурная программа (_пляж, экскурсии по городу_)

  

14:00-16:00 - 1-й и 2-й отборочные туры ЧМ (_зал отеля &#8220;Европа&#8221;_)

  

16:00-17:00 - Обед (_гостиница &#8220;Апшерон&#8221;_)

  

17:00-19:00 - 3-й и 4-й отборочные туры ЧМ (_зал отеля &#8220;Европа&#8221;_)

  

19:30-20.30 - Ужин (_гостиница &#8220;Апшерон&#8221;_)

  

20:30 - Заседание Правления МАК (_гостиница &#8220;Апшерон&#8221;_)



**16 июня, воскресенье**

  

10:00-11:00 - Завтрак (_гостиница &#8220;Апшерон&#8221;_)

  

11:00-14:00 - Культурная программа (_пляж, экскурсии по городу_)

  

14:00-16:00 - Финал ЧМ (_зал отеля &#8220;Европа&#8221;_)

  

19:00-21:00 - Шоу-игры. Игра бакинских зрителей против сборной Элитарного клуба. Награждение участников ЧМ. Церемония закрытия ЧМ (_зал отеля &#8220;Европа&#8221;_)

  

21:00 - Заключительный банкет (_гостиница &#8220;Апшерон&#8221;_)



**17 июня, понедельник**

  

10:30 - Вылет из Баку