+++
title = "Результаты"
date = 2012-06-12T13:59:36
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=21029"
slug = "results"
aliases = [ "/post_21029",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Итоговая классификация**



<table>

  <tr>

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

  </tr>

  

  <tr>

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Белкин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Севриновский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Поташев

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Спешков

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Виватенко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Тальянский

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Тель-Авив

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ф. Гусейнов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Ленский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Шишко

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td>

      Катус

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Р. Семизаров

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Данько

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Против ветра

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Брукман

    </td>

    

    <td>

    </td>

    

    <td>

      Дюссельдорф

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Беспалов

    </td>

    

    <td>

    </td>

    

    <td>

      Саранск

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Слон в удаве

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Н. Ибрагимова

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Санников

    </td>

    

    <td>

    </td>

    

    <td>

      Одесса

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      П. Малышев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Друзь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      К. Бриф

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нью-Йорк

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Касумов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td>

      Вестимо

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Т. Зильберштейн

    </td>

    

    <td>

    </td>

    

    <td>

      Торонто

    </td>

    

    <td>

    </td>

    

    <td>

      Канада

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Троцюк

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

  </tr>

  

  <tr>

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Тваскис

    </td>

    

    <td>

    </td>

    

    <td>

      Ереван

    </td>

    

    <td>

    </td>

    

    <td>

      Армения

    </td>

  </tr>

  

  <tr>

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Арш

    </td>

    

    <td>

    </td>

    

    <td>

      Лондон

    </td>

    

    <td>

    </td>

    

    <td>

      Великобритания

    </td>

  </tr>

  

  <tr>

    <td align="center">

      26

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      TGDNSKY KOHb

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      О. Госман

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      27

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Омадов

    </td>

    

    <td>

    </td>

    

    <td>

      Ашхабад

    </td>

    

    <td>

    </td>

    

    <td>

      Туркмения

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Карлов Моск

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Ширяев

    </td>

    

    <td>

    </td>

    

    <td>

      Прага

    </td>

    

    <td>

    </td>

    

    <td>

      Чехия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Команда

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Жибуртович

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

  </tr>

  

  <tr>

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Мы-6

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Борисов

    </td>

    

    <td>

    </td>

    

    <td>

      Хельсинки

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В яблочко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Н. Собачкин

    </td>

    

    <td>

    </td>

    

    <td>

      Висагинас

    </td>

    

    <td>

    </td>

    

    <td>

      Литва

    </td>

  </tr>

  

  <tr>

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Р. Саид-Аминов

    </td>

    

    <td>

    </td>

    

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

  </tr>

</table>



&nbsp;



**Отборочный турнир, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Катус

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Против ветра

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Слон в удаве

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

  </tr>

  

  <tr>

    <td>

      Вестимо

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      TGDNSKY KOHb

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Карлов моск

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td>

      Команда

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мы-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      В яблочко

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-34

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Отборочный турнир, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

  </tr>

  

  <tr>

    <td>

      Катус

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Против ветра

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Реал-Мордовия

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Слон в удаве

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      ОНУ им. Мечникова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

  </tr>

  

  <tr>

    <td>

      Вестимо

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      TGDNSKY KOHb

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Карлов моск

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td>

      Команда

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мы-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      В яблочко

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Перестрелка за 6 место в отборочном турнире**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

</table>



**1** и **2** - номер вопроса

  

**О** - сумма очков

  

**М** - место в отборочном турнире 



&nbsp;



**Финал, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Финал, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Перестрелка за 2-4 места**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место в чемпионате 



&nbsp;



1. Неспроста (Москва, Россия)



  * Юлия Архангельская

  * Анатолий Белкин (к)

  * Дмитрий Лурье

  * Антон Снятковский

  * Артём Сорожкин

  * Виталий Фёдоров



2. Команда Кузьмина (Москва, Россия)



  * Дмитрий Борок

  * Павел Володин

  * Евгений Калюков

  * Андрей Кузьмин (к)

  * Михаил Левандовский

  * Александр Либер

  * Пётр Сухачёв

  * Борис Чигидин



3. Команда Губанова (Петродворец, Россия)



  * Ольга Берёзкина

  * Юрий Выменец

  * Антон Губанов (к)

  * Михаил Матвеев

  * Борис Моносов

  * Александр Скородумов