+++
title = "Расписание"
date = 2012-02-23T20:21:55
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2008/schedule/"
slug = "schedule"
aliases = [ "/post_20582", "/world/2008/schedule",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**17-18 сентября, среда-четверг**

  

С 12:00 - Поселение в гостиницу



**19 сентября, пятница**

  

08:00-10:00 - Завтрак

  

11:00-13:30 - 1-2 туры чемпионата мира

  

14:00-15:00 - Обед

  

16:00-17:00 - 3 тур чемпионата мира

  

17:00-20:00 - Развлекательная игровая программа

  

20:30-21:30 - Ужин

  

21:30-00:00 - Развлекательная игровая программа



**20 сентября, суббота**

  

08:00-10:30 - Завтрак

  

11:00-12:00 - 4 тур чемпионата мира

  

12:30-13:00 - Перестрелка

  

13:00-14:00 - Обед

  

14:00-17:00 - 1-3 туры финала чемпионата мира

  

17:00 - Шествие и игра &#8220;Жители Калининградской области против клуба &#8220;Что? Где? Когда?&#8221;



**21 сентября, воскресенье**

  

12:00 - Расчётный час в гостинице