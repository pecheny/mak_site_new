+++
title = "Расписание"
date = 2012-02-23T20:36:17
author = "Антон Губанов"
guid = "http://mak-chgk.ru/20584/"
slug = "schedule"
aliases = [ "/post_20584", "/20584",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**11 ноября, четверг**

  

07:00-17:30 - Прибытие участников

  

14:30 - Экскурсия в подводную обсерваторию (_сбор у входа в отель_)

  

17:00-19:00 - Футбол (_сбор у входа в отель_)

  

19:00-20:30 - Ужин

  

20:30-21:15 - Письменный отбор на турнир по спортивной &#8220;Своей игре&#8221; (_зал &#8220;Океан&#8221;_)

  

21:15-00:30 - &#8220;Кубок Дружбы&#8221; под эгидой ЕАЕК (турнир смешанными составами) (_зал &#8220;Океан&#8221;_)



**12 ноября, пятница**

  

08:00-10:00 - Завтрак

  

09:45-13:30 - Турнир по олимпийской системе, 1-4 туры (_зал &#8220;Океан&#8221;_)

  

10:45-13:30 - &#8220;Даугавпилс&#8221; (_лекционный зал, корпус &#8220;Пасифик&#8221;_)

  

13:15-14:45 - Обед

  

14:45-15:45 - Открытие чемпионата мира (_зал &#8220;Аудиториум&#8221;_)

  

15:45-19:00 - Предварительный этап чемпионата мира / Открытый турнир, 1-3 туры (_зал &#8220;Океан&#8221;_)

  

19:00-20:15 - Ужин

  

20:15-00:00 - Турнир по спортивной &#8220;Своей игре&#8221; (_зал &#8220;Океан&#8221; и зал спиннинга_)

  

20:15-00:00 - Турнир по &#8220;Своей Игре&#8221;-микст (_лекционный зал, корпус &#8220;Пасифик&#8221;_)

  

20:15-00:00 - &#8220;Недостоверный турнир&#8221; (_зал &#8220;Мигдалор&#8221;_)



**13 ноября, суббота**

  

08:00-10:00 - Завтрак

  

09:45-14:15 - Турнир по &#8220;Брейн-рингу&#8221; (_зал &#8220;Океан&#8221; и зал &#8220;Аудиториум&#8221;_)

  

14:15-15:45 - Обед

  

15:45-19:30 - Предварительный этап чемпионата мира / Открытый турнир, 4-6 туры (_зал &#8220;Океан&#8221;_)

  

19:30-20:30 - Ужин

  

20:30-21:00 - Возможная перестрелка. Оглашение результатов Предварительного этапа и Открытого турнира (_зал &#8220;Океан&#8221;_)

  

21:00-23:00 - Алфавитный турнир (_зал &#8220;Океан&#8221;_)

  

23.00-02:00 - Ночная развлекательная программа &#8220;Килты, виски, два ствола&#8221; (_Зал &#8220;Океан&#8221;_)



**14 ноября, воскресенье**

  

08:00-10:00 - Завтрак

  

10:00-12:00 - Финалы &#8221; Своей игры&#8221;, &#8220;Брейн-ринга&#8221; и олимпийского турнира (_зал &#8220;Аудиториум&#8221;_)

  

12:00-14:45 - Финал чемпионата мира (_зал &#8220;Аудиториум&#8221;_)

  

14:45-15:45 - Награждение. Закрытие чемпионата мира (_зал &#8220;Аудиториум&#8221;_)