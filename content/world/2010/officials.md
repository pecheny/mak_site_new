+++
title = "Официальные лица"
date = 2012-02-25T20:58:34
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2010/officials/"
slug = "officials"
aliases = [ "/post_20604", "/world/2010/officials",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Оргкомитет**



  * Александр Рубин (председатель)

  * Светлана Александрова

  * Тимур Барский

  * Юлия Воробьёва

  * Игорь Гельфонд

  * Владислав Говердовский

  * Михаил Клейман

  * Юлия Ольштейн

  * Илья Тальянский

  * Александр Толесников

  * Майкл Фрадис

  * Евгений Чёрный



**Редакторы**



  * Наталья Кудряшова

  * Михаил Перлин



**Ведущие**



  * Сергей Абрамов

  * Кирилл Кобелев



**Игровое жюри**



  * Сергей Абрамов

  * Кирилл Кобелев

  * Евгений Поникаров

  * Азизбек Юсуфов



**Апелляционное жюри**



  * Руслан Батдалов

  * Константин Науменко

  * Тимур Сайфуллин