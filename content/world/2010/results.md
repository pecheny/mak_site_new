+++
title = "Результаты"
date = 2012-06-14T03:08:35
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=21050"
slug = "results"
aliases = [ "/post_21050",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Итоговая классификация**



<table>

  <tr>

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

  </tr>

  

  <tr>

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Р. Саид-Аминов

    </td>

    

    <td>

    </td>

    

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Семушин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Косолапова

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Скородумов

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Спешков

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Борок

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Н. Лёгенький

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      П. Малышев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      9

    </td>

    

    <td>

    </td>

    

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      К. Бриф

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нью-Йорк

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

  </tr>

  

  <tr>

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Т. Кочарян

    </td>

    

    <td>

    </td>

    

    <td>

      Ереван

    </td>

    

    <td>

    </td>

    

    <td>

      Армения

    </td>

  </tr>

  

  <tr>

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Савченков

    </td>

    

    <td>

    </td>

    

    <td>

      Могилёв

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Виватенко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      14

    </td>

    

    <td>

    </td>

    

    <td>

      Eclipse

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Н. Новыш

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Элитарного клуба

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Мухин

    </td>

    

    <td>

    </td>

    

    <td>

      сборная

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Эволюция

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Колмаков

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Тель-Авив

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Ратнер

    </td>

    

    <td>

    </td>

    

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Тальянский

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Тель-Авив

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ф. Гусейнов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Приматы

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Пискун

    </td>

    

    <td>

    </td>

    

    <td>

      Днепропетровск

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Понты Пилата

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Гусаков

    </td>

    

    <td>

    </td>

    

    <td>

      Берлин

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Касумов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Троцюк

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      TGDNSKY KOHЬ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Воронов

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Тейтельман

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Пало-Альто

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

  </tr>

  

  <tr>

    <td align="center">

      26

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Оки-Доки

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Мачевский

    </td>

    

    <td>

    </td>

    

    <td>

      Мюнхен

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Д. Арш

    </td>

    

    <td>

    </td>

    

    <td>

      Лондон

    </td>

    

    <td>

    </td>

    

    <td>

      Великобритания

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Rietumu

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Ермак

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Л. Никогосян

    </td>

    

    <td>

    </td>

    

    <td>

      Ереван

    </td>

    

    <td>

    </td>

    

    <td>

      Армения

    </td>

  </tr>

  

  <tr>

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Город утраТ

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Кузнецов

    </td>

    

    <td>

    </td>

    

    <td>

      Тарту

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

  </tr>

  

  <tr>

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Fair Play

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Варрик

    </td>

    

    <td>

    </td>

    

    <td>

      Рига

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Перепелица

    </td>

    

    <td>

    </td>

    

    <td>

      Ашхабад

    </td>

    

    <td>

    </td>

    

    <td>

      Туркмения

    </td>

  </tr>

  

  <tr>

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td>

      777

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Власов

    </td>

    

    <td>

    </td>

    

    <td>

      Хельсинки

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

  </tr>

</table>



&nbsp;



**Предварительный этап, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      6

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      МО

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      62

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      61

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      58

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Eclipse

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Элитарного клуба

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

  </tr>

  

  <tr>

    <td>

      Эволюция

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td>

      Приматы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Понты Пилата

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      TGDNSKY KOHЬ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

  </tr>

  

  <tr>

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Оки-Доки

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

  </tr>

  

  <tr>

    <td>

      Rietumu

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

  </tr>

  

  <tr>

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Город утраТ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Fair Play

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

  

  <tr>

    <td>

      777

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место на предварительном этапе

  

**МО** - место в Открытом турнире 



&nbsp;



**Предварительный этап, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      6

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      64

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      52

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      62

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      61

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      58

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      50

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-13

    </td>

  </tr>

  

  <tr>

    <td>

      Eclipse

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Элитарного клуба

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

  </tr>

  

  <tr>

    <td>

      Эволюция

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      48

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td>

      Приматы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Понты Пилата

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Куча костей

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      TGDNSKY KOHЬ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

  </tr>

  

  <tr>

    <td>

      Тормоза

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      26

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Оки-Доки

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      26

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Greedy Squirrel

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

  </tr>

  

  <tr>

    <td>

      Rietumu

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      37

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

  </tr>

  

  <tr>

    <td>

      ДАФ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      29

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Город утраТ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      29-30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      30-31

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center">

      30

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Fair Play

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      26-27

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      25-26

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-28

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      28-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      31

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      31-32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      32

    </td>

  </tr>

  

  <tr>

    <td>

      777

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      27-29

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      33

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Перестрелка за 10-13 места на предварительном этапе**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

  </tr>

  

  <tr>

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место на предварительном этапе 



&nbsp;



**Финальный этап, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Финальный этап, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Никита Мобайл ТэТэ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      МИД-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Социал-демократы

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      9

    </td>

  </tr>

  

  <tr>

    <td>

      Инк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

  

  <tr>

    <td>

      Перезагрузка

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

  </tr>

  

  <tr>

    <td>

      Джокер

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Перестрелка за 3-6 места**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Бандерлоги-RITLabs

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Привет, Петербург!

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td>

      Понаехали

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Мистраль

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место в чемпионате 



&nbsp;



1. Никита Мобайл ТТ (Ташкент, Узбекистан)



  * Игорь Глущенко

  * Абдулазиз Джалилов

  * Александр Друзь

  * Александр Коробейников

  * Александр Райков

  * Рустамхужа Саид-Аминов (к)

  * Александр Салита



2. ЛКИ (Москва, Россия)



  * Дмитрий Белявский

  * Александра Брутер

  * Вероника Ромашова

  * Максим Руссо

  * Иван Семушин (к)

  * Ольга Хворова



3. Бандерлоги-RITLabs (Запорожье, Украина)



  * Григорий Алхазов

  * Эдуард Голуб

  * Сергей Горбунов

  * Александра Косолапова (к)

  * Максим Коцюруба

  * Григорий Шлайфер