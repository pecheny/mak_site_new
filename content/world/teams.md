+++
title = "Статистика, команды"
date = 2012-02-11T23:24:42
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/participants/"
slug = "teams"
aliases = [ "/post_20403", "/world/participants",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Участие в чемпионатах (2002-2012)**



**Команды** (не менее 5 чемпионатов)



10 - Афина (Москва, Россия)



9 - Команда Губанова (Петродворец/Санкт-Петербург, Россия), Команда Гусейнова (Баку, Азербайджан), Команда Касумова (Баку, Азербайджан), Ксеп (Москва, Россия)



8 - Братья (Тель-Авив, Израиль)



7 - Команда Кузьмина (Москва, Россия), Транссфера (Санкт-Петербург, Россия), ЮМА (Санкт-Петербург, Россия)



6 - Ва-банк (Ашхабад, Туркмения), ЛКИ (Москва, Россия), ОНУ им. Мечникова (Одесса, Украина)



5 - ДАФ (Ереван, Армения), Десятый вал (Хайфа, Израиль), Джокер (Могилёв, Беларусь), Мистер Икс (Даугавпилс, Латвия), Неспроста (Москва, Россия), Суббота 13 (Нью-Йорк, США)



&nbsp;



**Города**



10 - Баку, Москва, Санкт-Петербург



8 - Кишинёв, Петродворец, Ташкент, Тель-Авив



7 - Ашхабад, Висагинас, Ереван, Нью-Йорк, Тбилиси



6 - Даугавпилс, Минск, Одесса, Хайфа



5 - Могилёв, Таллин, Харьков, Хельсинки



4 - Дюссельдорф, Лондон, Рига



3 - Днепропетровск, Запорожье, Пало-Альто, Саранск, Симферополь, Торонто



2 - Алма-Ата, Берлин, Бохум, Гомель, Горловка, Калининград, Кёльн, Сухуми, Тарту



1 - Актау, Дортмунд, Кембридж, Киев, Мюнхен, Надым, Нижний Новгород, Прага, Самара, София, Эспоо



&nbsp;



**Страны**



10 - Азербайджан, Беларусь, Германия, Израиль, Латвия, Россия, Украина



9 - США



8 - Грузия, Молдова, Узбекистан, Эстония



7 - Армения, Литва, Туркмения



6 - Финляндия



5 - Великобритания



4 - Канада



2 - Казахстан



1 - Болгария, Чехия



&nbsp;



**Призовые места (2002-2012)**



**Команды**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td>

      Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td>

      Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      НМТТ

    </td>

    

    <td>

    </td>

    

    <td>

      Саид-Аминов

    </td>

    

    <td>

    </td>

    

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td>

      Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td>

      Касумов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td>

      Белкин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Транссфера

    </td>

    

    <td>

    </td>

    

    <td>

      Друзь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td>

      Севриновский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td>

      Леденёв

    </td>

    

    <td>

    </td>

    

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      ЛКИ

    </td>

    

    <td>

    </td>

    

    <td>

      Семушин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Бандерлоги

    </td>

    

    <td>

    </td>

    

    <td>

      Косолапова

    </td>

    

    <td>

    </td>

    

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

</table>



**1** - количество первых мест

  

**2** - количество вторых мест

  

**3** - количество третьих мест

  

**В** - всего призовых мест 



&nbsp;



**Города**



<table>

  <tr>

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

  </tr>

  

  <tr>

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

  </tr>

  

  <tr>

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Запорожье

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

</table>



**1** - количество первых мест

  

**2** - количество вторых мест

  

**3** - количество третьих мест

  

**В** - всего призовых мест 



&nbsp;



**Страны**



<table>

  <tr>

    <th>

      Страна

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      В

    </th>

  </tr>

  

  <tr>

    <td>

      Россия

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

  </tr>

  

  <tr>

    <td>

      Узбекистан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Азербайджан

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Израиль

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Украина

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

  </tr>

</table>



**1** - количество первых мест

  

**2** - количество вторых мест

  

**3** - количество третьих мест

  

**В** - всего призовых мест