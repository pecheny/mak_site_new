+++
title = "Результаты"
date = 2012-05-07T23:04:32
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=20889"
slug = "results"
aliases = [ "/post_20889",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Итоговая классификация**



<table>

  <tr>

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

  </tr>

  

  <tr>

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td>

      Азерселл

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Касумов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      О. Леденёв

    </td>

    

    <td>

    </td>

    

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Севриновский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Сборная Азербайджана

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ф. Гусейнов

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Белкин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Е. Канищева

    </td>

    

    <td>

    </td>

    

    <td>

      Симферополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      9-11

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Крейзер Аврора

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Л. Кандыба

    </td>

    

    <td>

    </td>

    

    <td>

      Бохум

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Шпунгин

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нью-Йорк

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      П. Забавский

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Шихов

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Галахад

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Л. Киладзе

    </td>

    

    <td>

    </td>

    

    <td>

      Тбилиси

    </td>

    

    <td>

    </td>

    

    <td>

      Грузия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Друзь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Кулиев

    </td>

    

    <td>

    </td>

    

    <td>

      Ашхабад

    </td>

    

    <td>

    </td>

    

    <td>

      Туркмения

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td>

      Excalibur

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Спатарь

    </td>

    

    <td>

    </td>

    

    <td>

      София

    </td>

    

    <td>

    </td>

    

    <td>

      Болгария

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      БФ-6

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Е. Арташов

    </td>

    

    <td>

    </td>

    

    <td>

      Тарту

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td>

      ROMA

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Р. Алиев

    </td>

    

    <td>

    </td>

    

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      О. Кирничанская

    </td>

    

    <td>

    </td>

    

    <td>

      Даугавпилс

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Афазия Вернике

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Н. Новицкий

    </td>

    

    <td>

    </td>

    

    <td>

      Хельсинки

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td>

      Shvyturys

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Прохоров

    </td>

    

    <td>

    </td>

    

    <td>

      Висагинас

    </td>

    

    <td>

    </td>

    

    <td>

      Литва

    </td>

  </tr>

</table>



&nbsp;



**Отбор, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Азерселл

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Азербайджана

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Крейзер Аврора

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

  </tr>

  

  <tr>

    <td>

      Галахад

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Shvyturys

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td>

      ROMA

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Excalibur

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Афазия Вернике

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      БФ-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Отбор, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Азерселл

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Азербайджана

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      30

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Крейзер Аврора

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-10

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-12

    </td>

  </tr>

  

  <tr>

    <td>

      Галахад

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

  </tr>

  

  <tr>

    <td>

      Shvyturys

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td>

      ROMA

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td>

      Excalibur

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Афазия Вернике

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      БФ-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Финал**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Азерселл

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Сборная Азербайджана

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

</table>



**1**, **2** - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Утешительный турнир**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Неспроста

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Крейзер Аврора

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

  </tr>

  

  <tr>

    <td>

      Умник

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

  </tr>

  

  <tr>

    <td>

      Галахад

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Excalibur

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      БФ-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-17

    </td>

  </tr>

  

  <tr>

    <td>

      ROMA

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      16-18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Афазия Вернике

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

  </tr>

  

  <tr>

    <td>

      Shvyturys

    </td>

    

    <td>

    </td>

    

    <td align="right">

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

</table>



**1**, **2** - очки за тур

  

**МТ** - место в туре (начиная с 7-го)

  

**О** - сумма очков

  

**М** - место в чемпионате 



&nbsp;



**Суперфинал**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      6

    </th>

    

    <td>

    </td>

    

    <th>

      7

    </th>

    

    <td>

    </td>

    

    <th>

      8

    </th>

    

    <td>

    </td>

    

    <th>

      9

    </th>

    

    <td>

    </td>

    

    <th>

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Азерселл

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место 



&nbsp;



1. Азерселл (Баку, Азербайджан)



  * Дмитрий Авдеенко

  * Ровшан Аскеров

  * Аднан Ахундов

  * Анар Гулиев

  * Балаш Касумов (к)

  * Борис Левин

  * Рауф Наджафли



2. Десятый вал (Хайфа, Израиль)



  * Сусанна Бровер

  * Александр Левитас

  * Олег Леденёв (к)

  * Станислав Малышев

  * Леонид Медников

  * Яков Подольный



3. Ксеп (Москва, Россия)



  * Игорь Бахарев

  * Юрий Выменец

  * Роман Немучинский

  * Илья Новиков

  * Владимир Севриновский (к)

  * Антон Снятковский