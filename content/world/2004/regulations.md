+++
title = "Положение"
date = 2012-02-19T15:55:46
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2004/regulations/"
slug = "regulations"
aliases = [ "/post_20526", "/world/2004/regulations",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Положение о проведении третьего чемпионата мира по игре &#8220;Что? Где? Когда?&#8221; сезона 2003/04**



1. Третий чемпионат мира по игре &#8220;Что? Где? Когда?&#8221; (в дальнейшем - Чемпионат) проводится совместно [Международной Ассоциацией Клубов (МАК) &#8220;Что? Где? Когда?&#8221;](http://register.chgk.info/?cid=3), Национальным Олимпийским комитетом Республики Азербайджан, Министерством по делам молодёжи, спорта и туризма Республики Азербайджан.



2. Для проведения Чемпионата создается Дирекция в составе: Е. Алексеев, Р. Аскеров, Ю. Галуева, Ф. Гусейнов, Б. Левин, М. Поташев (председатель), а также Оргкомитет в составе представителей Национального Олимпийского комитета и Министерства по делам молодёжи, спорта и туризма Республики Азербайджан.



3. Время и место проведения Чемпионата: Баку, 27-31 июля 2004 г.



4. К участию в Чемпионате допускаются следующие команды.



I. Чемпионы стран, официальные чемпионаты которых состоялись в течение последнего года.



II. Победители каждого из этапов Кубка мира сезона 2003/04.



III. Призёры Кубка мира сезона 2003/04 в общем зачёте.



IV. Команда, приглашённая Правлением МАК.



5. В составе команды, допущенной к участию в Чемпионате, должно быть не менее 4 человек из состава команды в турнире, по результатам которого она получила право участия в Чемпионате.



6. Дирекция имеет право не допустить к участию в Чемпионате команды, не предоставившие заявочный список до 08.07.2004.



7. Дирекция и Оргкомитет обязаны:



  * соблюдать настоящее Положение и регламент соревнований;

  * не позднее чем за 20 дней до начала Чемпионата официально сообщить командам, имеющим право участвовать в Чемпионате, время начала и окончания игр, условия приёма команд, состав Редколлегии и Апелляционного Жюри, а также, по просьбе команд, выслать им официальные приглашения;

  * обеспечить проживание в гостиницах, а также трёхразовое питание участников, включённых в заявочные списки команд;

  * организовать работу редакционной коллегии, игрового и апелляционного жюри;

  * обеспечить команды игровыми местами и карточками для ответов;

  * обеспечить награждение победителей и призёров турнира;

  * в течение месяца предоставить итоговые результаты и вопросы турнира командам-участницам.



8. Команды, которые получили право участвовать в Чемпионате:



  * должны соблюдать настоящее Положение и регламент Чемпионата;

  * должны играть только в единой для команды форме, соответствующей уровню соревнования;