+++
title = "Чемпионат мира 2003 года"
date = 2012-02-13T13:19:18
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2003/"
slug = "chempionat-mira-2003-goda"
aliases = [ "/post_20427", "/world/2003",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

2-й чемпионат мира, Баку, 13&#8212;18 августа 2003 года



[Положение](http://mak-chgk.ru/world/2003/regulations/)

  

[Регламент](http://mak-chgk.ru/world/2003/reglament/)

  

[Расписание](http://mak-chgk.ru/world/2003/schedule/)

  

[Официальные лица](http://mak-chgk.ru/world/2003/officials/)

  

[Команды](http://mak-chgk.ru/world/2003/teams/)

  

[Результаты](http://mak-chgk.ru/world/2003/results/) 



[Страница чемпионата в Летописи](http://letopis.chgk.info/200308Baku.html)

  

[Страница чемпионата в рейтинге](http://ratingnew.chgk.info/tournaments.php?displaytournament=8)

  

[Вопросы чемпионата в Базе вопросов](http://db.chgk.info/tour/wc03)