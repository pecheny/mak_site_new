+++
title = "Результаты"
date = 2012-05-08T21:28:15
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=20894"
slug = "results"
aliases = [ "/post_20894",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Итоговая классификация**



<table>

  <tr>

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      Капитан

    </th>

    

    <td>

    </td>

    

    <th>

      Город

    </th>

    

    <td>

    </td>

    

    <th>

      Страна

    </th>

  </tr>

  

  <tr>

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Кузьмин

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Поташев

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      О. Леденёв

    </td>

    

    <td>

    </td>

    

    <td>

      Хайфа

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Севриновский

    </td>

    

    <td>

    </td>

    

    <td>

      Москва

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      5

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Губанов

    </td>

    

    <td>

    </td>

    

    <td>

      Петродворец

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Тальянский

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Тель-Авив

    </td>

    

    <td>

    </td>

    

    <td>

      Израиль

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Богословского

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Богословский

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      И. Шпунгин

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Нью-Йорк

    </td>

    

    <td>

    </td>

    

    <td>

      США

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      9-10

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Е. Канищева

    </td>

    

    <td>

    </td>

    

    <td>

      Симферополь

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Виватенко

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center" nowrap>

      11-14

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ф. Гусейнов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Касумов

    </td>

    

    <td>

    </td>

    

    <td>

      Баку

    </td>

    

    <td>

    </td>

    

    <td>

      Азербайджан

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Данько

    </td>

    

    <td>

    </td>

    

    <td>

      Харьков

    </td>

    

    <td>

    </td>

    

    <td>

      Украина

    </td>

  </tr>

  

  <tr>

    <td>

    </td>

    

    <td>

    </td>

    

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      С. Шишко

    </td>

    

    <td>

    </td>

    

    <td>

      Минск

    </td>

    

    <td>

    </td>

    

    <td>

      Беларусь

    </td>

  </tr>

  

  <tr>

    <td align="center">

      15

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      РПА-DAF

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Тваскис

    </td>

    

    <td>

    </td>

    

    <td>

      Ереван

    </td>

    

    <td>

    </td>

    

    <td>

      Армения

    </td>

    

    <td>

    </td>

  </tr>

  

  <tr>

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Друзь

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Санкт-Петербург

    </td>

    

    <td>

    </td>

    

    <td>

      Россия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Перлин

    </td>

    

    <td>

    </td>

    

    <td>

      Кёльн

    </td>

    

    <td>

    </td>

    

    <td>

      Германия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Брутер

    </td>

    

    <td>

    </td>

    

    <td>

      Кишинёв

    </td>

    

    <td>

    </td>

    

    <td>

      Молдова

    </td>

  </tr>

  

  <tr>

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Шиман

    </td>

    

    <td>

    </td>

    

    <td>

      Даугавпилс

    </td>

    

    <td>

    </td>

    

    <td>

      Латвия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Б. Кулиев

    </td>

    

    <td>

    </td>

    

    <td>

      Ашхабад

    </td>

    

    <td>

    </td>

    

    <td>

      Туркмения

    </td>

  </tr>

  

  <tr>

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      А. Таубер

    </td>

    

    <td>

    </td>

    

    <td>

      Эспоо

    </td>

    

    <td>

    </td>

    

    <td>

      Финляндия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td>

      Инкогнито

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Т. Данелия

    </td>

    

    <td>

    </td>

    

    <td>

      Тбилиси

    </td>

    

    <td>

    </td>

    

    <td>

      Грузия

    </td>

  </tr>

  

  <tr>

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В чёрных ботинках

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Ю. Мухаева

    </td>

    

    <td>

    </td>

    

    <td>

      Висагинас

    </td>

    

    <td>

    </td>

    

    <td>

      Литва

    </td>

  </tr>

  

  <tr>

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Брюссельские мальчики

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      В. Клименко

    </td>

    

    <td>

    </td>

    

    <td>

      Ташкент

    </td>

    

    <td>

    </td>

    

    <td>

      Узбекистан

    </td>

  </tr>

  

  <tr>

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      Dolche vita

    </td>

    

    <td>

    </td>

    

    <td nowrap>

      М. Тарасенко

    </td>

    

    <td>

    </td>

    

    <td>

      Таллин

    </td>

    

    <td>

    </td>

    

    <td>

      Эстония

    </td>

  </tr>

</table>



&nbsp;



**Отбор, результаты**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      МТ

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Богословского

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      РПА-DAF

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Инкогнито

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      19-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      В чёрных ботинках

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      2

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Брюссельские мальчики

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Dolche vita

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      20-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      22-23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

</table>



**1** и т. д. - очки за тур

  

**МТ** - место в туре

  

**О** - сумма очков

  

**М** - место 



&nbsp;



**Отбор, движение по турам**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      57

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Golden Telecom

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      56

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

  </tr>

  

  <tr>

    <td>

      Ксеп

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      23

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      47

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      1-2

    </td>

    

    <td>

    </td>

    

    <td align="right">

      55

    </td>

    

    <td>

    </td>

    

    <td align="center">

      4

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Губанова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      43

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5

    </td>

    

    <td>

    </td>

    

    <td align="right">

      54

    </td>

    

    <td>

    </td>

    

    <td align="center">

      5

    </td>

  </tr>

  

  <tr>

    <td>

      Братья

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      2-3

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      34

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      3-4

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      53

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Богословского

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      4-6

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      42

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      6-7

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Суббота 13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      51

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Канищевой

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      20

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      10-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      40

    </td>

    

    <td>

    </td>

    

    <td align="center">

      10

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

  </tr>

  

  <tr>

    <td>

      ЮМА

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-8

    </td>

    

    <td>

    </td>

    

    <td align="right">

      41

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      8-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      49

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-10

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Гусейнова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      38

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Касумова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      12-13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      9-11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      11

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      От Винта - Братья по фазе

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td>

      Хунта

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      7-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      5-9

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      12

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      13

    </td>

    

    <td>

    </td>

    

    <td align="right">

      46

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-14

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      РПА-DAF

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      35

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      14-15

    </td>

    

    <td>

    </td>

    

    <td align="right">

      45

    </td>

    

    <td>

    </td>

    

    <td align="center">

      15

    </td>

  </tr>

  

  <tr>

    <td>

      Троярд

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      13-14

    </td>

    

    <td>

    </td>

    

    <td align="right">

      33

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      44

    </td>

    

    <td>

    </td>

    

    <td align="center">

      16

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Филателисты им. Грибоедова

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      15-17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      31

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

    

    <td>

    </td>

    

    <td align="right">

      39

    </td>

    

    <td>

    </td>

    

    <td align="center">

      17

    </td>

  </tr>

  

  <tr>

    <td>

      NB

    </td>

    

    <td>

    </td>

    

    <td align="right">

      7

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      11-16

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      21

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      28

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

    

    <td>

    </td>

    

    <td align="right">

      36

    </td>

    

    <td>

    </td>

    

    <td align="center">

      18

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Мистер Икс

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-35

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      25

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      32

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Ва-банк

    </td>

    

    <td>

    </td>

    

    <td align="right">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      17-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      18-19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      27

    </td>

    

    <td>

    </td>

    

    <td align="center">

      19

    </td>

    

    <td>

    </td>

    

    <td align="right">

      29

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Столичные лобстеры

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      14

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

    

    <td>

    </td>

    

    <td align="right">

      24

    </td>

    

    <td>

    </td>

    

    <td align="center">

      21

    </td>

  </tr>

  

  <tr>

    <td>

      Инкогнито

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      22

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      В чёрных ботинках

    </td>

    

    <td>

    </td>

    

    <td align="right">

      5

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center">

      20

    </td>

    

    <td>

    </td>

    

    <td align="right">

      12

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      17

    </td>

    

    <td>

    </td>

    

    <td align="center">

      22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      19

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Брюссельские мальчики

    </td>

    

    <td>

    </td>

    

    <td align="right">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      23-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      11

    </td>

    

    <td>

    </td>

    

    <td align="center">

      23

    </td>

    

    <td>

    </td>

    

    <td align="right">

      15

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

    

    <td>

    </td>

    

    <td align="right">

      18

    </td>

    

    <td>

    </td>

    

    <td align="center">

      24

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Dolche vita

    </td>

    

    <td>

    </td>

    

    <td align="right">

      4

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      9

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      21-22

    </td>

    

    <td>

    </td>

    

    <td align="right">

      10

    </td>

    

    <td>

    </td>

    

    <td align="center" nowrap>

      24-25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      13

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

    

    <td>

    </td>

    

    <td align="right">

      16

    </td>

    

    <td>

    </td>

    

    <td align="center">

      25

    </td>

  </tr>

</table>



**1** и т. д. - сумма очков после тура

  

**М** - место после тура

  





&nbsp;



**Бой за 2 место в отборе**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

  

  <tr>

    <td nowrap>

      Десятый вал

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

    

    <td>

    </td>

    

    <td align="center">

      3

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место в отборе 



&nbsp;



**Суперфинал**



<table>

  <tr>

    <th>

      Команда

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      6

    </th>

    

    <td>

    </td>

    

    <th>

      7

    </th>

    

    <td>

    </td>

    

    <th>

      8

    </th>

    

    <td>

    </td>

    

    <th>

      9

    </th>

    

    <td>

    </td>

    

    <th>

    </th>

    

    <td>

    </td>

    

    <th>

      1

    </th>

    

    <td>

    </td>

    

    <th>

      2

    </th>

    

    <td>

    </td>

    

    <th>

      3

    </th>

    

    <td>

    </td>

    

    <th>

      4

    </th>

    

    <td>

    </td>

    

    <th>

      5

    </th>

    

    <td>

    </td>

    

    <th>

      О

    </th>

    

    <td>

    </td>

    

    <th>

      М

    </th>

  </tr>

  

  <tr>

    <td nowrap>

      Команда Кузьмина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      8

    </td>

    

    <td>

    </td>

    

    <td align="center">

      1

    </td>

  </tr>

  

  <tr>

    <td>

      Афина

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      +

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      -

    </td>

    

    <td>

    </td>

    

    <td align="center">

      6

    </td>

    

    <td>

    </td>

    

    <td align="center">

      2

    </td>

  </tr>

</table>



**1** и т. д. - номер вопроса

  

**О** - сумма очков

  

**М** - место 



&nbsp;



1. Golden Telecom (Москва, Россия)



  * Лариса Архипова

  * Дмитрий Васильев

  * Павел Володин

  * Андрей Кузьмин (к)

  * Михаил Левандовский

  * Александр Либер

  * Пётр Сухачёв



2. Афина (Москва, Россия)



  * Вадим Карлинский

  * Дмитрий Коноваленко

  * Максим Поташев (к)

  * Максим Руссо

  * Владимир Степанов

  * Антон Чернин



3. Десятый вал (Хайфа, Израиль)



  * Сусанна Бровер

  * Сергей Волчёнков

  * Натали Красногор

  * Александр Левитас

  * Олег Леденёв (к)

  * Леонид Медников

  * Яков Подольный