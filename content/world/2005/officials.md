+++
title = "Официальные лица"
date = 2012-02-25T02:31:48
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2005/officials/"
slug = "officials"
aliases = [ "/post_20596", "/world/2005/officials",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Оргкомитет**



  * Максим Поташев (председатель)

  * Андрей Кузьмин

  * Михаил Левандовский

  * Александр Рубин

  * Пётр Сухачёв



**Редакторы**



  * Андрей Абрамов

  * Дмитрий Борок

  * Константин Кноп



**Ведущий**



  * Дмитрий Соловьёв



**Игровое жюри**



  * Евгений Поникаров

  * Кирилл Теймуразов

  * Леонид Черненко



**Апелляционное жюри**



  * Владимир Белкин

  * Борис Бурда

  * Андрей Козлов