+++
title = "Команды"
date = 2012-02-26T16:18:49
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2006/teams/"
slug = "teams"
aliases = [ "/post_20612", "/world/2006/teams",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

Golden Telecom, Москва (чемпион мира)

  

Команда Гусейнова, Баку (чемпион Азербайджана)

  

DAF, Ереван (чемпион Армении)

  

Против ветра, Дюссельдорф (чемпион Германии)

  

Пентиум, Сухуми (чемпион Грузии)

  

Десятый вал, Хайфа (чемпион Израиля)

  

Вестимо, Торонто (2-й призёр чемпионата Канады)

  

ГДР, Даугавпилс (чемпион Латвии)

  

Спящие головастики, Висагинас (чемпион Литвы)

  

NB, Кишинёв (чемпион Молдовы)

  

Неспроста, Москва (чемпион России)

  

Суббота 13, Нью-Йорк (чемпион США)

  

Ва-банк, Ашхабад (чемпион Туркмении)

  

7Hz, Ташкент (чемпион Узбекистана)

  

ОНУ им. Мечникова, Одесса (чемпион Украины)

  

Мы-6, Хельсинки (чемпион Финляндии)

  

Команда, Таллин (чемпион Эстонии)

  

Социал-демократы, Москва (победитель этапа Кубка мира)

  

Троярд, Санкт-Петербург (по приглашению Правления МАК)

  

Команда Касумова, Баку (по приглашению Правления МАК)

  

Ультиматум, Гомель (по приглашению Правления МАК)

  

ЮМА, Санкт-Петербург (по приглашению Правления МАК)

  

Утекай, Калининград (по приглашению Правления МАК)

  

Афина, Москва (топ-10 рейтинга МАК) *

  

Ксеп, Москва (топ-10 рейтинга МАК)

  

Команда Губанова, Петродворец (топ-10 рейтинга МАК)

  

Братья, Тель-Авив (топ-10 рейтинга МАК)

  

Гросс-бух, Москва (топ-10 рейтинга МАК, за свой счёт)

  

ЛКИ, Москва (топ-10 рейтинга МАК, за свой счёт)



* Команда была приглашена как победитель этапа Кубка мира &#8220;Белые ночи&#8221;, однако после проверки составов была исключена из таблицы этапа ввиду нарушения правил участия игроков в Кубке мира. Фактический победитель этапа - команда &#8220;Умник&#8221; (Минск) не получила приглашения на чемпионат мира.