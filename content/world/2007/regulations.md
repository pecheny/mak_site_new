+++
title = "Правила участия"
date = 2012-02-21T01:14:53
author = "Антон Губанов"
guid = "http://mak-chgk.ru/world/2007/regulations/"
slug = "regulations"
aliases = [ "/post_20540", "/world/2007/regulations",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

К участию в чемпионате (с оплатой расходов Оргкомитетом) допускались:



- чемпион мира;

  

- чемпионы стран (в случае отказа чемпиона приглашался вице-чемпион);

  

- победители этапов Кубка мира сезона 2006/07;

  

- команды по приглашению Правления МАК;

  

- команды, занимавшие верхние места в рейтинге МАК.



Команды, входящие в топ-10 рейтинга МАК, но не попавшие в список участников, могли принять участие в чемпионате без оплаты проезда и проживания Оргкомитетом.