+++
title = "Дополнение к Положению о чемпионатах мира"
date = 2013-06-27T01:59:33
author = "Антон Губанов"
guid = "http://mak-chgk.ru/?page_id=21621"
slug = "regulations"
aliases = [ "/post_21621",]
type = "post"
categories = [ "Чемпионат мира",]
tags = []
+++

**Дополнение к Положению о чемпионатах мира по спортивному &#8220;Что? Где? Когда?&#8221;. XI чемпионат мира**



1. XI чемпионат мира по спортивному &#8220;Что? Где? Когда?&#8221; (_далее - чемпионат_) проходит с 14 по 15 сентября 2013 года в г. Дубна, Россия.



2. К участию в чемпионате допускаются (в порядке приоритета):



2.1. команды, имеющие право участия в чемпионате согласно [Положению о чемпионатах мира](http://mak-chgk.ru/world/2013/regulations13);

  

2.2 победитель &#8220;малого финала&#8221; X чемпионата мира;

  

2.3. команды, занимающие 4-10 места в рейтинге МАК по состоянию на 1 июня 2013 года;

  

2.4. команды по приглашению Оргкомитета чемпионата (не более 2 команд);

  

2.5. команды, не соответствующие пунктам 2.1-2.4, при уплате ими взноса в размере, установленном Оргкомитетом чемпионата (не более 6 команд);

  

2.6. вторые команды от стран, представленных по пунктам 2.1-2.5 не более чем одной командой. Приглашение представителям стран по пункту 2.6 осуществляется в порядке мест, занятых лучшей командой страны на [X чемпионате мира](http://mak-chgk.ru/world/2012/results/). В случае если на участие в чемпионате по пункту 2.6 претендует несколько команд одной страны, предпочтение отдаётся команде, занявшей более высокое место в чемпионате страны.

  

2.7. прочие команды по приглашению Оргкомитета чемпионата.



Допуск по пунктам 2.2 - 2.7 осуществляется с тем условием, чтобы общее число участников не превышало 35 команд. Максимальное количество команд-участниц может быть увеличено по решению Оргкомитета чемпионата.



3. Для команд, допускаемых в соответствии с пунктами 2.1-2.3, действуют правила преемственности пункта 6 [Положения о чемпионатах мира](http://mak-chgk.ru/world/2013/regulations13). Те же правила устанавливаются для команды, допущенной в соответствии с пунктом 2.6 по более высокому месту, занятому на чемпионате страны. В остальных случаях ограничения на состав команды отсутствуют.