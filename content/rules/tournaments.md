+++
title = "Положение о турнирах"
date = 2011-04-02T14:58:05
author = "Антон Губанов"
guid = "http://mak-chgk.ru/rules/tournaments/"
slug = "tournaments"
aliases = [ "/post_16338", "/rules/tournaments",]
type = "post"
categories = [ "Правила игры",]
tags = []
+++

(_разработано подкомиссией по правилам турнирной комиссии МАК_)



## 1. Статус документа 



1.1. Настоящее Положение используется как дополнение к [второму изданию Кодекса спортивного ЧГК](http://mak-chgk.ru/rules/codex/) (2008). Положение вступает в силу после утверждения Правлением МАК и действует вплоть до своей отмены или до выпуска следующего издания Кодекса спортивного ЧГК.



## 2. Типы турниров



2.1. Данный раздел описывает типы турниров по спортивному ЧГК, существующие в настоящее время.



2.2. _Очным_ называется турнир, в котором каждый вопрос читается одновременно для всех команд и только одним ведущим, а также выполняется одно из следующих условий:



  * в любой момент игры все команды находятся в одном помещении;



  * в любой момент игры все команды находятся в смежных или близко расположенных помещениях одного здания, причём для технического обслуживания турнира (обработки ответов, выдачи раздаточных материалов секундантам и т. п.) используется только одно из помещений.



2.3. _Синхронным_ называется турнир, в котором одни и те же вопросы читают два и более ведущих (каждый - для отдельной группы команд на отдельной игровой площадке), либо один ведущий читает вопросы по очереди разным группам команд. В синхронных турнирах разрешены апелляции на ошибку ведущего (в соответствии с главой 3.5 Кодекса спортивного ЧГК).



2.4. _Заочным_ называется турнир, не являющийся ни очным, ни синхронным, в котором каждый вопрос читается только одним ведущим.



2.5. _Строго синхронный_ турнир - разновидность синхронного турнира, в котором одни и те же вопросы отыгрываются разными командами в одно и то же время с точностью до нескольких минут.



2.6. _Асинхронный_ турнир - разновидность синхронного турнира, в котором одни и те же вопросы отыгрываются разными командами в разные дни, причём интервал между проведением игр на разных площадках, отсчитываемый по московскому времени, составляет 4 суток и более.



2.7. _Марафоном_, вне зависимости от прочих обстоятельств, называется турнир, в котором количество вопросов, отыгрываемых в любой из игровых дней, превышает 90 (не считая вопросов &#8220;перестрелки&#8221;).



## 3. Количество вопросов



3.1. Турнир по спортивному ЧГК должен состоять не менее чем из 12 вопросов (не считая вопросов &#8220;перестрелки&#8221;).



## 4. Использование дополнительных показателей



4.1. Правила данного раздела относятся к турнирам, в которых основным показателем для распределения мест является количество взятых вопросов.



4.2. В турнирах, удовлетворяющих условию из пункта 4.1, рекомендуется не использовать дополнительные показатели для распределения мест, за исключением следующих случаев:



  * определение чемпиона;



  * распределение призовых мест;



  * отделение команд, прошедших отбор по итогам данного турнира или его части, от команд, не прошедших отбор.



Во всех остальных случаях при равном количестве взятых вопросов рекомендуется считать соответствующие места разделёнными на равных.



4.3. При необходимости распределения мест в турнирах, удовлетворяющих условию из пункта 4.1, рекомендуется использовать &#8220;перестрелку&#8221;.



## Приложение. Используемые сокращения.



МАК - Международная ассоциация клубов &#8220;Что, где, когда&#8221;.



ЧГК - &#8220;Что? Где? Когда?&#8221;.