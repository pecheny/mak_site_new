+++
title = "Правила игры"
date = 2011-03-24T03:50:57
author = "Антон Губанов"
guid = "http://mak-chgk.ru/rules/"
slug = "rules"
aliases = [ "/post_16188", "/rules",]
type = "post"
categories = [ "Правила игры",]
tags = []
+++

**Действующие правила**



[Кодекс спортивного ЧГК (2018)](http://mak-chgk.ru/rules/codex/)



[Положение о турнирах](http://mak-chgk.ru/rules/tournaments)



[Трактовка правил](http://mak-chgk.ru/rules/interpretation)



&nbsp;



**Архив**



[Кодекс спортивного ЧГК (2008)](http://mak-chgk.ru/rules/codex08)



[Кодекс спортивного ЧГК (2003)](http://mak-chgk.ru/rules/codex03)