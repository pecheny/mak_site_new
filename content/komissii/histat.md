+++
title = "Комиссия МАК по истории, статистике и награждениям"
date = 2010-06-14T11:05:52
author = "Евгений Коваль"
guid = "http://mak-chgk.ru/komissii/komissiya-mak-po-istorii-statistike-i-nagrazhdeniyam/"
slug = "histat"
aliases = [ "/post_12679", "/komissii/komissiya-mak-po-istorii-statistike-i-nagrazhdeniyam",]
type = "post"
categories = [ "Правление и комиссии",]
tags = []
+++

Комиссия создана в 2005 году.



**Состав комиссии (с 2014 года):** Константин Алдохин



**Председатель комиссии:**



  1. 2005-2009 гг. - Дмитрий Соловьев (Саранск)

  2. 2009-2014 гг. - Дмитрий Башук (Харьков)

  3. 2014 г. - н.в. Константин Алдохин (Великие Луки)



**Состав комиссии (на 1.06.2010):** Д. Соловьев, Л. Черненко, А. Друзь, Е. Алексеев, И. Тальянский, И. Бер, Е. Коваль 



* * *



[Положение о комиссии (2010)](http://mak-chgk.ru/komissii/histat/polozhenie2010/)   

[Положение о комиссии (2005)](http://mak-chgk.ru/komissii/histat/polozhenie2005/)