+++
title = "Дисциплинарная комиссия"
date = 2010-04-13T21:27:50
author = "ber"
guid = "http://mak-chgk.ru/komissii/discipl/"
slug = "discipl"
aliases = [ "/post_11998", "/komissii/discipl",]
type = "post"
categories = [ "Правление и комиссии",]
tags = []
+++

Создана по решению Конгресса МАК в 2009 г.



**Состав комиссии с 2014 г.**: И.Бер (председатель), С.Абрамов, Г.Арабули, П.Забавский, К.Кноп, Д.Марков, Д.Родионов.



Адрес для обращений: **mak-dk@googlegroups.com** 



### Положение о комиссии и принятые решения &nbsp;



[Положение о дисциплинарной комиссии (2010)](http://mak-chgk.ru/komissii/discipl/polozhenie2010/)



[Положение о дисциплинарной комиссии (2015)](http://mak-chgk.ru/komissii/discipl/polozhenie2015/)



[Рекомендации по информационной безопасности](http://mak-chgk.ru/komissii/discipl/infobez/)



[Решение дисциплинарной комиссии от 5.06.2011](http://mak-chgk.ru/komissii/discipl/disc20110605/)



[Решение дисциплинарной комиссии (в редакции Правления МАК) от 8.04.2012](http://mak-chgk.ru/komissii/discipl/reshenie-disciplinarnoj-komissii-ot-8-04-2012/)



[Решение Дисциплинарной комиссии МАК от 31.03.2013](http://mak-chgk.ru/komissii/discipl/reshenie-disciplinarnoj-komissii-mak-ot-31-03-2013/)



[Решения Дисциплинарной комиссии МАК от 9.06.2013](http://mak-chgk.ru/komissii/discipl/resheniya-disciplinarnoj-komissii-mak-ot-9-06-2013/)



[Решение-1 Дисциплинарной комиссии МАК от 02.09.2015](http://mak-chgk.ru/komissii/discipl/disc20150902-1/)



[Решение-2 Дисциплинарной комиссии МАК от 02.09.2015](http://mak-chgk.ru/komissii/discipl/disc20150902-2/)



[Решение-3 Дисциплинарной комиссии МАК от 02.09.2015](http://mak-chgk.ru/komissii/discipl/disc20150902-3/)



[Решение Дисциплинарной комиссии МАК от 30.09.2015](http://mak-chgk.ru/komissii/discipl/disc20150930/)



[Решение Дисциплинарной комиссии МАК в редакции, утверждённой Правлением МАК от 20.12.2015](http://mak-chgk.ru/komissii/discipl/disc20151220/)



[Решение дисциплинарной комиссии МАК от 18.08.2016](http://mak-chgk.ru/komissii/discipl/disc20160818/)



[Решение дисциплинарной комиссии МАК от 23.10.2016](http://mak-chgk.ru/komissii/discipl/disc20161023/)



[Решение дисциплинарной комиссии МАК от 21.05.2017](http://mak-chgk.ru/komissii/discipl/disc20170521/)



[Решение дисциплинарной комиссии МАК от 29.05.2017](http://mak-chgk.ru/komissii/discipl/disc20170529/)



[Решение-1 Дисциплинарной комиссии МАК от 22.10.2017](http://mak-chgk.ru/komissii/discipl/disc20171022-1/)



[Решение-2 Дисциплинарной комиссии МАК от 22.10.2017](http://mak-chgk.ru/komissii/discipl/disc20171022-2/)



[Решение-3 Дисциплинарной комиссии МАК от 22.10.2017](http://mak-chgk.ru/komissii/discipl/disc20171022-3/)



[Решение Дисциплинарной комиссии МАК от 13.11.2017](http://mak-chgk.ru/komissii/discipl/disc20171113/)



[Решение Дисциплинарной комиссии МАК от 25.01.2018](http://mak-chgk.ru/komissii/discipl/disc20180125/)



[Решение Дисциплинарной комиссии МАК от 21.03.2018](http://mak-chgk.ru/komissii/discipl/disc20180321/)



[Решение Дисциплинарной комиссии МАК от 26.05.2018](http://mak-chgk.ru/komissii/discipl/disc20180526/)