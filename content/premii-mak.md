+++
title = "Премии МАК"
date = 2007-07-22T01:05:24
author = "Евгений Коваль"
guid = "http://mak-chgk.ru/premii-mak/"
slug = "premii-mak"
aliases = [ "/post_817", "/premii-mak",]
type = "post"
categories = [ "Премии МАК",]
tags = []
+++

Присуждаются ежегодно (с 2003 года) в следующих номинациях



  1. Турнир года

  2. Клуб года

  3. Человек года

  4. Вопрос года

  5. Приз за вклад в развитие движения &#8220;Что? Где? Когда?&#8221; им. В.Я.Ворошилова

  6. (c 2008) Тренер года

  7. (c 2009) Детский (юношеский) турнир года



<a href="https://docs.google.com/spreadsheet/ccc?key=0AoH63Yvgr3jxdGloLVhxWDI4c0I3RG1YNjk5MERXWVE" target="_blank">Таблица победителей 2003-2012 годов</a>