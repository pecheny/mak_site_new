+++
title = "Думать &#8212; это интересно!"
date = 2011-02-14T11:13:16
guid = "http://mak-chgk.ru/metodichka/dumat-eto-interesno/"
slug = "dumat-eto-interesno"
aliases = [ "/post_15675", "/metodichka/dumat-eto-interesno",]
type = "post"
categories = [ "Методичка",]
tags = []
+++

<h2 style="text-align: center" align="center">

  ДЕПАРТАМЕНТ<span>  </span>ОБРАЗОВАНИЯ<span>  </span>ГОРОДА<span>  </span>МОСКВЫ

</h2>



<h2 style="text-align: center" align="center">

  СЕВЕРО-ЗАПАДНОЕ ОКРУЖНОЕ УПРАВЛЕНИЕ ОБРАЗОВАНИЯ

</h2>



<p class="MsoNormal" style="text-align: center" align="center">

  <span style="font-size: 14pt"> </span>

</p>



<h2 style="text-align: center" align="center">

  <span style="font-size: 14pt">ГОСУДАРСТВЕННОЕ ОБРАЗОВАТЕЛЬНОЕ УЧРЕЖДЕНИЕ ГОРОДА МОСКВЫ</span>

</h2>



<h2 style="text-align: center" align="center">

  <span style="font-size: 14pt">ДВОРЕЦ ТВОРЧЕСТВА ДЕТЕЙ И МОЛОДЕЖИ</span>

</h2>



<p align="right">

  <table class="MsoTableGrid" style="margin-left: -23.2pt; border-collapse: collapse" border="0" cellpadding="0" cellspacing="0">

    <tr>

      <td style="width: 320.15pt; padding: 0cm 5.4pt" valign="top" width="427">

        <p class="MsoNormal" style="text-align: center; text-indent: 1cm; background: none repeat scroll 0% 0% white" align="center">

          <span style="font-size: 12pt; letter-spacing: -0.05pt">УТВЕРЖДЕНА</span>

        </p>

        

        <p class="MsoNormal" style="text-align: center" align="center">

          <span style="font-size: 12pt; color: black; letter-spacing: -0.05pt">Программно-методическим</span><span style="font-size: 12pt; letter-spacing: -0.05pt"> советом ДТДиМ</span>

        </p>

        

        <p class="MsoNormal" style="text-align: center; text-indent: 1cm; background: none repeat scroll 0% 0% white" align="center">

          <span style="font-size: 12pt; letter-spacing: -0.05pt">Протокол № ____</span><span>  </span>от<span>   </span><span> </span>«____»__________ 200__ г.

        </p>

        

        <p class="MsoNormal" style="text-align: center; text-indent: 1cm; background: none repeat scroll 0% 0% white" align="center">

          <span style="font-size: 12pt; letter-spacing: -0.05pt">Председатель ___________________</span><span>  </span>Анисин А.А.

        </p>

        

        <p class="MsoNormal" style="text-align: center; text-indent: 1cm; background: none repeat scroll 0% 0% white" align="center">

          <span style="font-size: 12pt; letter-spacing: -0.05pt"> </span>

        </p>

      </td>

    </tr>

  </table>

</p>



<p class="MsoFooter" style="text-align: justify">

  &nbsp;

</p>



<p class="MsoFooter" style="text-align: justify">

  &nbsp;

</p>



<p class="MsoFooter" style="text-align: justify">

  &nbsp;

</p>



<p class="MsoFooter" style="text-align: justify">

  <strong> </strong>

</p>



### <span style="font-variant: small-caps; letter-spacing: -1pt"></span><span>  </span>Образовательная<span>  </span>программа<span>  </span>



### <span style="font-variant: small-caps; letter-spacing: -1pt">дополнительного образования детей</span>



<p class="MsoNormal">

  &nbsp;

</p>



<p class="MsoNormal">

  <span> </span>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <strong><span style="color: red"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 18pt">«ДУМАТЬ – ЭТО ИНТЕРЕСНО!</span></strong><strong><span style="font-size: 18pt">»</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <span style="font-size: 14pt">Интеллектуальное развитие </span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <span style="font-size: 14pt"> </span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <span style="font-size: 14pt"> </span>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <span style="font-size: 14pt">Срок реализации: 4 года</span>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <span style="font-size: 14pt">Возраст обучающихся: 11 - 16 лет</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 18pt"> </span></em>

</p>



<p class="MsoNormal" style="text-align: right" align="right">

  <span style="font-size: 14pt"></span><span>                                                                         </span>Автор:

</p>



<p class="MsoNormal" style="text-align: right" align="right">

  <strong><span style="font-size: 14pt">Анашина Н. Ю.,</span></strong>

</p>



<p class="MsoNormal" style="text-align: right" align="right">

  <span style="font-size: 14pt"></span><span>                                       </span><span>                            </span>педагог ДО

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt"></span><span>                                                                                                 </span></strong>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



### <span>                                                                                  </span>



<p class="MsoNormal" style="text-align: right" align="right">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 14pt"></span><span>  </span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 14pt"></span><span> </span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 14pt"> </span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <span style="font-size: 14pt">Москва</span>

</p>



<p class="MsoNormal" style="text-align: center" align="center">

  <span style="font-size: 14pt">2008</span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt">ПОЯСНИТЕЛЬНАЯ</span><span>   </span>ЗАПИСКА</strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span>АКТУАЛЬНОСТЬ, ПЕДАГОГИЧЕСКАЯ ЦЕЛЕСООБРАЗНОСТЬ</span><span>  </span>ПРОГРАММЫ</em></strong>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">В начале третьего тысячелетия не может оставаться прежним</span><span>  </span>взгляд на систему образования.<span>  </span>Большинство школьных учебных программ нацелены на репродуктивное воспроизведение готовых знаний. Улучшение собственно мыслительных способностей, развитие интеллекта подростков не предусматривается. <span> </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Данная программа культурологического направления деятельности предназначена для развития интеллектуальных способностей детей с использованием естественных для них игровых форм обучения и ориентирована на детей с начальным уровнем развития.</span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Сейчас, когда происходит лавинообразное увеличение объема знаний, на переднем плане должно стоять умение творчески подходить к самому процессу поиска и усвоения нового знания. Следует уделять больше внимания развивающим функциям обучения и делать это с самого начала учебы ребенка в школе. Известно, что процесс мышления включается только тогда, когда человек сталкивается с новой для себя задачей. Для развития навыков</span><span>  </span>эффективного мышления, требуется специально отработанная целенаправленная практика. «Подобно тому, как нужно научиться плавать до того, как начнешь тонуть, - говорил известный психолог Эдуард де Боно, - нужно чаще прибегать к нашему мышлению, развивать навыки нестандартного подхода к решению старых<span>  </span>и новых задач».<span>  </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">К настоящему времени накоплен значительный опыт развития творческого мышления при помощи различных программ, составленных известными психологами и педагогами. Однако, как мудро заметил когда-то Марк Твен: «Работа есть то, что мы обязаны делать, а игра есть то,</span><span>  </span>что мы не обязаны делать».<span>  </span>Наиболее ярко это проявляется при работе с детьми младшего школьного возраста. Игра для них является наиболее привычным, приятным и понятным<span>  </span>способом овладения приемами, навыками системного мышления. Гибкие рамки правил, многообразие форм проведения интеллектуальных игр дают простор детскому воображению.<span>  </span>Эффект усиливается еще и тем, что в игре присутствует главный фактор успешного обучения – активность обучающегося.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Очень важно вовремя научить ребенка не только ориентироваться в потоке информации, но и сохранить долговременный интерес (мотивацию) к самостоятельному поиску необходимых знаний, к самообразованию. Для этого следует ознакомить</span><span>  </span>детей с методами эффективного мышления, способствующими развитию природных способностей, «окультурить» интеллект.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Задания и тренинги в игровой форме не только способствуют созданию условий для развития психических функций интеллекта и повышению эрудиции. При умелом руководстве командная игра позволяет детям проявить лидерские качества, привить им навыки коммуникации и социальной толерантности, научить прислушиваться к высказываниям других.</span>

</p>



<p class="MsoBodyText" style="text-indent: 36pt">

  <span> </span>

</p>



<p class="MsoBodyText">

  <strong><em><span>ЦЕЛЬ</span><span>  </span>ПРОГРАММЫ</em></strong><strong><em><span style="font-size: 13pt"></span><span>  </span></em></strong><span style="font-size: 13pt">состоит в развитии мышления природных способностей детей к интеллектуальной творческой деятельности, формировании устойчивой мотивации к самообразованию.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 12pt"> </span>

</p>



<p class="MsoBodyText">

  <strong><em><span>ЗАДАЧИ ПРОГРАММЫ </span></em></strong><span style="font-size: 13pt">конкретизируют цель и подразделяются на обучающие,</span><span>  </span>воспитательные и развивающие.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">Развивающие:</span><span>  </span></em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">увеличить активный словарь обучающихся и на этой основе улучшить вербальные способности детей; </span>

</p>



<p class="MsoBodyText" style="margin-left: 36pt; text-indent: -18pt">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">расширить аппарат понятийного и проблемного мышления; </span>

</p>



<p class="MsoBodyText" style="margin-left: 36pt; text-indent: -18pt">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">развить логическое мышление;</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">Обучающие: </span></em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">научить детей пользоваться методами, позволяющими отойти от шаблонности мышления; </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">ознакомить обучающихся с элементами системного подхода;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">ознакомить детей с приемами эффективного запоминания информации.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">Воспитательные: </span></em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">сформировать у обучающихся навыки конструктивного коллективного обсуждения проблемных вопросов и ситуаций; </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">повысить мотивацию к обучению, получению новых знаний; </span><span> </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">воспитать позитивные личностные качества: терпение, самоорганизованность, толерантность, лидерские качества.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span>ПРЕЕМСТВЕННОСТЬ, ОСОБЕННОСТИ </span><span> </span>И НОВИЗНА ПРОГРАММЫ</em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"></span><span>      </span></em></strong><span style="font-size: 13pt">Данная программа построена с учетом идей и рекомендаций ученых и практиков, работающих с одаренными детьми: Д.Б. Богоявленской, В.Н. Дружиниа, Ю.Н. Белехова, А.И. Савенкова, а так же в программе А.З.Зака,</span><span>  </span>В. Стрениной и др.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">В программе предусмотрено использование игр и тренингов, стимулирующих развитие широкого спектра интеллектуальных способностей, формирование целостного взгляда на мир, которым отличаются широко образованные люди. В то же время у нее есть ряд особенностей, к которым можно отнести:</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">широкое использование тренингов, прошедших апробацию в психологической практике, которые после формализации правилами превращены в игры;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">использование игр на основе викторин различного типа с обновляемым банком вопросов и</span><span>  </span>заданий, гибкие правила и содержание которых позволяет подобрать задания, соответствующие возрасту и способностям детей;

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">активное участие обучающихся в соревнованиях по интеллектуальным играм различного уровня, вплоть до чемпионата мира, их участие в подготовке массовых мероприятий, проводимых на базе общеобразовательного учреждения, где они учатся;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">привлечение обучающихся к поиску интересной для них информации или информации на заданную тему, к работе с литературой;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">активное сотрудничество с родителями.</span>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>В первые два года обучения основной упор делается на развитие интеллектуальных базовых способностей: внимания, восприятия, памяти, фантазии и воображения, вербальных и способностей. На третьем и четвертом годах обучения основное внимание уделяется развитию логического, проблемного мышления, творческих способностей и практической деятельности обучающихся.

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>Постоянно обновляемый банк вопросов, ориентация на способности ребенка определили широту учебно-тематического плана программы: педагог вправе выбирать задания, соответствующие уровню знаний и способностям детей конкретной группы, делать упор на развитие тех или иных граней интеллекта детей.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">ВИД ПРОГРАММЫ</span><span>         </span></em></strong>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>Программа «Думать – это интересно!» является <strong><em>авторской культурологической направленности</em></strong>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"> </span>

</p>



<p class="MsoBodyText">

  <strong><em><span>ВОЗРАСТ</span><span>  </span><span> </span>ДЕТЕЙ, <span> </span>УЧАСТВУЮЩИХ <span> </span>В <span> </span>РЕАЛИЗАЦИИ <span> </span>ПРОГРАММЫ:</em></strong>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>В<span>  </span>группу первого года обучения принимаются дети 11-12 лет;<span>   </span>в группу второго года обучения – дети, обучавшиеся год по данной программе или дети 13 - 14 лет.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">В группу третьего года обучения принимаются дети 14 – 15 лет или дети 13 – 14 лет, которые два года обучались по данной программе; в группу четвертого года обучения принимаются дети 15 – 16 лет или дети, которые обучались по данной программе два – три года.</span>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>Количество детей в группах первого и второго года обучения составляет 10 – 12 человек. Такое количеством детей определяется, с одной стороны возможностью проследить за работой каждого ребенка во время занятий, с другой стороны - оптимальным количеством человек в команде при проведении интеллектуальных игр.

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>В группах третьего и четвертого годов обучения допустимо уменьшение количества обучаемых до 8 – 10 человек, что, с одной стороны, <span> </span>определяется численным составом команды таких игр, как «Что? Где? Когда?» и «Брэйн ринг» (не более 6 человек), а также необходимостью добиться эффекта состязательности во <span> </span>время проведения этих игр. Большее число детей группе приветствуется, но практика показывает, что в старших классах воспитанники очень загружены в общеобразовательных учреждениях.

</p>



<p class="MsoBodyText">

  <strong><em><span>ПРОДОЛЖИТЕЛЬНОСТЬ РЕАЛИЗАЦИИ ПРОГРАММЫ</span></em></strong>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>      </span>Общая продолжительность реализации программы – 4 года. Общий объем курса в общей сложности составляет 432 часа. В отдельных случаях допускается пятый год занятий с<span>  </span>детьми старшего школьного возраста. Такая ситуация возникает, когда сложилась хорошая команда, составляющая основу клуба, объединяющая детей по интеллектуальным интересам. Занятия с ними посвящены тренировкам и клубной деятельности.

</p>



<p class="MsoBodyText">

  <strong><em><span>ФОРМЫ</span><span>  </span>И<span>  </span>РЕЖИМ<span>  </span>ЗАНЯТИЙ</em></strong>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Основная форма занятий</span></strong><span style="font-size: 13pt"> - <em>групповая</em>, так же возможны <em>индивидуальные консультации</em> (по желанию детей или родителей), <em>участие в соревнованиях</em> по интеллектуальным играм различного ранга, <em>участие в массовых мероприятиях</em>, праздниках, проводимых учебным заведением, предусматривается <em>посещение музеев</em>, экскурсии на природу.</span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Основу программы составляют интеллектуальные упражнения и тренинги, логические задания, головоломки различного типа, проблемные вопросы, интеллектуальные игры. Упражнения, тренинги задаются в игровой, состязательной форме. </span><span> </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Каждое занятие в группах первого, второго годов обучения проводятся два раза в неделю в течение 1 часа. Количество учебных часов в год составляет 72 часа. </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Занятия в группах третьего и четвертого годов обучения продолжаются в течение 2-х часов и проводятся два раза в неделю. Количество учебных часов в год составляет 144 часа.</span><span>  </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">В группе пятого года обучения занятия со старшими школьниками проводятся один раз в неделю в течение 2-х часов.</span>

</p>



<p class="MsoBodyText">

  <strong><span style="font-size: 13pt"></span><span>   </span><span>   </span>Обязательным условием</strong><span style="font-size: 13pt"> для успешного выполнения Программы является тесная работа с родителями детей.<em> </em>Педагог обучает родителей, как правильно помочь ребенку найти нужную информацию, сообщает родителям об успехах и трудностях детей,</span><span>  </span>составляет с ними планы совместных мероприятий (экскурсий, праздников и др.).

</p>



# **_<span>ОЖИДАЕМЫЕ</span><span>  </span>РЕЗУЛЬТАТЫ И СПОСОБЫ ИХ ПРОВЕРКИ_**



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">После первого года</span><span>  </span>обучения у детей:</em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>1.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшатся вербальные способности</span></em><span style="font-size: 13pt">. Дети на слух понимают объяснения, реже нуждаются в пояснениях, легко подбирают и оперируют словами, используют понятия, отвечающие заданным условиям, умеют объяснять слова и понятия.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>2.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшится память</span></em><span style="font-size: 13pt">. Дети осваивают методы ассоциативного запоминания списков, улучшают слуховую память, привыкают к широкому «сканированию» своей памяти, извлекая необходимые сведения из самых различных областей знаний.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>3.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развивается фантазия и воображение</span></em><span style="font-size: 13pt">. Дети способны придумать несуществующее слово, понятие, дать им толкование, сочинить загадку, поговорку, сказку.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>4.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развивается логическое мышление</span></em><span style="font-size: 13pt">. Дети могут объяснить разницу и сходство между предметами, их существенные и несущественные признаки, сгруппировать предметы в соответствии с самостоятельно установленной закономерностью, дать объяснение своего решения, объяснить выбор варианта, найти понятие, аналогичное или противоположное заданному, подхватить и развить мысль товарища.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>5.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшаются личностно - социальные характеристики</span></em><span style="font-size: 13pt"> детей. Дети приобретают навыки совместной работы в небольших группах, делятся друг с другом интересной информацией, вносят предложения по проведению игр, мероприятий и др.</span>

</p>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">После второго года</span><span>  </span>обучения у детей:</em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>1.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развиваются вербальные способности</span></em><span style="font-size: 13pt">. Обучающиеся легко оперируют терминами, словами, подбирают антонимы, синонимы, омонимы и др., чтобы составить фразу с заданными требованиями, умеют пользоваться методами преобразования слов, могут распознать литературный стиль знакомого автора;</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>2.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развиваются творческие способности.</span></em><span style="font-size: 13pt"> Воспитанники способны составить вопрос по данной информации, составить образ из набора фигур, подать идею разрешения проблемы, развить сюжет старых и сочинить сказку по заданным условиям и т.д.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>3.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшается логическое мышление</span></em><span style="font-size: 13pt">. Обучающиеся могут выявить главное, увидеть закономерность, аналогию,</span><span>  </span>логично продолжить оборванную мысль.

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>4.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развивается проблемное мышление</span></em><span style="font-size: 13pt">. Дети способны генерировать идеи для решения заданной проблемы, создания конструкции, классифицировать и анализировать проблемную ситуацию, наметить варианты путей ее разрешения.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>5.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшаются личностно - социальные характеристики</span></em><span style="font-size: 13pt"> детей: обучающиеся способны к эффективному обсуждению в стрессовых условиях соревнования; у них расширяется круг интересов, повышается мотивация к получению знаний.</span>

</p>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">После третьего года обучения:</span></em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>1.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Улучшаются вербальные способности</span></em><span style="font-size: 13pt">. При этом воспитанники способны сформулировать новый вопрос по предложенной информации, составить задание бланковой викторины на заданную тему, самостоятельно провести вербальную игру, сделать подборку информации на указанную тему.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>2.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развивается проблемное мышление</span></em><span style="font-size: 13pt">. Воспитанники способны вычленить проблему в заданной проблемной ситуации, ибо не всегда понятно, что именно составляет трудность в такой ситуации. Обучающиеся пытаются анализировать проблему как недостающее звено некой ими же выбранной информационной системы, рассмотреть ее элементы и связи между ними, оценить варианты решения, выдвинуть гипотезу, пользуясь приемами: «по аналогии», «от противоположного», «переход от конца к началу», от общих сведений к частным и наоборот и др.; предложить нешаблонный путь решения.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>3.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развиваются творческие способности</span></em><span style="font-size: 13pt">. Дети способны углублять заданные образы, придумать рассказ с использованием заданных фраз или темы, произнести монолог от имени любого героя произведения, создать </span><span> </span>виршу из нескольких<span>  </span>рифмованных строк, пользоваться ассоциациями, аналогиями, <span> </span>доверять интуиции.

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>4.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Нарабатывается навык интеллектуального коллективного творчества</span></em><span style="font-size: 13pt">: умение слышать других в команде, продолжить мысль, подавлять негативные эмоции при неудаче; проявлять интерес к соревнованиям и участию в подготовке к ним.</span>

</p>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">После четвертого года обучения:</span></em></strong>

</p>



<p class="MsoNormal" style="margin-left: 0cm; text-align: justify; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>1.</span><span style="font: 7pt 'Times New Roman'">      </span><span style="font-size: 13pt"></span><span> </span><em>Развивается проблемное мышление.</em> Дети способны в конкретной проблемной ситуации сформулировать проблему и задачи, практически использовать законы создания нового - видеть проблему во взаимосвязи с окружающей реальности, произвести анализ с учетом развития ситуации во времени, оценить положительные и отрицательные стороны явления, учесть возможные последствия результатов решения, разрешать проблемные ситуации, требующие различных теоретических знаний, решать некоторые задачи ТРИЗ; использовать методы ухода от шаблонности мышления, использовать системный подход.

</p>



<p class="MsoNormal" style="margin-left: 0cm; text-align: justify; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>2.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Развиваются творческие вербальные способности.</span></em><span style="font-size: 13pt"> Дети могут составить сборник своих заданий, отредактировать подборку чужих вопросов, модернизировать правила </span><span> </span>некоторых<span>  </span>игр, самостоятельно подготовить и<span>  </span>провести игру.

</p>



<p class="MsoNormal" style="margin-left: 0cm; text-align: justify; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>3.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Нарабатывается навык интеллектуальной деятельности в коллективе</span></em><span style="font-size: 13pt">: навык обсуждения, взаимопонимания и терпимости; проявляются способности «генератора идей», «критика», «лидера» и т.д.; вырабатываются умение «держать напор», отстаивая свою точку зрения; уменьшается страх перед публичным выступлением, боязнь сцены и т.п.</span>

</p>



<p class="MsoNormal" style="margin-left: 0cm; text-align: justify; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>4.</span><span style="font: 7pt 'Times New Roman'">      </span><em><span style="font-size: 13pt">Закрепляется интерес к получению знаний</span></em><span style="font-size: 13pt">, работе с книгами, самообразованию.</span>

</p>



<p class="MsoBodyText2" style="text-align: justify; text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">Оценка результатов работы каждого ребенка в конце года</span></em></strong><span style="font-size: 13pt"> производится в соответствии с таблицей критериев результатов обучения и личностного развития ребенка, которая дифференцирована по годам обучения.</span><span>  </span>Таблица приведена в разделе «Методическое обеспечение программы».

</p>



<p class="MsoBodyText" style="text-indent: 36pt">

  <strong><em><span style="font-size: 13pt">Оценка результатов</span></em></strong><span style="font-size: 13pt"> <strong><em>работы детей на занятиях</em></strong> проводится с помощью рейтинговой системы накопления очков. Дети заполняют свои таблицы сами в конце урока. Каждый тип задания имеет свой рейтинг (очки). Педагог может добавить баллы за оригинальность, образность, количество и скорость выполнения заданий, размер (длину цепочки слов, объем рассказа и т.д.).</span>

</p>



<p class="MsoBodyText2" style="text-align: justify; text-indent: 36pt">

  <strong><em><span style="font-size: 13pt">Результаты тестирования </span></em></strong><span style="font-size: 13pt">в начале и конце года (при отсутствии профессионального психолога в учреждении) </span><span> </span>помогают сделать вывод о динамике изменения способностей детей, соответствии параметрам таблиц критериев результатов обучения и критериев личностного развития ребенка. Используются тесты, оценивающие вербальное, знаковое, образное мышление детей, их внимание, сосредоточенность, объем памяти и др.

</p>



<p class="MsoBodyText2" style="text-align: justify; text-indent: 35.4pt">

  <strong><em><span style="font-size: 13pt">Внешней экспертной оценкой достижений детей</span></em></strong><span style="font-size: 13pt"> <strong><em><span> </span></em></strong>является участие в конкурсах, фестивалях интеллектуальных игр, проводимых другими организациями. Каждое достижение в соревнованиях, конкурсах, фестивалях</span><span>  </span>также оценивается в определенной сумме баллов. Подготовка и проведение мероприятий и соревнований в своем образовательном учреждении<span>  </span>является способом практического приложения знаний и умений детей.

</p>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <span> </span>

</p>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  &nbsp;

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt">УЧЕБНО - ТЕМАТИЧЕСКИЙ ПЛАН</span></strong><span style="font-size: 14pt"></span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>Первый год обучения</span></strong>

</p>



<table class="MsoNormalTable" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 9.25pt">

    <td rowspan="3" style="width: 369pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="492">

      <p class="MsoBodyText" style="margin-right: -53.7pt; text-align: center" align="center">

        <strong> </strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong>Разделы и темы</strong>

      </p>

    </td>

    

    <td colspan="3" style="width: 105.55pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="141">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 11pt">Колич. уч.часов</span></strong>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>Второй год обучения</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span> </span></strong>

</p>



<table class="MsoNormalTable" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 9.25pt">

    <td rowspan="3" style="width: 369pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="492">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong> </strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong>Разделы и темы</strong>

      </p>

    </td>

    

    <td colspan="3" style="width: 105.55pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="141">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 9pt">Количество </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 9pt">учебных часов</span></strong>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>  </span>Третий<span>  </span>год<span>  </span>обучения</strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<table class="MsoNormalTable" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 9.25pt">

    <td rowspan="3" style="width: 369pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="492">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong> </strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong>Разделы и темы</strong>

      </p>

    </td>

    

    <td colspan="3" style="width: 108pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="144">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 11pt">Количество </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 11pt">учебных часов</span></strong>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong>Четвертый<span>  </span>год<span>  </span>обучения</strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong> </strong>

</p>



<table class="MsoNormalTable" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 9.25pt">

    <td rowspan="3" style="width: 368.15pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="491">

      <p class="MsoBodyText">

        <strong> </strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong>Разделы и темы</strong>

      </p>

    </td>

    

    <td colspan="3" style="width: 110.2pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 9.25pt" valign="top" width="147">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 11pt">Количество</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span style="font-size: 11pt">учебных часов</span></strong>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt">СОДЕРЖАНИЕ ИЗУЧАЕМОГО КУРСА</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 10pt"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 13pt">Первый год обучения</span></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">1. Введение в программу</span><span>  </span></em></strong><em><span style="font-size: 13pt">(1 час)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.1.</span><span>  </span>Тема: О программе и правилах поведения. </em><span style="font-size: 13pt">Знакомство с родителями и детьми, сведения об особенностях программы,</span><span>  </span>ожидаемых результатах. Правила внутреннего распорядка и техники безопасности. Цели и задачи Программы,<span>  </span>особенности курса первого года обучения – упор на развитие воображения, базовых интеллектуальных способностей (наблюдательность, восприятие, память, развитие речи). Роль игры в процессе развития интеллекта человека. Каткая характеристика типов интеллектуальных игр. Возможные результаты, достигаемые к концу учебного года. Материалы для работы. Желаемые формы общения и взаимодействия преподавателя с родителями. Заполнение анкеты родителями.

</p>



<p class="MsoBodyText" style="margin-left: 24pt; text-indent: -24pt">

  <span style="font-size: 13pt"></span><span>1.2.</span><span style="font: 7pt 'Times New Roman'">   </span><em><span style="font-size: 13pt">Тема: Игра «Ассоциации». </span><span> </span></em><span style="font-size: 13pt">Понятие ассоциации, развитие вербальных способностей в игровых формах, игра</span><span>  </span>«ассоциации» с карточками-рисунками.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">2. Развитие вербальных способностей</span><span>  </span></em></strong><em><span style="font-size: 13pt"></span><span> </span>(13 часов)</em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.1.</span><span>  </span>Тема: Некоторые сведения об особенностях слов русского языка. </em><span style="font-size: 13pt">Созвучия и многозначность слов. Изменение содержания слов со временем. Синонимы, омонимы, антонимы и т.д. Иерархия понятий. Группировка слов по смыслу, форме, частям речи и др.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.2.</span><span>  </span>Тема: Устные задания по работе со словами. </em><span style="font-size: 13pt">Устный подбор слов с указанными свойствами. Определение общего для заданного ряда слов.</span><span>  </span>Задания: продолжить ряд слов, объединенных общими грамматическими или лексическими признаками; выявить общие признаки группы слов; объяснить смысл (совпадение или различие смысла) нескольких слов – синонимов, антонимов, омонимов и т.д.; определить вид соподчинения слов и продолжить ряд.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.3. Тема: </span><span> </span>Задания в рисунках по работе со словами.<span>  </span></em><span style="font-size: 13pt">Устные или письменные задания: найти на рисунках предметы, означаемые словами-омонимами, антонимами; назвать одни и те же предметы словами -</span><span>  </span>синонимами; отыскать предметы по неким признакам, дать толкование предметам, составить задание, основанную на изученных свойствах слов.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.4.</span><span>  </span>Тема: Устные и бланковые задания по работе со словами и фразами.<span>  </span></em><span style="font-size: 13pt">Задания: записать как можно больше слов с заданными грамматическими свойствами, уловить закономерность и продолжить ряд слов, подобрать рифму. Задания типа «телеграмма», «угадайка», «поле чудес на бумаге», «море слов» и т.д. Составить собственное задание аналогичного типра.</span>

</p>



<p class="MsoBodyText" style="margin-left: 0cm; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>2.6.</span><span style="font: 7pt 'Times New Roman'">     </span><em><span style="font-size: 13pt">Тема: Устные и бланковые словесные игры.</span><span>  </span></em><span style="font-size: 13pt">Устные игры: «грузить пароход», «строка из песни», «контакт» и др.</span><span>  </span>Бланковые задания: «чайник», «плетенка», «пирамида», «натворд», «общие части слов» и др.<span>  </span>Игры на бумаге: «наборщик», «балда». Составить собственное задание аналогичного пирамиде, натворду и др.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">3. Развитие воображения и фантазии</span><span>  </span></em></strong><em><span style="font-size: 13pt">(10 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.1.</span><span>  </span>Тема: Сочинение новых слов, их толкование.<span>  </span></em><span style="font-size: 13pt">Сочинение новых слов, «тарабарского языка», новые названия к старым предметам,</span><span>  </span>понятиям и др. Задания: придумать неизвестный предмет, устройство или живое существо, дать название, объяснить назначение или образ жизни и т.п.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.2.</span><span>  </span>Тема: Создание новых загадок, пословиц, фраз.<span>  </span></em><span style="font-size: 13pt">Анализ нескольких загадок, сравнение предметов по разным признакам, по назначению, материалам и т.д. Анализ признаков объекта для загадки, подбор объекта для сравнения. Создание новой загадки «по аналогии», по собственному выбору. Анализ пословиц и поговорок, толкование их смысла, ситуаций, когда их можно применить. Создание пословицы «по аналогии», на определенную тему, на тему, выбранную самими детьми. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.3. Тема: </span><span> </span>Задания с готовыми рисунками </em><span style="font-size: 13pt">Задания: подписать рисунок, придумать прошлое и будущее изображенной ситуации; развить сюжет, исходя из рисунка предмета; придумать варианты завершения изображенной ситуации.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.4. Тема:</span></em><span style="font-size: 13pt"> <em>Создание новых рисунков.</em></span><span>  </span>Задания: сложить из спичек известную фигуру; нарисовать неизвестное устройство или животное; создать рисунок из заданных геометрических фигур на заданную, затем собственную тему.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.5.</span></em><span style="font-size: 13pt"> <em>Тема: Игры. </em>Игры с варьированием голоса («интонации», «разное чтение стихов»). Теневой театр из рук (теневые фигуры животных, предметов). Сюжетные жесты руками («одной рукой», «как это делают»). Игры с движениями тела («передай другому»).</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">4. Методы запоминания информации, тренинг памяти, сосредоточенности, наблюдательности</span><span>   </span></em></strong><em><span style="font-size: 13pt">(9 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.1. Тема: </span><span> </span>Краткие сведения о видах памяти. Гигиена памяти.<span>  </span></em><span style="font-size: 13pt">Виды памяти: оперативная и долговременная, зрительная и слуховая и т.д. Правила гигиены мозга. Свойства некоторых видов памяти (амнезия, астения, циклотомия). Правила гигиены памяти.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.2. Тема: Естественный ход запоминания.</span><span>  </span></em><span style="font-size: 13pt">Три главных этапа запоминания: впечатления, ассоциации,</span><span>  </span>повторение.<span>  </span>Упражнения для улучшения фиксации информации на этих этапах. Закон Жоста.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.3. Тема: Некоторые способы запоминания информации. </span></em><span style="font-size: 13pt">Простейшие приемы запоминания слов, однородных понятий:</span><span>  </span>классификация (группировка), ассоциативные связки («запоминальная» история»), метод кумулятивного повторения.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.4. Тема: Умение сосредоточиться. </span></em><span style="font-size: 13pt">Правила для достижения эффективной сосредоточенности: выбор условий, исключение причин рассеивания внимания, хорошая физическая форма, базовые знания, повышение мотивации.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.5. Тема: Игры и тренинги на запоминание. </span></em><span style="font-size: 13pt">Тренинг зрительной памяти «муха».<strong> </strong>Тренинг оперативной памяти:</span><span>  </span>фразы «считалки – не запоминалки», «снежный ком» и др.. Игры-тренинги: «строка из песни», «святцы», «грузить пароход», «ассоциации» с рисунками, «найди пару» и др.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">5. Тренинг пространственно – комбинаторного мышления</span><span>  </span></em></strong><em><span style="font-size: 13pt">(9 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.1.</span><span>  </span>Тема: Работа с плоскостными головоломками (пазлами). </em><span style="font-size: 13pt">Поиск решения готовых плоскостных головоломок. Вырезание деталей головоломок по чертежам и создание полного комплекта головоломки. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.2.</span><span>  </span>Тема: Знакомство с некоторыми алгоритмами выполнения фигур оригами. </em><span style="font-size: 13pt">Основные приемы оригами, условные обозначения на чертежах алгоритмов: линии, стрелки, типы сгибов и т.д. Базовые фигуры оригами (двойной квадрат, двойной треугольник, домик, конверт и т.д.).</span><span>  </span>Алгоритмы выполнения фигур (лодки, стаканчики, шапки и др.).

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.3.</span><span>  </span>Тема: Решение плоскостных и объемных задач. </em><span style="font-size: 13pt">Лабиринты, пространственные задачи (фигуры из проволоки, «бирюльки» и др.), </span><span> </span>сборка фигур из выкроек (перекати-поле, новогодний додекаэдр), задачи на укладку деталей. Сборка фигур из полосок, карточек.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.4. Тема: Игры</span><span>  </span>на организованном поле и комбинаторные игры. </em><span style="font-size: 13pt">Варианты игр «крестики-нолики», го, «китайская стена», палочки, «морской бой», «летучие мыши» и т.д.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">6. Развитие логического мышления</span><span>  </span></em></strong><em><span style="font-size: 13pt">(10 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.1.</span><span>  </span>Тема: Анализ свойств явлений и предметов. </em><span style="font-size: 13pt">Задание: назвать существенные признаки сходства и различия двух или нескольких предметов; обосновать возможность использования предметов не по назначению; обосновать конкретное воплощение, образ неких абстрактных понятий («Музей рекордов», «Разрешающие таблички» и др.) Тренинг «группировка», «назовите третье».</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.2.</span><span>  </span>Тема: Анализ причинно-следственных связей заданных ситуаций. </em><span style="font-size: 13pt">Обсуждение проблемных ситуационных вопросов типа: почему такое могло произойти; как добиться желаемого результата, что нужно для того, чтобы сделать… и т.д.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.3.</span><span>  </span>Тема: Логические задачи и некоторые алгоритмы их решения. </em><span style="font-size: 13pt">Задачи с тремя – четырьмя объектами и двумя, тремя наборами признаков или атрибутов. Приемы для решения: обозначения и связи,</span><span>  </span>рисунки, фигурки – модели объектов, таблицы.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.4. Тема: Детективные истории в устных задачах и рисунках. </span></em><span style="font-size: 13pt">Короткие устные истории из сборников,</span><span>  </span>анализ основных положений, выявление противоречий. Детективы-комиксы в рисунках (серия о Карандашах и Ластиках, об инспекторе Варнике и др.)

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">7. Индивидуальные интеллектуальные игры</span><span>  </span></em></strong><em><span style="font-size: 13pt">(12</span><span>  </span>часов)</em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.1.</span><span>  </span>Тема: Системы подсчета очков устных интеллектуальных игр. </em><span style="font-size: 13pt">Круговая система с накоплением. Система «покера» до первой ошибки.<em> </em></span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.2.</span><span>  </span>Тема: Основные формы бланковых интеллектуальных игр. </em><span style="font-size: 13pt">Таблицы с графой ответа, бланки с номером варианта ответа, форма «хаос».</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">7.3.</span><span>  </span>Тема: Игры, основанные на группировке и классификации.</em><span style="font-size: 13pt">«Ассоциации-3», «синонимы» и «льзя иначе», «антонимы» и «перевертыши».</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.4. Тема: Игры-тренинги для оперативной и долговременной памяти. </span></em><span style="font-size: 13pt">«Фразы-считалки», «снежный ком», «строка из пени», «хоровод», «ассоциации-1 и 2», «муха», варианты «крестиков - ноликов»</span><span>  </span>вслепую и др.

</p>



<p class="MsoNormal">

  <em><span style="font-size: 13pt">7.5. Тема: Игры, использующие элементы индукции и дедукции. </span></em><span style="font-size: 13pt">«И – или - нет», «реалии», «девятый вал», «пентагон», «булева алгебра», «ситуации», др.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt"> </span></em></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">8. Практическая деятельность </span></em></strong><em><span style="font-size: 13pt">(8 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">8.1. Тема: </span><span>  </span>Подготовка к массовому мероприятию. </em><span style="font-size: 13pt">Подготовка бланков и деталей головоломок для заданий, посильное участие в подготовке помещения.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">8.2.</span><span>  </span>Тема: Участие в массовом мероприятии учреждения. </em><span style="font-size: 13pt">Участие в праздниках, соревнованиях, конкурсах, проводимых данным учебным учреждением.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">9. Тестирование обучающихся, анкетирование родителей </span></em></strong><em><span style="font-size: 13pt">(2 часа)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.1. Тема: </span><span> </span>Преобладающий тип мышления ребенка.<span>  </span></em><span style="font-size: 13pt">Определение преобладающего типа мышления ребенка: тесты, игры, задания с числом, фигурами, буквами, образами. Типовая анкета обучающихся, тест – опросник родителей на способность детей к творчеству.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.2. Тема: Тесты на запоминание и внимание. </span></em><span style="font-size: 13pt">Тесты на запоминание и внимание: числовые и буквенные таблицы Шультке, рисунки.</span>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt; background: none repeat scroll 0% 0% lime"> </span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 13pt">Второй год обучения</span></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">1. Особенности программы второго года обучения</span></em></strong><em><span style="font-size: 13pt"></span><span>  </span>(1 час)</em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.1.</span><span>  </span>Тема: Знакомство с вновь поступившими детьми. Особенности курса второго года обучения. </em><span style="font-size: 13pt">Основные темы курса направлены на развитие базовых интеллектуальных способностей. Ожидаемые результаты: приобретение навыков и умений активизации интеллектуальной деятельности. </span><span> </span>Игровые <span> </span>способы достижения желаемых результатов, правила ТБ и внутреннего распорядка.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.2. Тема:</span><span>  </span>Показательная игра. </em><span style="font-size: 13pt">Комплексной игры с различными типами несложных вопросов. Анализ вопросов,</span><span>  </span>Стратегия и тактика игры.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">2. Развитие вербальных способностей</span><span>  </span></em></strong><em><span style="font-size: 13pt">(8 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.1. Тема: Некоторые сведения о приемах, украшающих речь.</span><span>  </span></em><span style="font-size: 13pt">Тропы (метафоры, гиперболы, метонимика и др.),</span><span>  </span>игра слов на основе многозначности, совпадение разных слов при изменениях грамматических форм, использование фразеологизмов, идиом и т.д.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.2.</span><span>  </span>Тема: Бланковые задания по работе с фразами и текстами. </em><span style="font-size: 13pt">Рифмованные слова, фразы с рифмами, «фразы перевертыши», фразы «синонимы», фразы</span><span>  </span>«оборвыши», палиндромы, задания «он и она», фразы с гаплологией и др.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.3. Тема: </span><span> </span>Устные игры и задания со словами, фразами и текстами. </em><span style="font-size: 13pt"></span><span> </span>«Цитатник», перевертыши, идиомы с<span>  </span>«джокером», фразы с омонимами в контексте,<span>  </span>«маскарад или травести», шарады, шароиды и кубраички, словесные покерные подборки.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">3. Развитие творческих способностей</span><span>  </span></em></strong><em><span style="font-size: 13pt">(10 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.1. Тема: </span><span> </span>Задания с рисунками, плоскими фигурами, предметами. </em><span style="font-size: 13pt">Задания: составить из набора геометрических фигур как можно больше фигур-образов, дать им название, зарисовать схему в тетради.<strong><em> </em></strong>Рисунки «не отрывая карандаша от бумаги». Памятники литературным героям и т.д. («Съедобный зоопарк», «Чудодейственное лекарство», «Памятник»). </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.2. Тема: Сочинение новых сказок, рассказов, сюжетов. </span></em><span style="font-size: 13pt">Старая сказка с персонажем в новой роли, объяснялка происхождения явления, рассказ по отдельным строкам и т.д.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.3.</span><span>  </span>Тема: Устные монологи.<span>  </span></em><span style="font-size: 13pt">Монолог от имени второстепенного героя сказки; речь известного героя, но в других условиях (изменены место или эпоха); ритмические и/или рифмованные пожеланья и т.д.</span><span>  </span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">4. Развитие пространственно-комбинаторного мышления</span></em></strong><em><span style="font-size: 13pt">(7 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.1.</span><span>  </span>Тема: Работа с плоскостными и пространственными головоломками. </em><span style="font-size: 13pt">Складывание фигур из готовых головоломок («икебана», «снежинки» и др.)</span><span>  </span>Склеивание деталей пространственных головоломок из выкроек. Укладки, паркеты, коврики, танграмы (изготовление и решение заданий танграмов).

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.2. Тема: </span><span> </span>Изготовление бумажных фигур оригами. </em><span style="font-size: 13pt">Стандартные формы («катамаран»,</span><span>  </span>«рыба», «птица» др.). Алгоритмы фигур (самолеты, птицы»), чтение чертежей.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.2.</span><span>  </span>Тема: Трансформация фигур. </em><span style="font-size: 13pt">Задачи со спичками (выкладывание фигур с выполнением заданных условий), преобразование фигур (квадраты, краб, фонарь, весы и др.).</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.3. Тема: </span><span> </span>Игры на бумаге, на организованном поле. </em><span style="font-size: 13pt">«Футбол», «точки», «ход коня», «мостик», «рассада», «летучие мыши» и т.д.; решение игровых задач.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">5. Тренинг наблюдательности, памяти</span><span>   </span></em></strong><em><span style="font-size: 13pt">(8 часов)</span></em><strong><span style="font-size: 13pt"></span></strong>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.1. Тема: </span><span> </span>Мнемонические методы запоминания информации. </em><span style="font-size: 13pt">Правила и приемы метода ассоциативных связей. «Мысленные картинки», динамика, введение экспрессии: неожиданные, нелепые ситуации, неожиданные ассоциации,</span><span>  </span>введение эмоций, юмора и т.д.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.2.</span><span>  </span>Тема: Методы запоминания последовательности действий. </em><span style="font-size: 13pt">Метод лоции, маршрут памяти, выделение ключевого образа начала - начало отсчета, выбор обоснованного направления движения по маршруту. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.3. Тема: </span><span> </span>Запоминание списка слов абстрактных, иностранных, редких, новых, незнакомых терминов. </em><span style="font-size: 13pt">Перевод в конкретные образы, использование зрительных, слуховых ассоциаций,</span><span>  </span>усиление за счет образов, эксплуатирующих другие органы чувств, наличие внутренней логичности преобразования - ясность преобразования.<strong> </strong>

</p>



<p class="MsoBodyText" style="text-align: left" align="left">

  <em><span style="font-size: 13pt">5.4.</span><span>  </span>Тема: Запоминание текстов. </em><span style="font-size: 13pt">Классификация текста: сведения, порядок следования, разные аспекты одной темы и др.</span><span>  </span>Выделение ключевых моментов:<span>  </span>преобразование абстрактных образов в конкретные,<span>  </span>составление историй по правилам запоминания и др.. Запоминание цитат: конкретные образы цитаты, привязка к автору, смыслу цитаты.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.5. Тема: Игры – тренинги памяти и наблюдательности. </span></em><span style="font-size: 13pt">Бланковые задания типа «хаос», любые подборки - покеры на определенную тему, «география». Устные игры</span><span>  </span>«мнеморина», «по первой строке»,<span>  </span>«ассоциации», «девятый вал», речитативы типа «Зеленая история». Задания-тренинги наблюдательности: работа с рисунками, фотографиями, описания архитектурных сооружений, известного пути и т.д. Сочинение «фраз – считалок».

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">6. Развитие проблемного мышления</span><span>  </span></em></strong><em><span style="font-size: 13pt">(11 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.1.</span><span>  </span>Тема: Методы обсуждения проблемной ситуации. </em><span style="font-size: 13pt">Мозговой штурм, история создания,</span><span>  </span>его разновидности. Метод фокальных объектов, методы гирлянды случайностей и ассоциаций.<span>  </span>Четыре вида аналогий:<span>  </span>прямая аналогия,<span>  </span>символическая аналогия, фантастическая аналогия,<span>  </span>личная аналогия (эмпатия). Метод от противоположного.

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">6.2. Тема: </span><span> </span>Способы записи идей по ходу обсуждения проблемы. </em><span style="font-size: 13pt">Кластеры, деревья, таблицы.<strong><em> </em></strong>Создание символов понятий, рисунков – ситуаций, вывод по теме.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.3. Тема: </span><span> </span>Основные этапы разрешения проблемной ситуации. </em><span style="font-size: 13pt">Проблемный анализ: выявление задачи, поэтапное, обнаружение первопричины. Формулирование конечного результата, выявление и уточнение противоречий, мешающих его достижению. Поиск путей разрешения противоречий. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.4.</span><span>  </span>Тема: Решение некоторых теоретических и практических проблем. </em><span style="font-size: 13pt">Подборка заданий из сборника проблемных задач, соответствующих возрасту обучающихся. Решение проблем и задач из истории Древнего мира, биологические задачи, разрешение проблемы путем проектов создания неких конструкций, разрешение бытовых проблем.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">7. Интеллектуальные командные игры </span></em></strong><em><span style="font-size: 13pt">(10 часов.)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.1.</span><span>  </span>Тема: Правила некоторых интеллектуальных командных игр. </em><span style="font-size: 13pt">Варианты правил игр «Что? Где? Когда?» (ЧГК), «Брэйн-ринг» (БР), «Чеширский кот», «Надуваловка» и т.д.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.2. Тема: </span><span> </span>Стратегия и тактика команды в интеллектуальных играх. </em><span style="font-size: 13pt">Для игры «Что? Где? Когда?»: определение типа вопроса, область информации. Поиск ключевых слов: место, время или название; умение услышать все версии, отбросить неудовлетворяющие преамбуле. Умение привлекать дополнительную информацию путем формулирования дополнительных вопросов к частям вопроса ЧГК. Знание особенностей (сильных и слабых сторон)</span><span>  </span>членов команды и даже автора вопросов.<span>  </span>Для игры «Брэйн-ринг»: доверять интуиции, сканирование памяти во время чтения вопроса, при отсутствии версии - дать возможность противнику ошибиться первым и др.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.3.</span><span>  </span>Тема: Требования к членам команды. </em><span style="font-size: 13pt">Не бояться обнародовать даже нелепые идеи, умение слышать товарища, продолжить мысль, не высказывать вслух отрицательные эмоции, быстро переключаться на работу над следующим вопросом, не тратить время на глупые восклицания и переживания</span><span>  </span>и т.д.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.4.</span><span>  </span>Тема: Классификация вопросов интеллектуальных игр. </em><span style="font-size: 13pt">Аналогия, противопоставление, ситуационный, продолжить ряд, двух-, трехступенчатые, введение ключевых слов, слова – подсказки, комплексные вопросы и т.д.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.5.</span><span>  </span>Тема: Проведение игр. </em><span style="font-size: 13pt">Игры индивидуальные<em> </em>бланковые и устные (и-или-нет, да-нетки, девятый вал, своя игра и др.), командные (пентагон, ЧГК, чеширский кот, Своя игра вдвоем, втроем и др.)</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">8. Практическая деятельность </span></em></strong><em><span style="font-size: 13pt">(10 часов)</span></em><span style="font-size: 13pt"></span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">8.1.</span><span>  </span>Тема: Подготовка к массовым мероприятиям. </em><span style="font-size: 13pt">Изготовление заготовок атрибутики, тиражирование бланков при подготовке к занятиям или массовым мероприятиям, подготовка помещения к проведению мероприятий.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">8.2. Тема: Участие в массовых мероприятиях. </span></em><span style="font-size: 13pt">Участие в мероприятиях своего учебного заведения, в соревнованиях в масштабе школы, округа.</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: -18pt">

  <strong><em><span style="font-size: 13pt"></span><span>9.</span><span style="font: 7pt 'Times New Roman'">      </span></em></strong><strong><em><span style="font-size: 13pt">Тестирование обучающихся </span></em></strong><em><span style="font-size: 13pt">(2 часа)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.1. Тесты на внимательность. Скорость восприятия. </span></em><span style="font-size: 13pt">Тесты: скорость восприятия и мышления: «красно-черная таблица», таблицы со словами, «корректурно-буквенная проба», «визуал или аудиал»? </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.2. Тема: </span><span> </span>Оценка логики мышления.<span>  </span></em><span style="font-size: 13pt">Задания: количественные отношения, закономерность ряда.</span><span>  </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.3. Тема: Оценка творческих способностей. </span></em><span style="font-size: 13pt">Тест Торренса. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">9.4. Тема: Оценка логического мышления. </span></em><span style="font-size: 13pt">Тест Равенна, </span><span> </span>«африканец».

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 13pt"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 13pt">Третий год обучения</span></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">1. Особенности программы третьего года обучения </span></em></strong><em><span style="font-size: 13pt">(1</span><span>  </span>час)</em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.1.</span><span>  </span>Тема: Основные темы Программы третьего года обучения. </em><span style="font-size: 13pt">Виды игр и занятий, направленных на развитие логики, проблемного мышления, приобретение практических навыков.</span><span>  </span>Правила ТБ и внутреннего распорядка для вновь поступивших детей.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.2. Тема:</span><span>  </span>Командная комплексная игра. </em><span style="font-size: 13pt">Проведение комплексной игры, поведение игроков, самостоятельная формулировка правил поведения игрока в команде во время игры.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">2. Развитие вербальных и творческих способностей </span></em></strong><em><span style="font-size: 13pt">(18 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.1. Тема: Вербальные задания, тренинги и игры. </span></em><span style="font-size: 13pt">Устное решение заданий и проведение вербальных игр в качестве интеллектуальной «разминки». Устное создание словесных и текстовых заданий по аналогии, по заданной схеме, по таблице и т.д.</span><span style="background: none repeat scroll 0% 0% yellow"> </span>История письменности как проблемная ситуация. Задание «Рисуночное письмо» с передачей заданной информации. Отличие рисунчатого письма от пиктограммы. Придумать назначение предложенным пиктограммам – сделать подпись под пиктограммами.

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">2.2.</span><span>  </span>Тема: Самостоятельная формулировка вопросов. </em><span style="font-size: 13pt">Формулировка вопроса по заданной информации. Варианты вопросов по одной информации, редактирование вопросов.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.3. Тема: Создание вопросов для бланковых вербальных игр и заданий. </span></em><span style="font-size: 13pt">Самостоятельная подготовка с записью вербальных заданий по предложенной информации, во время занятия, в качестве домашнего задания.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">3. Развитие логического и проблемного мышления </span></em></strong><em><span style="font-size: 13pt">(46 часов)</span></em>

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">3.1.</span><span>  </span>Тема: Наиболее общие законы создания или открытия нового, законы или правила ТРИЗ.<span>  </span>Системность</em><span style="font-size: 13pt">: не существует в природе или технике отдельных изолированных вещей, событий или систем; законы из взаимосвязи.</span><span>  </span>Анализ связей между элементами системы, выявление недостающих элементов или связей. <em>Закон повышения идеальности конструкции</em>, породивший метод «идеального пути усовершенствования системы»: идти не от начала исходной задачи, а с конца, с формулировки «идеального конструкторского решения». Метод анализа проблемы с выходом в над- или подсистему. <em>Закон диалектических противоречий</em>: основные признаки, причины, последствия, условия разрешения административных (человек – техника),<span>  </span>технических и физических противоречий.<span>  </span><em>Закон S-образного развития систем,</em> отражающий этапы развития технических систем от единичного, затем массового использования, устаревания и переходя к новым принципам. Примеры проявления этих законов.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.2.</span><span>  </span>Тема: Правила и методы обсуждения проблемной ситуации. </em><span style="font-size: 13pt">Допустимая критика идей, 4 вида аналогий (прямая,</span><span>  </span>символическая, фантастической и<span>  </span>личная аналогия<span>  </span>- эмпатия). Метод ухода от шаблонности мышления<span>  </span>(метод ПРО – провоцирующая операция). Приемы выбора оптимального решения для некоторых видов задач.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.3. Тема: Решение проблемных вопросов различного типа. </span></em><span style="font-size: 13pt">Решение заданий из сборника проблемных вопросов и ситуаций для детей данного возраста.</span><span>  </span>Введение в проблемную ситуацию. Формулирование проблемы и задач для ее решения. Общее обсуждение с анализом противоречий, выявления первопричины, выявление элементов системы, основных связей между ними и отсутствующих элементов или связей. Генерация идей, обсуждение вариантов решения проблемы, их анализ, выбор оптимального решения.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.4.</span><span>  </span>Тема: Понятие системы, ее свойства и структура</em>

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 13pt">Произвольность понятия системы. Системообразующий элемент, механизм действия, элементы и связи между ними. Структура системы, системное свойство. Задания на отыскание системообразующего элемента, механизма, связей и т.д. Привести примеры собственных систем. Составить структуру информационной системы – вопроса игры «Что? Где? Когда?»</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">4. Интеллектуальные игры и задания </span></em></strong><em><span style="font-size: 13pt">(46 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.1. Тема: </span><span> </span>Анализ вопросов различного типа комплексных игр. </em><span style="font-size: 13pt">Вопросы на «голое знание», по аналогии, подобную ситуацию, обобщение, продолжить ряд, выявить лишнее, поэтапные (многоходовки), ключевые слова, скрытая подсказка, комплексные знания.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.2.</span><span>  </span>Тема: Тренинг членов команды на совместимость в группе, психологическую устойчивость. </em><span style="font-size: 13pt">Тест-тренинг «подхвати товарища», игра «слепой и поводырь», «праздник вежливости», игра «пойми меня» и др.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.3.</span><span>  </span>Тема: Подготовка вопросов для интеллектуальных игр. </em><span style="font-size: 13pt">Номинации вопросов для «своей игры» по предложенным темам,</span><span>  </span>вопросы ЧГК и БР по предложенным<span>  </span>информационным текстам. Подборка вопросов для соревнований для детей указанного возраста.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">4.4.</span><span>  </span>Тема: Участие в играх. </em><span style="font-size: 13pt">Участие в соревнованиях между классами, школами, выход на окружные и городские соревнования.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">5. Практическая деятельность </span></em></strong><em><span style="font-size: 13pt">(25 часов). </span></em><span style="font-size: 13pt">Участие в подготовке программы массового мероприятия учебного заведения,</span><span>  </span>в составе оргкомитета, вспомогательного конкурсного жюри, в конкурсах, соревнованиях сторонних организаций.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">6. Организационная деятельность</span><span>  </span></em></strong><em><span style="font-size: 13pt">(6 часов) </span></em><span style="font-size: 13pt">Самостоятельное проведение интеллектуальных игр; организационная работа с участниками массового мероприятия, организационная деятельность по подготовке и проведению собственного массового мероприятия.</span>

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">7.</span><span>  </span>Тестирование обучающихся </em></strong><em><span style="font-size: 13pt">(2 часа)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.1. </span><span> </span>Тема: Тесты на внимательность</em><span style="font-size: 13pt">. </span><span>  </span>Тесты: «интеллектуальная лабильность», «исключение понятий».

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">7.2. Тема: Оценка творческих способностей. </span></em><span style="font-size: 13pt">Качественная оценка творческих способностей у детей с преобладанием какого-либо типа мышления: вербальный -</span><span>  </span>«перепутанные записи»; логическое - «археолог»; пространственно-комбинаторный - «наскальные рисунки»; <span> </span>знаковый - «знаковое письмо».

</p>



<p class="MsoBodyText" style="text-align: left" align="left">

  <span style="font-size: 13pt"> </span>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 13pt">Четвертый год обучения</span></strong>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">1. Особенности программы четвертого года обучения </span></em></strong><em><span style="font-size: 13pt">(1 час)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.1.</span><span>  </span>Тема: Сведения об особенностях программы четвертого года обучения. </em><span style="font-size: 13pt">Основные особенности: упор на развитие проблемного мышления, приобретение практических навыков, участие в соревнованиях различного уровня.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.2.</span><span>  </span>Тема: Ожидаемые результаты и способы их оценки. </em><span style="font-size: 13pt">Высокие места в соревнованиях, участие в конкурсах высокого ранга (возможно, телепередачи), комплексный рейтинг по различным типам заданий.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">1.3. Тема:</span><span>  </span>Комплексная командная игра.<span>  </span></em><span style="font-size: 13pt">«Брэйн-ринг», «своя игра» по командам.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">2. Развитие творческих способностей </span></em></strong><em><span style="font-size: 13pt">(16 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.1.</span><span>  </span>Тема: Создание программы мероприятий и праздников. </em><span style="font-size: 13pt">Самостоятельное создание программы массового мероприятия (конкурса, соревнования, домашнего или школьного праздника): подбор игр, последовательность проведения, расчет времени с учетом подготовленности участников и ранга мероприятия.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.2.</span><span>  </span>Тема: Подборка материалов для вербальных и логических игр. </em><span style="font-size: 13pt">Создание новых или подборка из банка данных вопросов и заданий для вербальных и логических игр с учетом возраста игроков.</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">2.3. Тема: Подготовка сборников собственных заданий. </span><span> </span></em><span style="font-size: 13pt">Подготовка задания для<em> </em>тренингов, вопросов для интеллектуальных игр. Оформление собственных заданий в едином стиле, подборка рисунков и графики, компоновка брошюры, стенда, сочинение надписей и подписей под рисунками, заданиями и т.д.</span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">3. Решение конкретных проблем и задач ТРИЗ</span><span>  </span></em></strong><em><span style="font-size: 13pt">(26 часов)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.1. Тема: Решение проблем различного типа. </span></em><span style="font-size: 13pt">Анализ</span><span>  </span>проблемных ситуаций, решение сформулированных проблем методами аналогии, противопоставления, мозгового штурма. Анализ путей решения, результатов и последствий известными методами.

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">3.2. Тема: Решение задач ТРИЗ. </span></em><span style="font-size: 13pt">Решение подобранных задач с учетом изученных законов ТРИЗ (системность, динамизация, разрешения противоречий и т.д.). </span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">4. Интеллектуальные игры и задания (индивидуальные и командные) </span></em></strong><em><span style="font-size: 13pt">(33 часа).<strong> </strong></span></em>

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 13pt">Проведение различных интеллектуальных игр. Анализ тактики и стратегии команды в конкретной игре.</span><span>  </span>Формулировка причин выигрыша или проигрыша команды.<span>   </span>Формулировка задач конкретной команды для успеха в той или иной игре.

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">5.Развитие проблемного мышления, методика обсуждения и решения проблем (</span></em></strong><em><span style="font-size: 13pt">45 часов).</span></em>

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">5.1. Тема: </span><span> </span>Анализ и формулировка проблемы.<span>  </span></em><span style="font-size: 13pt">Анализ проблемной ситуации, выделение главного, постановка проблемных задач, возможные пути их решения. </span>

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">53.2. Тема: Законы создания нового в мире техники. Системность техники</span></em><span style="font-size: 13pt">: мир системен и состоит</span><span>  </span>из бесконечного ряда систем, подсистем и надсистем, все они взаимосвязаны. Анализ элементов системы, связей между ними, выход в под- и над-<span>  </span>систему.<span>  </span><em>Закон динамизации:</em> Изменение в одной части системы влечет изменения в других. Пути движения к идеальному результату. Девять «экранов мышления»: система, надсистема и подсистема в настоящем, прошедшем и будущем. <em><span> </span>Закон повышения идеальности:</em> в поисках решения часто полезно идти от конца к началу, от представления «идеального конечного результата».<span>  </span><em>Закон диалектических противоречий:</em> административные -<span>  </span>человек – техника;<span>  </span>технические<span>  </span>- между частями и в системы машин;<span>  </span>физические – с законами природы.<span>  </span>Причины возникновения, последствия, условия их разрешения. <em>Закон S-образного развития систем:</em> этапы развития системы от детства (малое использование) к зрелости (лавинообразное увеличение), «старость» - конфликт с новыми требованиями, частичный переход старых идей в новые разработки. И т.д.

</p>



<p class="MsoNormal" style="text-align: justify">

  <em><span style="font-size: 13pt">5.3. Тема: Анализ проблемной ситуации методом «плюс – минус – интересно» (ПМИ).</span><span>  </span></em><span style="font-size: 13pt">Метод качественного анализа проблемной ситуации «плюс – минус – интересно» (ПМИ), составление таблицы, где зафиксированы результаты анализа по трем колонкам для дальнейшего углубленного анализа выбранных моментов. </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.4. Тема: Методы анализа решения проблем «Альтернатива, возможности, выбор» (АВВ) и «Рассмотри все факторы» (РВФ). </span><span> </span></em><span style="font-size: 13pt">Обсуждение проблемной ситуации с анализом различных вариантов ее решения по схеме: «альтернативы – возможности – выбор» (АВВ). Понятие наилучшего, оптимального, компромиссного решения. Методика выбора. Углубленное сканирование проблемной ситуации – метод «рассмотри все факторы (<em>РВФ).<span>  </span></em>Метод рассмотрения готового решения во всех возможных ситуациях настоящего, прошедшего и будущего: «рассмотри все факторы» (РВФ).</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.5. Тема: Анализ возможных последствий выбранного решения «последствия и результаты» (ПиР).</span><span>  </span></em><span style="font-size: 13pt">Метод анализа выбранного результата решения проблемной ситуации для ближайших и отдаленных последствий – «последствия и результаты» (ПиР).</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">5.6.</span><span>  </span>Системный подход к решению вопросов как информационных систем. </em><span style="font-size: 13pt">Каждый вопрос ЧГК, из совокупности специально отобранных вопросов для подобной тренировки, рассматривать как информационную систему. Находить в ней части системы. Выявлять недостающие элементы или связи. Дополнительную информацию привлекать путем формулирования дополнительных вопросов к элементам системы (частям вопроса).</span><span>  </span>Находить смысловые связи между элементами вопроса и дополнительной информацией. Достраивать логичную смысловую цепочку.

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><em><span style="font-size: 13pt">6. Практическая и организационная деятельность</span></em></strong><em><span style="font-size: 13pt"> (18 часов практических занятий)</span></em>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.1.</span><span>  </span>Тема: Подготовка информационной и материальной части мероприятий.<span>  </span></em><span style="font-size: 13pt">Участие в подготовке атрибутики, программы и информационного содержания массового мероприятия различного ранга</span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.2. Тема: Организационная деятельность. </span></em><span style="font-size: 13pt">Деятельность с участниками мероприятия, участие в оргкомитете, жюри и т.д. Работа в составе оргкомитета, вспомогательного или основного конкурсного жюри.</span><span>  </span>Самостоятельное подготовка и проведение интеллектуальных игр или праздников в других учебных заведениях.<span>  </span>Организационная работа с будущими участниками массового мероприятия, проводимого данным учебным заведением.<span>  </span>

</p>



<p class="MsoBodyText">

  <em><span style="font-size: 13pt">6.3. Тема: </span></em><span style="font-size: 13pt">Участие в конкурсах, соревнованиях, фестивалях любого ранга сторонних организаций.</span>

</p>



<p class="MsoBodyText" style="margin-left: 24pt; text-indent: -24pt">

  <strong><em><span style="font-size: 13pt"></span><span>7.</span><span style="font: 7pt 'Times New Roman'">      </span></em></strong><strong><em><span style="font-size: 13pt">Оценка результатов работы по программе</span><span>  </span></em></strong><em><span style="font-size: 13pt">(5 часов)</span></em>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt">Предварительное подведение итогов по различным видам деятельности каждого обучаемого, проведение заключительного занятия с обсуждением собственных достижений, комплексной игрой.</span>

</p>



<p class="MsoBodyText" style="text-align: left" align="left">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: left" align="left">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: left" align="left">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span style="font-size: 14pt">МЕТОДИЧЕСКОЕ ОБЕСПЕЧЕНИЕ ЗАНЯТИЙ</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText">

  <strong><em><span>ОБЩИЕ ОСОБЕННОСТИ МЕТОДИЧЕСКОГО ОБЕСПЕЧЕНИЯ ПРОГРАММЫ</span></em></strong>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">На каждом занятии прорабатываются от 3</span><span>  </span>до 5 игр и заданий, относящихся к различным типам и формам. Темы, указанные в учебно-тематическом<span>  </span>плане,<span>  </span>прорабатываются в течение<span>  </span>всего срока обучения. Учебные часы, отведенные на ту или иную тему в тематическом плане каждого года обучения, показывают, каким темам следует уделить большее внимание в том или ином возрасте. При разработке конкретных планов занятий соблюдаются следующие <strong><em><u>правила</u>:</em></strong>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">нарастание сложности вопросов и заданий по содержанию и форме; </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">переход от простых мало информационных игр до сложных комплексных игр, требующих значительной эрудиции;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">в начале занятия дается устное задание, вовлекающее в работу всех обучающихся (вербальная игра);</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">наиболее сложное задание дается</span><span>  </span>в первой половине занятия (пространственные головоломка, проблемный вопрос); <span> </span><span> </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">комплексная игра со всеми обучающимися проводится</span><span>  </span>в конце занятия («брэйн ринг», «своя игра», «пентагон» и т.д.);

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">конкретное содержание вопросов игр соотносится со школьной программой</span><span>  </span>и согласуется с культурным уровнем обучающихся.

</p>



<p class="MsoBodyText">

  <strong><em><span>ПРИЕМЫ, </span><span> </span>МЕТОДЫ И ФОРМЫ<span>  </span>ОРГАНИЗАЦИИ УЧЕБНО-ВОСПИТАТЕЛЬНОГО ПРОЦЕССА ПО ОСНОВНЫМ РАЗДЕЛАМ ПРОГРАММЫ</em></strong>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Учебный год начинается с <strong>вводного занятия</strong>, цель - </span><span> </span>которого увлечь вновь поступивших детей программой курса. Преподаватель сообщает об особенностях курса и ожидаемых результатах, знакомит обучающихся<span>  </span>с «Правилами внутреннего распорядка и техники безопасности».<span>  </span>Для демонстрации роли игры в стимуляции развития памяти, ассоциативных связей и творческого мышления для детей среднего возраста проводится игра - тренинг «ассоциация». Старшим детям <span> </span>предлагается «брэйн ринг» или «своя игра», которые особенно привлекательны при наличии электронной игровой установки.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Игры и тренинги, способствующие развитию вербальных способностей, </span></strong><span style="font-size: 13pt">углубляют знания детей об особенностях русского языка, раскрывают его красоту. После знакомства с особенностями задания или игры, дети тренируются на вопросах преподавателя, набирая «очки»; затем сами составляют аналогичные упражнения.</span><span>  </span>Дети учатся слышать созвучия, ритмику фразы, формулировать определения, привлекать дополнительную информацию. <strong><em>Игры и задания</em></strong> <strong><em>по работе с фразами</em></strong> способствуют активизации словаря, умению высказать одну и ту же мысль различными способами, чувствовать двойной смысл фразеологизмов и т.д. <strong><em>Игры и задания</em></strong> <strong><em>по работе с текстами</em></strong><span>  </span>раскрывают особенности стиля писателей, подвигают детей на поиск и подбор цитат. В результате этого у них<span>  </span>повышается мотивация к чтению литературы. <span> </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Развитию творческих способностей детей </span></strong><span style="font-size: 13pt">способствуют<strong> </strong>такие задания, как <strong><em>сочинение нового окончания</em></strong><em> </em>старой сказки или <em>сюжета</em> сказки, сценария на заданную тему, загадки, пословицы, «вирши» (о стихах речи не идет, но и создание рифмованной вирши тоже процесс не простой). Вместе с преподавателем сначала разбираются «аналогичные случаи», обсуждается,</span><span>  </span>в каких случаях проявляется то или иное качество, что бывает, если… А затем в данном ключе создается свой сюжет. <strong><em>Ролевые игры</em></strong> с разработкой ситуации<span>  </span>и ее воплощения помогают снять усталость от долгого положения сидя.<span>  </span>Старшие дети проявляют свои творческие способности в <strong><em>составлении заданий словесных игр</em></strong>, текстовых заданий на основе информации, предложенной или подобранной самостоятельно, «изобретением» новых правил игр. Результаты работы старших детей используются и в качестве разминки при проверке домашнего задания, и как задания для внеконкурсных викторин во время проведения мероприятия в своем образовательном учреждении.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Методы запоминания информации, </span><span> </span>тренинги наблюдательности</strong><span style="font-size: 13pt"> осваиваются в игровой форме. Кто повторит больше слов из некого списка, кто придумает самый неожиданный поворот в сюжете рассказа и т.д. Используются рисунки предметов, таблицы слов и списки. <strong><em>Тренинг оперативной памяти</em></strong> проводится в виде </span><span> </span>«считалок», рассказов типа «снежный ком» из ритмических фраз, ассоциативных игр.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Тренинг пространственно – комбинаторного мышления. </span></strong><span style="font-size: 13pt">Как показала практика, пространственные головоломки, позиционные игры являются наиболее сложными для большей части детей. Поэтому задания с фигурами предлагаются после 1 – 2 месяцев зханятий. Тренинг проводится с плоскостными головоломками, пазлами, спичками, задачами на разрезание, трансформацию фигур, которые либо есть готовые, либо дети вырезают их из плотной бумаги или выполняют из бумаги в технике оригами.</span><span>  </span>Используются игры на бумаге, на перемещения, развертки, складывание фигур, решаются игровые задачи, разбираются типичные ситуации позиционных игр, анализируются игровые стратегии.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Стимулирование развития логического мышления </span></strong><span style="font-size: 13pt">производится с помощью л<strong><em>огических вербальных игр</em></strong>, (которые в дальнейшем помогают ребятам заполнить «пустое» время в транспорте во время совместной поездки на соревнования, в лагерь), <strong><em>логических задач или детективных историй</em></strong>, и<strong><em>ндивидуальные и командные игры</em></strong></span><span>  </span>«Что? Где? Когда?»,<span>  </span>«Брэйн ринга», «Своей игры» и т.д. <span> </span>Желательно иметь электронную установку, позволяющая установить, кто первым решился дать ответ. Игры проводятся по своим правилам. Результаты отслеживаются по личным таблицам рейтинга каждого участника. <span> </span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Решение проблемных вопросов </span></strong><span style="font-size: 13pt">помогает научить детей пользоваться методами, позволяющими глубже проанализировать ситуацию, рассмотреть факторы, определяющие ситуацию, использовать методы анализа, уйти от шаблона в мышлении. Учит умению определить область действия проблемы, выделить объекты системы, основные связи между ее объектами. Прививает навыки коллективного обсуждения, работы в группе.</span><span>  </span><strong><em>Разбор проблемной ситуации, формулирование проблемы</em></strong> начинается с рассказа преподавателя, с «введения в ситуацию». Дети выискивают причины, породившие ситуацию, находят противоречия, учатся выделять главное из них. Затем формулируется проблема, осуществляется поиск путей разрешения ситуации. Разбирается несколько проблемных вопросов, то есть таких, что имеют не одно, а несколько, порой множество, вариантов решения. Коллективным обсуждениям выбирается наилучший вариант. Дети учатся объяснять свой выбор, оценивать сильные и слабые стороны решения, возможные последствия его применения в ближайшем и отдаленном будущем.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><em><span style="font-size: 13pt">Способы записи</span><span>  </span>идей, связей.</em></strong><span style="font-size: 13pt"> Во время обсуждения, генерации идей методами мозгового штурма,</span><span>  </span>фокальных объектов, нужно успевать записывать то, что выкрикивают дети. Поэтому требуется обучить их быстрой фиксации идей при помощи кластеров, «деревьев», таблиц, где записываются не длинные фразы, а ключевые слова.<span>  </span>Появляется возможность классифицировать идеи, цепляя «лепестки» кластера или ветку «дерева» в нужное место. Затем классифицированная информация записывается в нужной форме.

</p>



<p class="MsoNormal" style="text-align: justify">

  <strong><span style="font-size: 13pt"></span><span> </span><span>     </span><em>Решение простейших задач ТРИЗ</em> </strong><span style="font-size: 13pt">является одним из видов практических «проблемных вопросов». Не обязательно это изобретение неких конструкций.</span><span>  </span>Иногда в таком качестве выступают и конкретные социальные задачи.<span>  </span>Преподаватель следит за ходом обсуждения, в затруднительных случаях подает подсказку, позволяющую нащупать путь. Во время мозгового штурма пресекаются<span>  </span>любые попытки анализа идей, личностные выпады детей, перебивания товарищей. Анализ предложенных идей производится после.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt"></span><span> </span>Интеллектуальные индивидуальные и командные игры<em> </em></strong><span style="font-size: 13pt"></span><span> </span>дети осваивают постепенно в течение первых двух лет обучения. Все типы игр используются в течение срока реализации Программы. Меняются формулировки, сложность<span>  </span>и глубина вопросов викторин. Для индивидуальных устных игр используются игровые технологии: устный круговой опрос индивидуальный или командный, покерная система накопления и «сгорания» очков, бланковые формы и др. <strong><em>Индивидуальные игры</em></strong> часто используются в качестве разминки или подготовки какому-либо тренингу. <strong><em>Командные игры</em></strong> – это викторины типа «Брэйн ринг»,<span>  </span>«Что? Где? Когда?»,<span>  </span>«Черная метка», «надуваловка» и др. Именно эти игры способствуют привитию навыков умственной работы в коллективе в условиях дефицита времени. Командная игра позволяет вывить у детей черты лидера и исполнителя, генератора идей и критика идей, «анализатора и синтезатора».<span>  </span>Эти игры позволяют детям расширить эрудицию, учат прислушиваться «к внутреннему голосу» - проявлению интуиции, способу неосознанного решения проблемы. Они являются практическим приложением навыка логического мышления. Поэтому они могут использоваться после разбора любой темы в конце занятия.

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><em><span style="font-size: 13pt">Выбор стратегии игры</span></em></strong><span style="font-size: 13pt"> определяется общим настроем игры, характером капитана и игроков. Задача преподавателя – объяснить достоинства и недостатки того или иного игрового поведения.</span>

</p>



<p class="MsoBodyText">

  <strong><span style="font-size: 13pt"></span><span> </span><span>     </span>Практическая работа и<span>  </span>организационная деятельность.</strong><span style="font-size: 13pt"> Только игры в вопросы и ответы не позволяют достичь желаемого результата, если дети не будут участвовать в подготовке и проведении массовых мероприятий, не научатся делать головоломки, сочинять сценарии домашних праздников, готовить к ним конкурсы и вопросы для игр, проводить предварительную организационную работу. Эта работа осваивается ребятами в течение срока обучения, начиная с разбора специализированных заданий, выполнения отдельных поручений, кончая активным участием в подготовке и проведении мероприятий.</span>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <strong><span style="font-size: 13pt">Тестирование об</span></strong><span style="font-size: 13pt">учающихся для определения начальных показателей основных психических функций проводится преподавателем, если в учебном заведении отсутствует специалист – психолог.</span><span>  </span>Однако по желанию преподаватель может провести специальные тесты на проверку когнитивных способностей детей, их творческих предпочтений. Некоторые тесты можно<span>  </span>провести в виде игры или бланкового задания. Результаты тестирования помогают увидеть особенности интеллектуальных способностей детей, динамику их развития.

</p>



<p class="MsoBodyText">

  <strong><em><span>СПОСОБЫ ПОДВЕДЕНИЯ ИТОГОВ ПО ОСНОВНЫМ РАЗДЕЛАМ ПРОГРАММЫ</span></em></strong>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt"></span><span>            </span>Отслеживание результатов работы каждого обучающегося по программе осуществляется в соответствии с приведенной ниже таблицей критериев результатов обучения и личного развития ребенка; разработана дифференциация критериев по годам обучения.<strong><span></span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>ТАБЛИЦА КРИТЕРИЕВ РЕЗУЛЬТАТОВ ОБУЧЕНИЯ</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>И ЛИЧНОСТНОГО РАЗВИТИЯ РЕБЕНКА</span></strong>

</p>



<p class="MsoBodyText" style="text-align: center" align="center">

  <strong><span>Первый год обучения</span></strong>

</p>



<table class="MsoTableGrid" style="border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr>

    <td style="width: 163.8pt; border: 1pt solid windowtext; padding: 0cm 5.4pt" valign="top" width="218">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Низкий уровень </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(1 – до 3 балла)</span></strong>

      </p>

    </td>

    

    <td style="width: 163.8pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="218">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Средний уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 3 – 5 – до 7</span><span>  </span>баллов)</strong>

      </p>

    </td>

    

    <td style="width: 159.35pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="212">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Высокий уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 7 – 10 баллов)</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 163.8pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="218">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабое знание детской<span>  </span>литературы. Словарь узок, трудности с подбором слов с заданными свойствами. Частое повторение одних и тех же фраз.<span>  </span>Тривиальные объяснения ситуаций, заданных вербальной или графической форме.<span>  </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span>  </span>Плохое восприятие задания на слух. Забывает<span>  </span>прошлые задания.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабая фантазия: трудности с созданием нового слова или понятия, отсутствие выразительности в жесте т.д.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабо различает главное и второстепенное.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Плохое владение инструментами, низкое качество изделий из бумаги. Нарушение правил ТБ.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Нетерпелив,<span>   </span>легко отвлекается, выпаливает первый пришедший на ум ответ.

      </p>

    </td>

    

    <td style="width: 163.8pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="218">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Уровень теоретических<span>  </span>знаний соответствует школьному. Ссылки не на первоисточник, а на экранизацию литературного произведения.<span>  </span>Словарь соответствует возрасту. Речь довольно грамотная, но слабо выразительная.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Способен сравнить, выделить главное и второстепенное. Находит традиционные объяснения ситуаций, оригинальные идее не часты.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Легко подражает знакомому, но не придумает нового, несуществующего.

      </p>

      

      <p class="MsoNormal" style="text-align: justify">

        <span style="font-size: 12pt"></span><span>  </span>Редко пользуется мнемоникой, легко схватывает, но быстро забывает. Различает фигуры независимо от их расположения на плоскости.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Требуется подбадривание,<span>  </span>чтобы трудное задание было доведено до конца. Проявляет интерес к новому.<span></span>

      </p>

    </td>

    

    <td style="width: 159.35pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="212">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Теоретические знаний превышают базовый школьный уровень, хорошее знание литературы. Понимание терминов. Грамотная речь, словарный запас широк.<span>   </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Легко выделяет существенные признаки,<span>  </span>классифицирует предметы, явления.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Справляется с плоскостными задачами,</span><span>  </span>выигрывает в позиционных играх.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Легко придумает новое слово, явление, невиданное животное.</span><span>  </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Пользуется на практике способами ассоциативного запоминания.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        Кропотливо работает над заданием, требующим владения инструментами

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Доброжелателен,, хорошо играет в команде. Легко откликается на призыв товарища о помощи.<span>  </span><span></span>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span lang="EN-US"> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span>Второй год обучения</span></strong>

</p>



<table class="MsoTableGrid" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr>

    <td style="width: 160.8pt; border: 1pt solid windowtext; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Низкий уровень </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(1 – до 3 балла)</span></strong>

      </p>

    </td>

    

    <td style="width: 160.8pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Средний уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 3 – 5 – до 7</span><span>  </span>баллов)</strong>

      </p>

    </td>

    

    <td style="width: 160.8pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Высокий уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 7 – 10 баллов)</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 160.8pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабое знание<span>  </span>литературы, неумение привлечь знания к поиску ответа на вопрос.<span>   </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Использование одних и тех же слов и конструкций в предложении, наличие слов – паразитов.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Отсутствие<span>  </span>идей для сюжета рассказа или решения проблемной задачи.<span> </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Плохое видение игрового поля в позиционных играх. Сложности с подбором деталей фигур.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабое понимание причинно-следственных связей событий, трудности с формулировкой вывода.<span>    </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Нет вдумчивости и<span>  </span>аккуратности. При неудаче заявляет, что «Не получится» и прекращает работу.<span> </span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Небрежное ведение тетради, неумение пользоваться сокращениями. Нарушение правил ТБ при работе с электронной установкой.

      </p>

    </td>

    

    <td style="width: 160.8pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span>  </span>Задания воспринимает на слух. Теоретические<span>  </span>знания, речь, словарь соответствуют школьному базовому уровню. Свободное использование терминов. Книгам предпочитает видео.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Способен увидеть главное и второстепенное, сделать выбор, подвести итог.</span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Редко использует методы запоминания. В тетради ведет подборку заданий, пользуется сокращениями, кластерами, таблицами.</span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Может развить предложенную идею, мысль, <span>предложить несколько способов решения проблемной задачи, но чаще те, что лежат на поверхности</span>.<span>  </span><span>Соблюдает правила ТБ при пользовании электронной установкой.</span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Отвлекается, но при поддержке доводит<span>  </span>дело до конца.<span>  </span>Проявляет интерес к новой информации.<span></span>

      </p>

    </td>

    

    <td style="width: 160.8pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="214">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Теоретические знаний выше<span>  </span>школьного уровня, регулярно чтение литературы вне школьной программы.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Свободное пользование терминами, расширенный лексический словарь. Правильная речь, легко формулирует вопросы и ответы.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Понимает и замечает аналогию, выделяет противоречия. Легко запоминает новые<span>  </span>алгоритм, пользуется способами<span>  </span>запоминания.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Записывает все задания. Четко работает с инструментами,</span><span>  </span>электронной установкой. Соблюдение правил ТБ при пользовании электронной установкой.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span> </span>Отдает отчет в своих действиях, в случае неудачи, начинает работу заново.<span>  </span>От конфликтов уклоняется. Помогает другим, делится информацией,<span>  </span>приносит книги.<span></span>

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span>Третий год обучения</span></strong>

</p>



<table class="MsoTableGrid" style="margin-left: 5.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 27.25pt">

    <td style="width: 162pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="216">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Низкий уровень </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(1 – до 3 балла)</span></strong>

      </p>

    </td>

    

    <td style="width: 153pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Средний уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 3 – 5 – до 5 -7</span><span>  </span>баллов)</strong>

      </p>

    </td>

    

    <td style="width: 171pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="228">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Высокий уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 7 – 10 баллов)</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 162pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="216">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Невыразительность речи. Трудности с формулировкой вопросов, ошибки в использовании<span>  </span>терминов, затруднение с подборкой<span>  </span>рифм.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Слабое видение связей между отдельными элементами системы. Слабое использование школьных знаний для решения практических проблем.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Трудности с развитием старых сюжетов или созданием новых образов.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Индивидуализм - неумение работать в команде, раздражительность,<span>  </span>отказ от участия в турнирах, массовых мероприятиях.

      </p>

    </td>

    

    <td style="width: 153pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Уровень теоретических<span>  </span>знаний не превышает школьный, знание самых популярных в данное время произведений.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Может сформулировать проблемный вопрос, выделить элементы системы в их взаимосвязи, сделать вывод, объяснить выбор варианта решения.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Умеет по аналогии создать рассказ, развить сюжет, изобразить героя различными средствами (графическими, вербальными др.)

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Любит командные игры, участвует в соревнованиях, готовит мероприятия.

      </p>

    </td>

    

    <td style="width: 171pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="228">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Теоретические знаний превышают базовый школьный уровень, хорошее знание художественной и научно-популярной литературы.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Пользуется приемами решения проблемных вопросов, умеет выделить систему, найти отсутствующие элементы или связи между ними, увидеть аналогию или противопоставление. Делает записи идей по ходу обсуждения.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Создает новые образы в любой форме.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Активно работает в команде, участвует в подготовке и проведении мероприятий, соревнований.

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span> </span></strong>

</p>



<p class="MsoBodyText" style="text-align: center; text-indent: 35.4pt" align="center">

  <strong><span>Четвертый год обучения</span></strong>

</p>



<table class="MsoTableGrid" style="margin-left: 14.4pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr style="page-break-inside: avoid; height: 27.25pt">

    <td style="width: 153pt; border: 1pt solid windowtext; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Низкий уровень </span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(1 – до 3 балла)</span></strong>

      </p>

    </td>

    

    <td style="width: 153pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Средний уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 3 – 5 – до 7</span><span>  </span>баллов)</strong>

      </p>

    </td>

    

    <td style="width: 171pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt; height: 27.25pt" valign="top" width="228">

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>Высокий уровень</span></strong>

      </p>

      

      <p class="MsoBodyText" style="text-align: center" align="center">

        <strong><span>(от 7 – 10 баллов)</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 153pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Низкая общая образованность, слабая связь между отдельными областями теоретических<span>  </span>знаний, потому и слабое использование их при решении практических проблем.<span>  </span>Редки новые идеи решения проблемы, они<span>  </span>тривиальны, лежат на поверхности. Неумение видеть скрытые<span>  </span>или опосредованные связи между элементами системы. Не способен прогнозировать последствия принятых решений. Также тривиальны созданные им образы или сюжеты.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Пассивное поведение в команде, может отказаться от участия в соревнованиях.</span><span>  </span>В команде конфликтен, не умеет сдерживать негативные эмоции. Не часто помогает при подготовке мероприятия.

      </p>

    </td>

    

    <td style="width: 153pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="204">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Уровень теоретических</span><span>  </span>знаний не превышает школьный, знание самых популярных в данное время произведений.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>При обсуждении практических проблемных задач часто останавливается на тривиальных вариантах решения

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>С трудом отходит от шаблона при создании новых образов, идей, вариантов развития сюжета.</span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>С удовольствием участвует в индивидуальных или командных играх, предпочитая те из них, что отвечают его типу мышления. В команде играет определенную роль.<span></span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>По мере возможности участвует в подготовке мероприятий, что поручили, но может и подвести, потому что «забыл». <span></span>

      </p>

    </td>

    

    <td style="width: 171pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="228">

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span>Теоретические знаний превышают базовый школьный уровень, хорошее знание литературы, оставшейся за рамками школьной программы, чтение научно-популярной литературы.</span>

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>При решении практических проблемных задач используют методы эффективного мышления, чтобы создать оригинальное решение. Могут прогнозировать результаты решения проблемы, оценить их последствия.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Способны сами подготовить и провести игру, придумать свои вопросы, сочинить новые правила.

      </p>

      

      <p class="MsoBodyText" style="text-align: left" align="left">

        <span></span><span> </span>Играют стабильно, умеет поддерживать хорошую атмосферу в команде, Занимает высокие места в соревнованиях по интеллектуальным играм.

      </p>

    </td>

  </tr>

</table>



<p class="MsoBodyText" style="text-indent: 35.4pt">

  <span> </span>

</p>



<p class="MsoBodyText">

  <strong><em><span>ТРЕБОВАНИЯ К ПОМЕЩЕНИЮ И ТЕХНИЧЕСКОМУ ОСНАЩЕНИЮ ЗАНЯТИЙ</span></em></strong>

</p>



<p class="MsoBodyText" style="text-indent: 18pt">

  <span style="font-size: 13pt">Помещение для занятий должно быть сухим, светлым, площадь и объем</span><span>  </span>- соответствовать санитарным нормам для одновременного нахождения в нем 20 человек. Освещение естественное через окна слева,<span>  </span>искусственное – <span> </span>лампы дневного света. Освещение непрямое, рассеянное. Электрические розетки находиться в местах, недоступных для детей младшего школьного возраста. Форточки и фрамуги должны находятся в верхней части окна. На окнах должны быть тюлевые занавески и обязательно шторы, если окна выходят на юг. Пол гладкий деревянный. Влажная уборка проводится ежедневно.<span>  </span>Часть площади пола свободна от мебели для подвижных игр.<span>  </span>Водопровод, туалет должны находиться недалеко.

</p>



<p class="MsoBodyText">

  <strong><em><span style="font-size: 13pt">Перечень</span><span>  </span>оборудования<span>  </span>и<span>  </span>материалов</em></strong><span style="font-size: 13pt"></span>

</p>



<p class="MsoBodyText">

  <span style="font-size: 13pt">В помещении желательно предусмотреть следующее оборудование:</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">школьная доска со створками или переворачивающаяся на другую сторону, магнитные подвески; стеллажи для работ обучающихся, стимульного материала, стол преподавателя с электрической розеткой на 220 в,</span><span>  </span>недоступной для детей; книжный шкаф для литературы, материалов,<span>  </span>8 – 10 парт и 16-20 стульев для детей;

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">стенд для клубной информации, работ детей, фотографий, отчетов и др.;</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">компьютер, для формирования банка работ детей, преподавателя, необходимых методических материалов, заданий и т.д., учебные географические карты мира и России большого формата; материалы для проведения занятий,</span><span>  </span>магнитофон;<span>  </span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">игровая установка с 2 - 4 выносными пультами с сигнализацией и фиксацией первого нажатия.</span>

</p>



<p class="MsoBodyText" style="margin-left: 18pt; text-indent: 0cm">

  <span style="font-size: 13pt"></span><span>-</span><span style="font: 7pt 'Times New Roman'">         </span><span style="font-size: 13pt">копировальная техника для подготовки стимульного материала, канцелярские товары: бумага, папки, ножницы, цветной картон, клей, и т.д. </span>

</p>



<p class="MsoBodyText" style="margin-left: 36pt">

  &nbsp;

</p>



<p class="MsoBodyText" style="margin-left: 36pt">

  &nbsp;

</p>



<p class="MsoBodyText" style="margin-left: 36pt">

  &nbsp;

</p>



<h1 style="text-align: center" align="center">

  <strong><span style="font-size: 14pt">БИБЛИОГРАФИЯ</span></strong>

</h1>



<p class="MsoNormal">

  &nbsp;

</p>



<h2 style="text-align: center" align="center">

  <span style="font-size: 14pt; font-weight: normal; font-style: normal">Литература для преподавателей</span>

</h2>



<p class="MsoNormal">

  &nbsp;

</p>



<table class="MsoTableGrid" style="border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr>

    <td style="width: 23.4pt; border: 1pt solid windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">№</span></strong>

      </p>

    </td>

    

    <td style="width: 100.95pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="135">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">Характеристика книги</span></strong>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">Библиографическое издание</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">1</span>

      </p>

    </td>

    

    <td rowspan="8" style="width: 100.95pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="135">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Книга с одним автором</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <span style="font-size: 11pt">126 эффективных упражнений по развитию вашей памяти/</span><span>  </span>М.: АСТ, 2000

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">2</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Де Боно</span><span>  </span>Э.</em><span style="font-size: 11pt"> Нестандартное мышление/ Минск: Попурри, 2000. </span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">3</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <span style="font-size: 11pt">Лейтес Н.С. Способности и одаренность в детские годы/</span><span>  </span>М.: Знание 1994

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">4</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Савенков А.И.</span></em><span style="font-size: 11pt"> Технология работы с одаренными обучающимися/</span><span>  </span>М.: Педагогика, 2002

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">5</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Савенков А.И.</span></em><span style="font-size: 11pt"> Путь в неизведанное, развитие исследовательских способностей школьников, М.: Генезис, 2005</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">6</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <span style="font-size: 11pt">Стайн Дж.М.: Расширение возможностей интеллекта/ М.: ЭКСМО </span>

      </p>

      

      <p class="MsoNormal">

        <span style="font-size: 11pt">-пресс, 2001</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">7</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Штернберг Р. (Стернберг)</span></em><span style="font-size: 11pt"> Отточите свой интеллект/</span><span>  </span>Минск: Попурри, 2000.

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">8</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Юркевич В.С.</span></em><span style="font-size: 11pt"> Одаренный ребенок/</span><span>  </span>М.: Просвещение, 1996

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">9</span>

      </p>

    </td>

    

    <td rowspan="2" style="width: 100.95pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="135">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Книга с двумя и тремя авторами</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Грановская Р.М., Крижанская Ю.С.</span></em><span style="font-size: 11pt"> Творчество и преодоление стереотипов/ С-П: ОМС,1994</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">10</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Загашев И.О., Заир-Бек С.И., Муштавинская</span><span>  </span>И.В.</em><span style="font-size: 11pt"> Учим</span><span>  </span>детей мыслить критически/ С-П: Скифия,<span>  </span>Альянс - Дельт, Речь, 2003

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">11</span>

      </p>

    </td>

    

    <td style="width: 100.95pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="135">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Многотомные издания</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <span style="font-size: 11pt">Психологические тесты в 2-х томах под ред. <em>А.А. Карелина/ </em>М.: Владос, 2000</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">12</span>

      </p>

    </td>

    

    <td rowspan="4" style="width: 100.95pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="135">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Статьи из журналов</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Богоявленская Д.Б.</span></em><span style="font-size: 11pt"> Одаренность: предмет и метод/ Магистр, 1994, № 6</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">13</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Чудновский В.С., Юркевич В.С.</span></em><span style="font-size: 11pt"> Одаренность: дар или испытание/ М.: Знание, серия «Педагогика и психология» 1990, № 12.</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">14</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <span style="font-size: 11pt">Научно - популярный журнал «Знание – сила», 2000 – 2005 гг.</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 23.4pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="31">

      <p class="MsoNormal">

        <span style="font-size: 11pt">15</span>

      </p>

    </td>

    

    <td style="width: 354.2pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="472">

      <p class="MsoNormal" style="text-align: justify">

        <span style="font-size: 11pt">Научно - популярный журнал «Наука и жизнь», 2000 – 2005 гг.</span>

      </p>

    </td>

  </tr>

</table>



<h2 style="text-align: center" align="center">

  <span style="font-size: 14pt; font-weight: normal; font-style: normal">Литература для детей и родителей</span>

</h2>



<p class="MsoNormal">

  &nbsp;

</p>



<table class="MsoTableGrid" style="margin-left: -0.9pt; border-collapse: collapse; border: medium none" border="1" cellpadding="0" cellspacing="0">

  <tr>

    <td style="width: 24.3pt; border: 1pt solid windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">№</span></strong>

      </p>

    </td>

    

    <td style="width: 99pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="132">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">Характеристика книги</span></strong>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: 1pt 1pt 1pt medium; border-style: solid solid solid none; border-color: windowtext windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt"> </span></strong>

      </p>

      

      <p class="MsoNormal" style="text-align: center" align="center">

        <strong><span style="font-size: 12pt">Библиографическое издание</span></strong>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">1</span>

      </p>

    </td>

    

    <td rowspan="19" style="width: 99pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="132">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 12pt">Книга с одним автором</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Агафонова</span><span>  </span>И.</em><span style="font-size: 11pt"></span><span>  </span>Учимся думать/ С-П: МиМ-Экспрессо, 1996

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">2</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Анашина Н.Ю.. Галеева Р.А., Мельченко И.В</span></em><span style="font-size: 11pt">. Интеллектуальные праздники, викторины, дни знаний/</span><span>  </span>Ростов-на-Дону: Феникс, 2004

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">3</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Анашина Н.Ю., </span></em><span style="font-size: 11pt">Энциклопедия интеллектуальных игр, 1- 2том,<em> </em>Ярославль, Академия развития, 2005, 2006</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt"> </span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Анашина Н.Ю., серия Интеллектуальные игры в школе: </span></em><span style="font-size: 11pt">День словесности в школе, 2006, День естествознания в школе, 2007, День истории в школе, 2008, Ярославль, Академия развития</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">4</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Винокурова Н.</span></em><span style="font-size: 11pt"> Магия интеллекта, или книга о том, когда дети бывают умнее, быстрее, смышленее взрослых/</span><span>  </span>М:<span>  </span>Эйдос, 1994

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">5</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Винокурова</span><span>  </span>Н.</em><span style="font-size: 11pt"> «Лучшие тесты на развитие творческих способностей»/</span><span>  </span>М.: АСТ-ПРЕСС, 1999

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">6</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Гурин Ю.В.</span></em><span style="font-size: 11pt"> Лучшие игры на бумаге/</span><span>  </span>С-П: Кристалл, 2000

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">7</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Годер Г.И.</span></em><span style="font-size: 11pt"> Задания и задачник по истории древнего мира/ М: Просвещение, 1996.</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">8</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Иванов</span><span>  </span>Г.И.</em><span style="font-size: 11pt"> Формула творчества, или как научиться изобретать/ М.: Просвещение, 1994</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">9</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Казанский О.А.</span></em><span style="font-size: 11pt"> Игры в самих себя/ М.: Менеджер, 1994</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">10</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Калашников В.Э.</span></em><span style="font-size: 11pt"> Интеллектуальные игры/ М.: Инновационно -образовательный центр, 1997</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt"> </span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Курбанов Г.С.</span></em><span style="font-size: 11pt">Умные игры для детей и их родителей/</span><span>  </span>Ростов-на-Дону: Феникс, 2003

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt"> </span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Модестов С.Ю.</span></em><span style="font-size: 11pt"></span><span>  </span>Сборник творческих задач по биологии. Экологии и ОБЖ/<span>  </span>С-П: Акцидент», 1998

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">11</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Левин Борис</span></em><span style="font-size: 11pt"> Что? Где? Когда? для чайников/ Донецк: ИКФ Сталкер, 1998</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">12</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Синицына Е.</span></em><span style="font-size: 11pt"> Умные загадки/ М.: Лист, 1997</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">13</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Сукач Дж.</span></em><span style="font-size: 11pt"></span><span>  </span>Детективные головоломки для начинающих сыщиков/ М.: АСТ-Пресс, 1998

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">14</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Хайчин Ю.Д.</span></em><span style="font-size: 11pt"> Твоя игра/ М.: Издательство торговый дом «Гранд», 2000</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">15</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Хенкок Дж.</span></em><span style="font-size: 11pt"> Самоучитель по развитию памяти/ М.: ЭКСМО-пресс, 2001</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">16</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">А.Я.Цукарь </span></em><span style="font-size: 11pt">Уроки развития воображения/ Новосибирск, 1997</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">18</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Никитин Ю.З., Никитина Е.Ю.</span></em><span style="font-size: 11pt"> Развитие сообразительности у детей/</span><span>   </span>Екатеринбург: АРД ЛТД, 1999

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">19</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Рыбинский В., . Мельченко И.</span></em><span style="font-size: 11pt"> Развитие творческого мышления/ М., 2002</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">20</span>

      </p>

    </td>

    

    <td style="width: 99pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="132">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Книга со многими авторами</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal" style="text-align: justify">

        <em><span style="font-size: 11pt">Е.Алексеев, В.Белкин, Н.Курмашова, М. Поташов, И.Тюрикова</span></em><span style="font-size: 11pt"> Что? Где? Когда? Ваш путь к успеху/ Айрис-пресс, Рольф, М. 2000</span>

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">21</span>

      </p>

    </td>

    

    <td rowspan="2" style="width: 99pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="132">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Многотомные издания</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Тюрикова И., Пехлецкий С., Бражников В., Молчанов В. </span></em><span style="font-size: 11pt">Своя игра: Выпуски 1 – 9/</span><span>  </span>М.: Тера 1995-1999

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">22</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">Хайчин Ю.Д.</span></em><span style="font-size: 11pt"> Книга для умных и находчивых. Книги 1, 2, 3/</span><span>  </span>Донецк: ИКФ Сталкер 1996-1998

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">23</span>

      </p>

    </td>

    

    <td rowspan="2" style="width: 99pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="132">

      <p class="MsoNormal">

        <span style="font-size: 12pt">Материалы журналов</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <span style="font-size: 11pt">«<em>Абажур»/</em></span><span>  </span>М.: МГУ, 1999 – 2002

      </p>

    </td>

  </tr>

  

  <tr>

    <td style="width: 24.3pt; border-width: medium 1pt 1pt; border-style: none solid solid; border-color: -moz-use-text-color windowtext windowtext; padding: 0cm 5.4pt" valign="top" width="32">

      <p class="MsoNormal" style="text-align: center" align="center">

        <span style="font-size: 11pt">24</span>

      </p>

    </td>

    

    <td style="width: 356.85pt; border-width: medium 1pt 1pt medium; border-style: none solid solid none; border-color: -moz-use-text-color windowtext windowtext -moz-use-text-color; padding: 0cm 5.4pt" valign="top" width="476">

      <p class="MsoNormal">

        <em><span style="font-size: 11pt">«Игра</span></em><span style="font-size: 11pt">»/ Днепропетровск: ИГРА –</span><span style="font-size: 11pt" lang="EN-US">VLD</span><span style="font-size: 11pt"> press-Stirol,</span><span>  </span>ЧП ПАФ, 1995 - 2005

      </p>

    </td>

  </tr>

</table>



<p class="MsoNormal">

  &nbsp;

</p>



<p class="MsoNormal" style="text-align: justify">

  <span style="font-size: 12pt"></span><span>            </span><span style="font-size: 13pt">В качестве <strong>видеоинформации</strong> рекомендуются записи <strong>телепередач:</strong></span>

</p>



<p class="MsoNormal" style="text-align: justify; text-indent: 35.4pt">

  <span style="font-size: 13pt">«Что? Где? Когда?», «Умники и умницы». «Своя</span><span>  </span>игра», «Брэйн-ринг», «Детектив-шоу», «Антимонии», «Я знаю все», «Как стать миллионером?», «Звездный час», «Великолепная семерка».

</p>