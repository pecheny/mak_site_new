+++
title = "Лицензирование турниров"
date = 2007-07-21T15:57:54
author = "Константин Кноп"
guid = "http://mak-chgk.ru/licenz/"
slug = "licenztur"
aliases = [ "/post_807", "/licenz",]
type = "post"
categories = [ "Прочее",]
tags = []
+++

Изменения в порядок лицензирования соревнований были внесены в июне 2012 г.



[Действующие правила лицензирования](../komissii/licenzkom/licenztur2014).



[Предыдущая версия правил](../komissii/licenzkom/licenztur2012).