+++
title = "Рейтинг МАК"
date = 2007-07-20T22:53:54
author = "Константин Кноп"
guid = "http://mak-chgk.ru/rating_base/"
slug = "rating"
aliases = [ "/post_806", "/rating_base",]
type = "post"
categories = [ "Прочее",]
tags = []
+++

Официальный рейтинг МАК создан с целью учесть результаты команд в турнирах по спортивному &#8220;Что? Где? Когда?&#8221; для дальнейшего использования в качестве инструмента отбора на соревнования, проводимые МАК.



C Положением о рейтинге можно ознакомиться [здесь](http://ratingnew.chgk.info/documents.php?doc=24)